Subject_ID	Session_No	Trial_No	PRL_Position	Presentation_Mode	Num_Errors_Trial
S05	1	0	Left	Simulated with guides	1
S05	1	1	Left	Simulated with guides	2
S05	1	2	Left	Simulated with guides	5
S05	1	3	Left	Simulated with guides	4
S05	1	4	Left	Simulated with guides	2
S05	1	5	Left	Simulated with guides	4
S05	1	6	Left	Simulated with guides	1
S05	1	7	Left	Simulated with guides	7
S05	1	8	Left	Simulated with guides	4
S05	1	9	Left	Simulated with guides	2
S05	1	10	Left	Simulated with guides	6
S05	1	11	Left	Simulated with guides	1
S05	1	12	Left	Simulated	8
S05	1	13	Left	Simulated	4
S05	1	14	Left	Simulated	3
S05	1	15	Left	Simulated	4
S05	1	16	Left	Simulated	4
S05	1	17	Left	Simulated	7
S05	1	18	Left	Simulated	2
S05	1	19	Left	Simulated	3
S05	1	20	Left	Simulated	3
S05	1	21	Left	Simulated	7
S05	1	22	Left	Simulated	6
S05	1	23	Left	Simulated	0
S05	1	24	Left	Foveal	0
S05	1	25	Left	Foveal	0
S05	1	26	Left	Foveal	0
S05	1	27	Left	Foveal	0
S05	1	28	Left	Foveal	0
S05	1	29	Left	Foveal	0
S05	1	30	Left	Foveal	0
S05	1	31	Left	Foveal	0
S05	1	32	Left	Foveal	0
S05	1	33	Left	Foveal	0
S05	1	34	Left	Foveal	0
S05	1	35	Left	Foveal	0
S05	2	0	Left	Foveal	0
S05	2	1	Left	Foveal	0
S05	2	2	Left	Foveal	0
S05	2	3	Left	Foveal	0
S05	2	4	Left	Foveal	0
S05	2	5	Left	Foveal	0
S05	2	6	Left	Foveal	0
S05	2	7	Left	Foveal	0
S05	2	8	Left	Foveal	0
S05	2	9	Left	Foveal	0
S05	2	10	Left	Foveal	0
S05	2	11	Left	Foveal	0
S05	2	12	Left	Simulated	1
S05	2	13	Left	Simulated	2
S05	2	14	Left	Simulated	2
S05	2	15	Left	Simulated	5
S05	2	16	Left	Simulated	5
S05	2	17	Left	Simulated	2
S05	2	18	Left	Simulated	5
S05	2	19	Left	Simulated	4
S05	2	20	Left	Simulated	8
S05	2	21	Left	Simulated	4
S05	2	22	Left	Simulated	3
S05	2	23	Left	Simulated	7
S05	2	24	Left	Simulated with guides	4
S05	2	25	Left	Simulated with guides	1
S05	2	26	Left	Simulated with guides	5
S05	2	27	Left	Simulated with guides	5
S05	2	28	Left	Simulated with guides	4
S05	2	29	Left	Simulated with guides	3
S05	2	30	Left	Simulated with guides	3
S05	2	31	Left	Simulated with guides	3
S05	2	32	Left	Simulated with guides	5
S05	2	33	Left	Simulated with guides	4
S05	2	34	Left	Simulated with guides	5
S05	2	35	Left	Simulated with guides	4
S01	1	0	Right	Foveal	0
S01	1	1	Right	Foveal	0
S01	1	2	Right	Foveal	0
S01	1	3	Right	Foveal	0
S01	1	4	Right	Foveal	0
S01	1	5	Right	Foveal	0
S01	1	6	Right	Foveal	0
S01	1	7	Right	Foveal	0
S01	1	8	Right	Foveal	0
S01	1	9	Right	Foveal	0
S01	1	10	Right	Foveal	0
S01	1	11	Right	Foveal	0
S01	1	12	Right	Simulated	1
S01	1	13	Right	Simulated	4
S01	1	14	Right	Simulated	0
S01	1	15	Right	Simulated	2
S01	1	16	Right	Simulated	0
S01	1	17	Right	Simulated	0
S01	1	18	Right	Simulated	1
S01	1	19	Right	Simulated	1
S01	1	20	Right	Simulated	2
S01	1	21	Right	Simulated	3
S01	1	22	Right	Simulated	2
S01	1	23	Right	Simulated	0
S01	1	24	Right	Simulated with guides	1
S01	1	25	Right	Simulated with guides	1
S01	1	26	Right	Simulated with guides	0
S01	1	27	Right	Simulated with guides	0
S01	1	28	Right	Simulated with guides	0
S01	1	29	Right	Simulated with guides	0
S01	1	30	Right	Simulated with guides	1
S01	1	31	Right	Simulated with guides	1
S01	1	32	Right	Simulated with guides	0
S01	1	33	Right	Simulated with guides	0
S01	1	34	Right	Simulated with guides	1
S01	1	35	Right	Simulated with guides	0
S01	2	0	Right	Simulated with guides	0
S01	2	1	Right	Simulated with guides	0
S01	2	2	Right	Simulated with guides	1
S01	2	3	Right	Simulated with guides	1
S01	2	4	Right	Simulated with guides	0
S01	2	5	Right	Simulated with guides	0
S01	2	6	Right	Simulated with guides	0
S01	2	7	Right	Simulated with guides	1
S01	2	8	Right	Simulated with guides	1
S01	2	9	Right	Simulated with guides	0
S01	2	10	Right	Simulated with guides	0
S01	2	11	Right	Simulated with guides	0
S01	2	12	Right	Simulated	0
S01	2	13	Right	Simulated	1
S01	2	14	Right	Simulated	0
S01	2	15	Right	Simulated	0
S01	2	16	Right	Simulated	0
S01	2	17	Right	Simulated	0
S01	2	18	Right	Simulated	1
S01	2	19	Right	Simulated	2
S01	2	20	Right	Simulated	0
S01	2	21	Right	Simulated	1
S01	2	22	Right	Simulated	2
S01	2	23	Right	Simulated	1
S01	2	24	Right	Foveal	0
S01	2	25	Right	Foveal	0
S01	2	26	Right	Foveal	0
S01	2	27	Right	Foveal	0
S01	2	28	Right	Foveal	0
S01	2	29	Right	Foveal	0
S01	2	30	Right	Foveal	0
S01	2	31	Right	Foveal	0
S01	2	32	Right	Foveal	0
S01	2	33	Right	Foveal	0
S01	2	34	Right	Foveal	0
S01	2	35	Right	Foveal	0
S05	1	0	Inferior	Simulated with guides	2
S05	1	1	Inferior	Simulated with guides	1
S05	1	2	Inferior	Simulated with guides	2
S05	1	3	Inferior	Simulated with guides	1
S05	1	4	Inferior	Simulated with guides	0
S05	1	5	Inferior	Simulated with guides	1
S05	1	6	Inferior	Simulated with guides	1
S05	1	7	Inferior	Simulated with guides	1
S05	1	8	Inferior	Simulated with guides	0
S05	1	9	Inferior	Simulated with guides	1
S05	1	10	Inferior	Simulated with guides	0
S05	1	11	Inferior	Simulated with guides	1
S05	1	12	Inferior	Simulated	2
S05	1	13	Inferior	Simulated	2
S05	1	14	Inferior	Simulated	2
S05	1	15	Inferior	Simulated	3
S05	1	16	Inferior	Simulated	2
S05	1	17	Inferior	Simulated	2
S05	1	18	Inferior	Simulated	1
S05	1	19	Inferior	Simulated	2
S05	1	20	Inferior	Simulated	1
S05	1	21	Inferior	Simulated	1
S05	1	22	Inferior	Simulated	1
S05	1	23	Inferior	Simulated	1
S05	2	0	Inferior	Simulated	4
S05	2	1	Inferior	Simulated	0
S05	2	2	Inferior	Simulated	2
S05	2	3	Inferior	Simulated	1
S05	2	4	Inferior	Simulated	1
S05	2	5	Inferior	Simulated	2
S05	2	6	Inferior	Simulated	4
S05	2	7	Inferior	Simulated	2
S05	2	8	Inferior	Simulated	0
S05	2	9	Inferior	Simulated	1
S05	2	10	Inferior	Simulated	1
S05	2	11	Inferior	Simulated	0
S05	2	12	Inferior	Simulated with guides	0
S05	2	13	Inferior	Simulated with guides	0
S05	2	14	Inferior	Simulated with guides	0
S05	2	15	Inferior	Simulated with guides	1
S05	2	16	Inferior	Simulated with guides	0
S05	2	17	Inferior	Simulated with guides	0
S05	2	18	Inferior	Simulated with guides	1
S05	2	19	Inferior	Simulated with guides	0
S05	2	20	Inferior	Simulated with guides	0
S05	2	21	Inferior	Simulated with guides	1
S05	2	22	Inferior	Simulated with guides	0
S05	2	23	Inferior	Simulated with guides	0
S02	2	0	Inferior	Foveal	0
S02	2	1	Inferior	Foveal	0
S02	2	2	Inferior	Foveal	0
S02	2	3	Inferior	Foveal	0
S02	2	4	Inferior	Foveal	0
S02	2	5	Inferior	Foveal	0
S02	2	6	Inferior	Foveal	0
S02	2	7	Inferior	Foveal	0
S02	2	8	Inferior	Foveal	0
S02	2	9	Inferior	Foveal	0
S02	2	10	Inferior	Foveal	0
S02	2	11	Inferior	Foveal	0
S02	2	12	Inferior	Simulated with guides	1
S02	2	13	Inferior	Simulated with guides	1
S02	2	14	Inferior	Simulated with guides	2
S02	2	15	Inferior	Simulated with guides	0
S02	2	16	Inferior	Simulated with guides	0
S02	2	17	Inferior	Simulated with guides	0
S02	2	18	Inferior	Simulated with guides	1
S02	2	19	Inferior	Simulated with guides	1
S02	2	20	Inferior	Simulated with guides	1
S02	2	21	Inferior	Simulated with guides	1
S02	2	22	Inferior	Simulated with guides	1
S02	2	23	Inferior	Simulated with guides	2
S02	2	24	Inferior	Simulated	2
S02	2	25	Inferior	Simulated	0
S02	2	26	Inferior	Simulated	0
S02	2	27	Inferior	Simulated	3
S02	2	28	Inferior	Simulated	2
S02	2	29	Inferior	Simulated	0
S02	2	30	Inferior	Simulated	1
S02	2	31	Inferior	Simulated	1
S02	2	32	Inferior	Simulated	0
S02	2	33	Inferior	Simulated	1
S02	2	34	Inferior	Simulated	2
S02	2	35	Inferior	Simulated	1
S02	1	0	Inferior	Simulated	1
S02	1	1	Inferior	Simulated	0
S02	1	2	Inferior	Simulated	0
S02	1	3	Inferior	Simulated	0
S02	1	4	Inferior	Simulated	0
S02	1	5	Inferior	Simulated	2
S02	1	6	Inferior	Simulated	2
S02	1	7	Inferior	Simulated	1
S02	1	8	Inferior	Simulated	0
S02	1	9	Inferior	Simulated	2
S02	1	10	Inferior	Simulated	1
S02	1	11	Inferior	Simulated	0
S02	1	12	Inferior	Simulated with guides	0
S02	1	13	Inferior	Simulated with guides	0
S02	1	14	Inferior	Simulated with guides	0
S02	1	15	Inferior	Simulated with guides	1
S02	1	16	Inferior	Simulated with guides	1
S02	1	17	Inferior	Simulated with guides	1
S02	1	18	Inferior	Simulated with guides	1
S02	1	19	Inferior	Simulated with guides	0
S02	1	20	Inferior	Simulated with guides	0
S02	1	21	Inferior	Simulated with guides	0
S02	1	22	Inferior	Simulated with guides	0
S02	1	23	Inferior	Simulated with guides	1
S02	1	24	Inferior	Foveal	0
S02	1	25	Inferior	Foveal	0
S02	1	26	Inferior	Foveal	0
S02	1	27	Inferior	Foveal	0
S02	1	28	Inferior	Foveal	0
S02	1	29	Inferior	Foveal	0
S02	1	30	Inferior	Foveal	0
S02	1	31	Inferior	Foveal	0
S02	1	32	Inferior	Foveal	0
S02	1	33	Inferior	Foveal	0
S02	1	34	Inferior	Foveal	0
S02	1	35	Inferior	Foveal	0
S01	1	0	Left	Simulated with guides	1
S01	1	1	Left	Simulated with guides	2
S01	1	2	Left	Simulated with guides	0
S01	1	3	Left	Simulated with guides	0
S01	1	4	Left	Simulated with guides	0
S01	1	5	Left	Simulated with guides	2
S01	1	6	Left	Simulated with guides	0
S01	1	7	Left	Simulated with guides	0
S01	1	8	Left	Simulated with guides	0
S01	1	9	Left	Simulated with guides	1
S01	1	10	Left	Simulated with guides	1
S01	1	11	Left	Simulated with guides	1
S01	1	12	Left	Simulated	0
S01	1	13	Left	Simulated	0
S01	1	14	Left	Simulated	2
S01	1	15	Left	Simulated	0
S01	1	16	Left	Simulated	1
S01	1	17	Left	Simulated	0
S01	1	18	Left	Simulated	0
S01	1	19	Left	Simulated	0
S01	1	20	Left	Simulated	0
S01	1	21	Left	Simulated	1
S01	1	22	Left	Simulated	0
S01	1	23	Left	Simulated	0
S01	2	0	Left	Simulated	1
S01	2	1	Left	Simulated	1
S01	2	2	Left	Simulated	0
S01	2	3	Left	Simulated	0
S01	2	4	Left	Simulated	0
S01	2	5	Left	Simulated	0
S01	2	6	Left	Simulated	0
S01	2	7	Left	Simulated	0
S01	2	8	Left	Simulated	0
S01	2	9	Left	Simulated	2
S01	2	10	Left	Simulated	1
S01	2	11	Left	Simulated	0
S01	2	12	Left	Simulated with guides	0
S01	2	13	Left	Simulated with guides	0
S01	2	14	Left	Simulated with guides	0
S01	2	15	Left	Simulated with guides	1
S01	2	16	Left	Simulated with guides	0
S01	2	17	Left	Simulated with guides	0
S01	2	18	Left	Simulated with guides	0
S01	2	19	Left	Simulated with guides	1
S01	2	20	Left	Simulated with guides	0
S01	2	21	Left	Simulated with guides	0
S01	2	22	Left	Simulated with guides	1
S01	2	23	Left	Simulated with guides	0
S02	1	0	Left	Simulated with guides	1
S02	1	1	Left	Simulated with guides	1
S02	1	2	Left	Simulated with guides	2
S02	1	3	Left	Simulated with guides	2
S02	1	4	Left	Simulated with guides	1
S02	1	5	Left	Simulated with guides	1
S02	1	6	Left	Simulated with guides	2
S02	1	7	Left	Simulated with guides	1
S02	1	8	Left	Simulated with guides	1
S02	1	9	Left	Simulated with guides	2
S02	1	10	Left	Simulated with guides	1
S02	1	11	Left	Simulated with guides	1
S02	1	12	Left	Simulated	2
S02	1	13	Left	Simulated	3
S02	1	14	Left	Simulated	2
S02	1	15	Left	Simulated	3
S02	1	16	Left	Simulated	1
S02	1	17	Left	Simulated	0
S02	1	18	Left	Simulated	0
S02	1	19	Left	Simulated	0
S02	1	20	Left	Simulated	1
S02	1	21	Left	Simulated	0
S02	1	22	Left	Simulated	1
S02	1	23	Left	Simulated	1
S02	2	0	Left	Simulated	0
S02	2	1	Left	Simulated	3
S02	2	2	Left	Simulated	1
S02	2	3	Left	Simulated	0
S02	2	4	Left	Simulated	0
S02	2	5	Left	Simulated	3
S02	2	6	Left	Simulated	1
S02	2	7	Left	Simulated	2
S02	2	8	Left	Simulated	2
S02	2	9	Left	Simulated	2
S02	2	10	Left	Simulated	1
S02	2	11	Left	Simulated	2
S02	2	12	Left	Simulated with guides	1
S02	2	13	Left	Simulated with guides	2
S02	2	14	Left	Simulated with guides	2
S02	2	15	Left	Simulated with guides	0
S02	2	16	Left	Simulated with guides	1
S02	2	17	Left	Simulated with guides	2
S02	2	18	Left	Simulated with guides	3
S02	2	19	Left	Simulated with guides	1
S02	2	20	Left	Simulated with guides	0
S02	2	21	Left	Simulated with guides	2
S02	2	22	Left	Simulated with guides	1
S02	2	23	Left	Simulated with guides	0
S04	1	0	Left	Simulated with guides	2
S04	1	1	Left	Simulated with guides	2
S04	1	2	Left	Simulated with guides	2
S04	1	3	Left	Simulated with guides	3
S04	1	4	Left	Simulated with guides	3
S04	1	5	Left	Simulated with guides	0
S04	1	6	Left	Simulated with guides	2
S04	1	7	Left	Simulated with guides	3
S04	1	8	Left	Simulated with guides	1
S04	1	9	Left	Simulated with guides	5
S04	1	10	Left	Simulated with guides	3
S04	1	11	Left	Simulated with guides	1
S04	1	12	Left	Simulated	2
S04	1	13	Left	Simulated	1
S04	1	14	Left	Simulated	2
S04	1	15	Left	Simulated	2
S04	1	16	Left	Simulated	0
S04	1	17	Left	Simulated	3
S04	1	18	Left	Simulated	2
S04	1	19	Left	Simulated	0
S04	1	20	Left	Simulated	1
S04	1	21	Left	Simulated	1
S04	1	22	Left	Simulated	2
S04	1	23	Left	Simulated	1
S04	1	24	Left	Foveal	0
S04	1	25	Left	Foveal	0
S04	1	26	Left	Foveal	0
S04	1	27	Left	Foveal	0
S04	1	28	Left	Foveal	0
S04	1	29	Left	Foveal	0
S04	1	30	Left	Foveal	0
S04	1	31	Left	Foveal	0
S04	1	32	Left	Foveal	0
S04	1	33	Left	Foveal	0
S04	1	34	Left	Foveal	0
S04	1	35	Left	Foveal	0
S04	2	0	Left	Foveal	0
S04	2	1	Left	Foveal	0
S04	2	2	Left	Foveal	0
S04	2	3	Left	Foveal	0
S04	2	4	Left	Foveal	0
S04	2	5	Left	Foveal	0
S04	2	6	Left	Foveal	0
S04	2	7	Left	Foveal	0
S04	2	8	Left	Foveal	0
S04	2	9	Left	Foveal	0
S04	2	10	Left	Foveal	0
S04	2	11	Left	Foveal	0
S04	2	12	Left	Simulated	3
S04	2	13	Left	Simulated	3
S04	2	14	Left	Simulated	2
S04	2	15	Left	Simulated	0
S04	2	16	Left	Simulated	0
S04	2	17	Left	Simulated	2
S04	2	18	Left	Simulated	2
S04	2	19	Left	Simulated	0
S04	2	20	Left	Simulated	1
S04	2	21	Left	Simulated	3
S04	2	22	Left	Simulated	3
S04	2	23	Left	Simulated	1
S04	2	24	Left	Simulated with guides	3
S04	2	25	Left	Simulated with guides	3
S04	2	26	Left	Simulated with guides	1
S04	2	27	Left	Simulated with guides	2
S04	2	28	Left	Simulated with guides	1
S04	2	29	Left	Simulated with guides	2
S04	2	30	Left	Simulated with guides	1
S04	2	31	Left	Simulated with guides	2
S04	2	32	Left	Simulated with guides	2
S04	2	33	Left	Simulated with guides	0
S04	2	34	Left	Simulated with guides	0
S04	2	35	Left	Simulated with guides	4
S04	1	0	Right	Simulated with guides	4
S04	1	1	Right	Simulated with guides	1
S04	1	2	Right	Simulated with guides	1
S04	1	3	Right	Simulated with guides	4
S04	1	4	Right	Simulated with guides	1
S04	1	5	Right	Simulated with guides	1
S04	1	6	Right	Simulated with guides	4
S04	1	7	Right	Simulated with guides	1
S04	1	8	Right	Simulated with guides	2
S04	1	9	Right	Simulated with guides	1
S04	1	10	Right	Simulated with guides	1
S04	1	11	Right	Simulated with guides	4
S04	1	12	Right	Simulated	5
S04	1	13	Right	Simulated	4
S04	1	14	Right	Simulated	2
S04	1	15	Right	Simulated	2
S04	1	16	Right	Simulated	2
S04	1	17	Right	Simulated	1
S04	1	18	Right	Simulated	0
S04	1	19	Right	Simulated	2
S04	1	20	Right	Simulated	2
S04	1	21	Right	Simulated	2
S04	1	22	Right	Simulated	1
S04	1	23	Right	Simulated	0
S04	2	0	Right	Simulated	3
S04	2	1	Right	Simulated	1
S04	2	2	Right	Simulated	3
S04	2	3	Right	Simulated	1
S04	2	4	Right	Simulated	6
S04	2	5	Right	Simulated	2
S04	2	6	Right	Simulated	2
S04	2	7	Right	Simulated	3
S04	2	8	Right	Simulated	3
S04	2	9	Right	Simulated	3
S04	2	10	Right	Simulated	3
S04	2	11	Right	Simulated	4
S04	2	12	Right	Simulated with guides	3
S04	2	13	Right	Simulated with guides	1
S04	2	14	Right	Simulated with guides	3
S04	2	15	Right	Simulated with guides	2
S04	2	16	Right	Simulated with guides	2
S04	2	17	Right	Simulated with guides	4
S04	2	18	Right	Simulated with guides	1
S04	2	19	Right	Simulated with guides	0
S04	2	20	Right	Simulated with guides	1
S04	2	21	Right	Simulated with guides	1
S04	2	22	Right	Simulated with guides	1
S04	2	23	Right	Simulated with guides	4
S02	1	0	Right	Simulated	1
S02	1	1	Right	Simulated	1
S02	1	2	Right	Simulated	1
S02	1	3	Right	Simulated	0
S02	1	4	Right	Simulated	1
S02	1	5	Right	Simulated	1
S02	1	6	Right	Simulated	2
S02	1	7	Right	Simulated	1
S02	1	8	Right	Simulated	0
S02	1	9	Right	Simulated	1
S02	1	10	Right	Simulated	2
S02	1	11	Right	Simulated	3
S02	1	12	Right	Simulated with guides	1
S02	1	13	Right	Simulated with guides	0
S02	1	14	Right	Simulated with guides	1
S02	1	15	Right	Simulated with guides	1
S02	1	16	Right	Simulated with guides	0
S02	1	17	Right	Simulated with guides	0
S02	1	18	Right	Simulated with guides	2
S02	1	19	Right	Simulated with guides	1
S02	1	20	Right	Simulated with guides	0
S02	1	21	Right	Simulated with guides	1
S02	1	22	Right	Simulated with guides	3
S02	1	23	Right	Simulated with guides	1
S02	2	0	Right	Simulated with guides	1
S02	2	1	Right	Simulated with guides	2
S02	2	2	Right	Simulated with guides	0
S02	2	3	Right	Simulated with guides	1
S02	2	4	Right	Simulated with guides	0
S02	2	5	Right	Simulated with guides	0
S02	2	6	Right	Simulated with guides	0
S02	2	7	Right	Simulated with guides	0
S02	2	8	Right	Simulated with guides	1
S02	2	9	Right	Simulated with guides	0
S02	2	10	Right	Simulated with guides	1
S02	2	11	Right	Simulated with guides	1
S02	2	12	Right	Simulated	0
S02	2	13	Right	Simulated	0
S02	2	14	Right	Simulated	1
S02	2	15	Right	Simulated	3
S02	2	16	Right	Simulated	1
S02	2	17	Right	Simulated	0
S02	2	18	Right	Simulated	2
S02	2	19	Right	Simulated	1
S02	2	20	Right	Simulated	2
S02	2	21	Right	Simulated	2
S02	2	22	Right	Simulated	1
S02	2	23	Right	Simulated	1
S05	1	0	Right	Simulated with guides	2
S05	1	1	Right	Simulated with guides	2
S05	1	2	Right	Simulated with guides	2
S05	1	3	Right	Simulated with guides	3
S05	1	4	Right	Simulated with guides	2
S05	1	5	Right	Simulated with guides	0
S05	1	6	Right	Simulated with guides	3
S05	1	7	Right	Simulated with guides	5
S05	1	8	Right	Simulated with guides	0
S05	1	9	Right	Simulated with guides	3
S05	1	10	Right	Simulated with guides	2
S05	1	11	Right	Simulated with guides	1
S05	1	12	Right	Simulated	0
S05	1	13	Right	Simulated	2
S05	1	14	Right	Simulated	0
S05	1	15	Right	Simulated	2
S05	1	16	Right	Simulated	0
S05	1	17	Right	Simulated	2
S05	1	18	Right	Simulated	1
S05	1	19	Right	Simulated	1
S05	1	20	Right	Simulated	4
S05	1	21	Right	Simulated	2
S05	1	22	Right	Simulated	1
S05	1	23	Right	Simulated	1
S05	2	0	Right	Simulated	3
S05	2	1	Right	Simulated	2
S05	2	2	Right	Simulated	0
S05	2	3	Right	Simulated	1
S05	2	4	Right	Simulated	2
S05	2	5	Right	Simulated	0
S05	2	6	Right	Simulated	1
S05	2	7	Right	Simulated	1
S05	2	8	Right	Simulated	1
S05	2	9	Right	Simulated	2
S05	2	10	Right	Simulated	1
S05	2	11	Right	Simulated	0
S05	2	12	Right	Simulated with guides	0
S05	2	13	Right	Simulated with guides	1
S05	2	14	Right	Simulated with guides	1
S05	2	15	Right	Simulated with guides	1
S05	2	16	Right	Simulated with guides	0
S05	2	17	Right	Simulated with guides	2
S05	2	18	Right	Simulated with guides	0
S05	2	19	Right	Simulated with guides	0
S05	2	20	Right	Simulated with guides	1
S05	2	21	Right	Simulated with guides	0
S05	2	22	Right	Simulated with guides	1
S05	2	23	Right	Simulated with guides	2
S01	1	0	Inferior	Simulated	1
S01	1	1	Inferior	Simulated	1
S01	1	2	Inferior	Simulated	0
S01	1	3	Inferior	Simulated	0
S01	1	4	Inferior	Simulated	2
S01	1	5	Inferior	Simulated	0
S01	1	6	Inferior	Simulated	1
S01	1	7	Inferior	Simulated	1
S01	1	8	Inferior	Simulated	0
S01	1	9	Inferior	Simulated	0
S01	1	10	Inferior	Simulated	0
S01	1	11	Inferior	Simulated	1
S01	1	12	Inferior	Simulated with guides	0
S01	1	13	Inferior	Simulated with guides	1
S01	1	14	Inferior	Simulated with guides	0
S01	1	15	Inferior	Simulated with guides	1
S01	1	16	Inferior	Simulated with guides	1
S01	1	17	Inferior	Simulated with guides	0
S01	1	18	Inferior	Simulated with guides	3
S01	1	19	Inferior	Simulated with guides	0
S01	1	20	Inferior	Simulated with guides	1
S01	1	21	Inferior	Simulated with guides	1
S01	1	22	Inferior	Simulated with guides	0
S01	1	23	Inferior	Simulated with guides	0
S01	2	0	Inferior	Simulated with guides	0
S01	2	1	Inferior	Simulated with guides	0
S01	2	2	Inferior	Simulated with guides	2
S01	2	3	Inferior	Simulated with guides	0
S01	2	4	Inferior	Simulated with guides	0
S01	2	5	Inferior	Simulated with guides	0
S01	2	6	Inferior	Simulated with guides	2
S01	2	7	Inferior	Simulated with guides	0
S01	2	8	Inferior	Simulated with guides	0
S01	2	9	Inferior	Simulated with guides	0
S01	2	10	Inferior	Simulated with guides	0
S01	2	11	Inferior	Simulated with guides	0
S01	2	12	Inferior	Simulated	1
S01	2	13	Inferior	Simulated	0
S01	2	14	Inferior	Simulated	1
S01	2	15	Inferior	Simulated	0
S01	2	16	Inferior	Simulated	0
S01	2	17	Inferior	Simulated	0
S01	2	18	Inferior	Simulated	1
S01	2	19	Inferior	Simulated	0
S01	2	20	Inferior	Simulated	0
S01	2	21	Inferior	Simulated	1
S01	2	22	Inferior	Simulated	0
S01	2	23	Inferior	Simulated	0
S04	1	0	Inferior	Simulated with guides	1
S04	1	1	Inferior	Simulated with guides	0
S04	1	2	Inferior	Simulated with guides	2
S04	1	3	Inferior	Simulated with guides	0
S04	1	4	Inferior	Simulated with guides	0
S04	1	5	Inferior	Simulated with guides	0
S04	1	6	Inferior	Simulated with guides	0
S04	1	7	Inferior	Simulated with guides	0
S04	1	8	Inferior	Simulated with guides	1
S04	1	9	Inferior	Simulated with guides	1
S04	1	10	Inferior	Simulated with guides	0
S04	1	11	Inferior	Simulated with guides	1
S04	1	12	Inferior	Simulated	0
S04	1	13	Inferior	Simulated	0
S04	1	14	Inferior	Simulated	1
S04	1	15	Inferior	Simulated	1
S04	1	16	Inferior	Simulated	0
S04	1	17	Inferior	Simulated	0
S04	1	18	Inferior	Simulated	1
S04	1	19	Inferior	Simulated	0
S04	1	20	Inferior	Simulated	0
S04	1	21	Inferior	Simulated	1
S04	1	22	Inferior	Simulated	0
S04	1	23	Inferior	Simulated	0
S04	2	0	Inferior	Simulated	0
S04	2	1	Inferior	Simulated	1
S04	2	2	Inferior	Simulated	0
S04	2	3	Inferior	Simulated	1
S04	2	4	Inferior	Simulated	0
S04	2	5	Inferior	Simulated	0
S04	2	6	Inferior	Simulated	2
S04	2	7	Inferior	Simulated	0
S04	2	8	Inferior	Simulated	0
S04	2	9	Inferior	Simulated	1
S04	2	10	Inferior	Simulated	1
S04	2	11	Inferior	Simulated	0
S04	2	12	Inferior	Simulated with guides	0
S04	2	13	Inferior	Simulated with guides	1
S04	2	14	Inferior	Simulated with guides	2
S04	2	15	Inferior	Simulated with guides	0
S04	2	16	Inferior	Simulated with guides	0
S04	2	17	Inferior	Simulated with guides	2
S04	2	18	Inferior	Simulated with guides	0
S04	2	19	Inferior	Simulated with guides	0
S04	2	20	Inferior	Simulated with guides	0
S04	2	21	Inferior	Simulated with guides	2
S04	2	22	Inferior	Simulated with guides	0
S04	2	23	Inferior	Simulated with guides	1
S03	1	0	Inferior	Simulated	3
S03	1	1	Inferior	Simulated	1
S03	1	2	Inferior	Simulated	3
S03	1	3	Inferior	Simulated	1
S03	1	4	Inferior	Simulated	1
S03	1	5	Inferior	Simulated	2
S03	1	6	Inferior	Simulated	2
S03	1	7	Inferior	Simulated	2
S03	1	8	Inferior	Simulated	1
S03	1	9	Inferior	Simulated	2
S03	1	10	Inferior	Simulated	0
S03	1	11	Inferior	Simulated	7
S03	1	12	Inferior	Simulated with guides	0
S03	1	13	Inferior	Simulated with guides	2
S03	1	14	Inferior	Simulated with guides	0
S03	1	15	Inferior	Simulated with guides	0
S03	1	16	Inferior	Simulated with guides	0
S03	1	17	Inferior	Simulated with guides	2
S03	1	18	Inferior	Simulated with guides	1
S03	1	19	Inferior	Simulated with guides	0
S03	1	20	Inferior	Simulated with guides	0
S03	1	21	Inferior	Simulated with guides	2
S03	1	22	Inferior	Simulated with guides	2
S03	1	23	Inferior	Simulated with guides	3
S03	1	24	Inferior	Foveal	0
S03	1	25	Inferior	Foveal	0
S03	1	26	Inferior	Foveal	0
S03	1	27	Inferior	Foveal	0
S03	1	28	Inferior	Foveal	0
S03	1	29	Inferior	Foveal	0
S03	1	30	Inferior	Foveal	0
S03	1	31	Inferior	Foveal	0
S03	1	32	Inferior	Foveal	0
S03	1	33	Inferior	Foveal	0
S03	1	34	Inferior	Foveal	0
S03	1	35	Inferior	Foveal	0
S03	2	0	Inferior	Foveal	0
S03	2	1	Inferior	Foveal	0
S03	2	2	Inferior	Foveal	0
S03	2	3	Inferior	Foveal	0
S03	2	4	Inferior	Foveal	0
S03	2	5	Inferior	Foveal	0
S03	2	6	Inferior	Foveal	0
S03	2	7	Inferior	Foveal	0
S03	2	8	Inferior	Foveal	0
S03	2	9	Inferior	Foveal	0
S03	2	10	Inferior	Foveal	0
S03	2	11	Inferior	Foveal	0
S03	2	12	Inferior	Simulated with guides	0
S03	2	13	Inferior	Simulated with guides	0
S03	2	14	Inferior	Simulated with guides	0
S03	2	15	Inferior	Simulated with guides	1
S03	2	16	Inferior	Simulated with guides	1
S03	2	17	Inferior	Simulated with guides	0
S03	2	18	Inferior	Simulated with guides	0
S03	2	19	Inferior	Simulated with guides	0
S03	2	20	Inferior	Simulated with guides	0
S03	2	21	Inferior	Simulated with guides	0
S03	2	22	Inferior	Simulated with guides	0
S03	2	23	Inferior	Simulated with guides	1
S03	2	24	Inferior	Simulated	1
S03	2	25	Inferior	Simulated	2
S03	2	26	Inferior	Simulated	3
S03	2	27	Inferior	Simulated	0
S03	2	28	Inferior	Simulated	1
S03	2	29	Inferior	Simulated	1
S03	2	30	Inferior	Simulated	1
S03	2	31	Inferior	Simulated	0
S03	2	32	Inferior	Simulated	0
S03	2	33	Inferior	Simulated	0
S03	2	34	Inferior	Simulated	0
S03	2	35	Inferior	Simulated	4
S03	1	0	Right	Simulated	2
S03	1	1	Right	Simulated	2
S03	1	2	Right	Simulated	0
S03	1	3	Right	Simulated	0
S03	1	4	Right	Simulated	1
S03	1	5	Right	Simulated	0
S03	1	6	Right	Simulated	0
S03	1	7	Right	Simulated	1
S03	1	8	Right	Simulated	2
S03	1	9	Right	Simulated	1
S03	1	10	Right	Simulated	1
S03	1	11	Right	Simulated	1
S03	1	12	Right	Simulated with guides	3
S03	1	13	Right	Simulated with guides	0
S03	1	14	Right	Simulated with guides	0
S03	1	15	Right	Simulated with guides	1
S03	1	16	Right	Simulated with guides	0
S03	1	17	Right	Simulated with guides	2
S03	1	18	Right	Simulated with guides	2
S03	1	19	Right	Simulated with guides	5
S03	1	20	Right	Simulated with guides	2
S03	1	21	Right	Simulated with guides	2
S03	1	22	Right	Simulated with guides	1
S03	1	23	Right	Simulated with guides	3
S03	2	0	Right	Simulated with guides	4
S03	2	1	Right	Simulated with guides	1
S03	2	2	Right	Simulated with guides	1
S03	2	3	Right	Simulated with guides	1
S03	2	4	Right	Simulated with guides	2
S03	2	5	Right	Simulated with guides	2
S03	2	6	Right	Simulated with guides	1
S03	2	7	Right	Simulated with guides	1
S03	2	8	Right	Simulated with guides	1
S03	2	9	Right	Simulated with guides	0
S03	2	10	Right	Simulated with guides	2
S03	2	11	Right	Simulated with guides	3
S03	2	12	Right	Simulated	3
S03	2	13	Right	Simulated	2
S03	2	14	Right	Simulated	4
S03	2	15	Right	Simulated	1
S03	2	16	Right	Simulated	1
S03	2	17	Right	Simulated	1
S03	2	18	Right	Simulated	1
S03	2	19	Right	Simulated	2
S03	2	20	Right	Simulated	1
S03	2	21	Right	Simulated	2
S03	2	22	Right	Simulated	1
S03	2	23	Right	Simulated	5
S03	1	0	Left	Simulated	3
S03	1	1	Left	Simulated	3
S03	1	2	Left	Simulated	0
S03	1	3	Left	Simulated	1
S03	1	4	Left	Simulated	3
S03	1	5	Left	Simulated	4
S03	1	6	Left	Simulated	2
S03	1	7	Left	Simulated	2
S03	1	8	Left	Simulated	5
S03	1	9	Left	Simulated	3
S03	1	10	Left	Simulated	2
S03	1	11	Left	Simulated	0
S03	1	12	Left	Simulated with guides	4
S03	1	13	Left	Simulated with guides	7
S03	1	14	Left	Simulated with guides	3
S03	1	15	Left	Simulated with guides	2
S03	1	16	Left	Simulated with guides	3
S03	1	17	Left	Simulated with guides	1
S03	1	18	Left	Simulated with guides	3
S03	1	19	Left	Simulated with guides	1
S03	1	20	Left	Simulated with guides	0
S03	1	21	Left	Simulated with guides	2
S03	1	22	Left	Simulated with guides	3
S03	1	23	Left	Simulated with guides	2
S03	2	0	Left	Simulated with guides	3
S03	2	1	Left	Simulated with guides	2
S03	2	2	Left	Simulated with guides	4
S03	2	3	Left	Simulated with guides	3
S03	2	4	Left	Simulated with guides	2
S03	2	5	Left	Simulated with guides	4
S03	2	6	Left	Simulated with guides	3
S03	2	7	Left	Simulated with guides	1
S03	2	8	Left	Simulated with guides	0
S03	2	9	Left	Simulated with guides	2
S03	2	10	Left	Simulated with guides	3
S03	2	11	Left	Simulated with guides	2
S03	2	12	Left	Simulated	5
S03	2	13	Left	Simulated	3
S03	2	14	Left	Simulated	6
S03	2	15	Left	Simulated	7
S03	2	16	Left	Simulated	0
S03	2	17	Left	Simulated	1
S03	2	18	Left	Simulated	3
S03	2	19	Left	Simulated	3
S03	2	20	Left	Simulated	3
S03	2	21	Left	Simulated	1
S03	2	22	Left	Simulated	0
S03	2	23	Left	Simulated	1
S06	1	0	Right	Simulated with guides	4
S06	1	1	Right	Simulated with guides	0
S06	1	2	Right	Simulated with guides	2
S06	1	3	Right	Simulated with guides	1
S06	1	4	Right	Simulated with guides	0
S06	1	5	Right	Simulated with guides	0
S06	1	6	Right	Simulated with guides	3
S06	1	7	Right	Simulated with guides	2
S06	1	8	Right	Simulated with guides	2
S06	1	9	Right	Simulated with guides	1
S06	1	10	Right	Simulated with guides	2
S06	1	11	Right	Simulated with guides	2
S06	1	12	Right	Simulated	1
S06	1	13	Right	Simulated	0
S06	1	14	Right	Simulated	0
S06	1	15	Right	Simulated	2
S06	1	16	Right	Simulated	1
S06	1	17	Right	Simulated	0
S06	1	18	Right	Simulated	0
S06	1	19	Right	Simulated	0
S06	1	20	Right	Simulated	3
S06	1	21	Right	Simulated	2
S06	1	22	Right	Simulated	1
S06	1	23	Right	Simulated	0
S06	1	24	Right	Foveal	0
S06	1	25	Right	Foveal	0
S06	1	26	Right	Foveal	0
S06	1	27	Right	Foveal	0
S06	1	28	Right	Foveal	0
S06	1	29	Right	Foveal	0
S06	1	30	Right	Foveal	0
S06	1	31	Right	Foveal	0
S06	1	32	Right	Foveal	0
S06	1	33	Right	Foveal	0
S06	1	34	Right	Foveal	0
S06	1	35	Right	Foveal	0
S06	2	0	Right	Foveal	0
S06	2	1	Right	Foveal	0
S06	2	2	Right	Foveal	0
S06	2	3	Right	Foveal	0
S06	2	4	Right	Foveal	0
S06	2	5	Right	Foveal	0
S06	2	6	Right	Foveal	0
S06	2	7	Right	Foveal	0
S06	2	8	Right	Foveal	0
S06	2	9	Right	Foveal	0
S06	2	10	Right	Foveal	0
S06	2	11	Right	Foveal	0
S06	2	12	Right	Simulated	0
S06	2	13	Right	Simulated	1
S06	2	14	Right	Simulated	0
S06	2	15	Right	Simulated	0
S06	2	16	Right	Simulated	0
S06	2	17	Right	Simulated	1
S06	2	18	Right	Simulated	1
S06	2	19	Right	Simulated	0
S06	2	20	Right	Simulated	0
S06	2	21	Right	Simulated	1
S06	2	22	Right	Simulated	0
S06	2	23	Right	Simulated	1
S06	2	24	Right	Simulated with guides	0
S06	2	25	Right	Simulated with guides	0
S06	2	26	Right	Simulated with guides	0
S06	2	27	Right	Simulated with guides	0
S06	2	28	Right	Simulated with guides	0
S06	2	29	Right	Simulated with guides	0
S06	2	30	Right	Simulated with guides	0
S06	2	31	Right	Simulated with guides	1
S06	2	32	Right	Simulated with guides	0
S06	2	33	Right	Simulated with guides	0
S06	2	34	Right	Simulated with guides	0
S06	2	35	Right	Simulated with guides	1
S06	1	0	Inferior	Simulated with guides	1
S06	1	1	Inferior	Simulated with guides	0
S06	1	2	Inferior	Simulated with guides	0
S06	1	3	Inferior	Simulated with guides	0
S06	1	4	Inferior	Simulated with guides	0
S06	1	5	Inferior	Simulated with guides	0
S06	1	6	Inferior	Simulated with guides	0
S06	1	7	Inferior	Simulated with guides	0
S06	1	8	Inferior	Simulated with guides	0
S06	1	9	Inferior	Simulated with guides	0
S06	1	10	Inferior	Simulated with guides	0
S06	1	11	Inferior	Simulated with guides	0
S06	1	12	Inferior	Simulated	0
S06	1	13	Inferior	Simulated	1
S06	1	14	Inferior	Simulated	1
S06	1	15	Inferior	Simulated	0
S06	1	16	Inferior	Simulated	0
S06	1	17	Inferior	Simulated	1
S06	1	18	Inferior	Simulated	3
S06	1	19	Inferior	Simulated	0
S06	1	20	Inferior	Simulated	1
S06	1	21	Inferior	Simulated	0
S06	1	22	Inferior	Simulated	1
S06	1	23	Inferior	Simulated	1
S06	2	0	Inferior	Simulated	1
S06	2	1	Inferior	Simulated	0
S06	2	2	Inferior	Simulated	1
S06	2	3	Inferior	Simulated	0
S06	2	4	Inferior	Simulated	2
S06	2	5	Inferior	Simulated	0
S06	2	6	Inferior	Simulated	1
S06	2	7	Inferior	Simulated	0
S06	2	8	Inferior	Simulated	0
S06	2	9	Inferior	Simulated	0
S06	2	10	Inferior	Simulated	1
S06	2	11	Inferior	Simulated	0
S06	2	12	Inferior	Simulated with guides	0
S06	2	13	Inferior	Simulated with guides	0
S06	2	14	Inferior	Simulated with guides	0
S06	2	15	Inferior	Simulated with guides	0
S06	2	16	Inferior	Simulated with guides	0
S06	2	17	Inferior	Simulated with guides	1
S06	2	18	Inferior	Simulated with guides	0
S06	2	19	Inferior	Simulated with guides	0
S06	2	20	Inferior	Simulated with guides	0
S06	2	21	Inferior	Simulated with guides	0
S06	2	22	Inferior	Simulated with guides	0
S06	2	23	Inferior	Simulated with guides	0
S06	1	0	Left	Simulated with guides	0
S06	1	1	Left	Simulated with guides	0
S06	1	2	Left	Simulated with guides	1
S06	1	3	Left	Simulated with guides	0
S06	1	4	Left	Simulated with guides	0
S06	1	5	Left	Simulated with guides	0
S06	1	6	Left	Simulated with guides	0
S06	1	7	Left	Simulated with guides	0
S06	1	8	Left	Simulated with guides	1
S06	1	9	Left	Simulated with guides	0
S06	1	10	Left	Simulated with guides	0
S06	1	11	Left	Simulated with guides	0
S06	1	12	Left	Simulated	0
S06	1	13	Left	Simulated	0
S06	1	14	Left	Simulated	0
S06	1	15	Left	Simulated	0
S06	1	16	Left	Simulated	2
S06	1	17	Left	Simulated	1
S06	1	18	Left	Simulated	0
S06	1	19	Left	Simulated	1
S06	1	20	Left	Simulated	1
S06	1	21	Left	Simulated	0
S06	1	22	Left	Simulated	0
S06	1	23	Left	Simulated	0
S06	2	0	Left	Simulated	0
S06	2	1	Left	Simulated	0
S06	2	2	Left	Simulated	0
S06	2	3	Left	Simulated	0
S06	2	4	Left	Simulated	0
S06	2	5	Left	Simulated	0
S06	2	6	Left	Simulated	1
S06	2	7	Left	Simulated	0
S06	2	8	Left	Simulated	1
S06	2	9	Left	Simulated	0
S06	2	10	Left	Simulated	1
S06	2	11	Left	Simulated	3
S06	2	12	Left	Simulated with guides	1
S06	2	13	Left	Simulated with guides	0
S06	2	14	Left	Simulated with guides	0
S06	2	15	Left	Simulated with guides	1
S06	2	16	Left	Simulated with guides	0
S06	2	17	Left	Simulated with guides	1
S06	2	18	Left	Simulated with guides	0
S06	2	19	Left	Simulated with guides	1
S06	2	20	Left	Simulated with guides	0
S06	2	21	Left	Simulated with guides	1
S06	2	22	Left	Simulated with guides	2
S06	2	23	Left	Simulated with guides	0
