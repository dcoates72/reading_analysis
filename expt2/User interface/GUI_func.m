function varargout = GUI_func(varargin)
% GUI_FUNC MATLAB code for GUI_func.fig
%      GUI_FUNC, by itself, creates a new GUI_FUNC or raises the existing
%      singleton*.
%
%      H = GUI_FUNC returns the handle to a new GUI_FUNC or the handle to
%      the existing singleton*.
%
%      GUI_FUNC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_FUNC.M with the given input arguments.
%
%      GUI_FUNC('Property','Value',...) creates a new GUI_FUNC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_func_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_func_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_func

% Last Modified by GUIDE v2.5 11-Jul-2019 14:42:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_func_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_func_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before GUI_func is made visible.
function GUI_func_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_func (see VARARGIN)

% Choose default command line output for GUI_func
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% Hint: place code in OpeningFcn to populate axes1

% UIWAIT makes GUI_func wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_func_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5


% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6


% --- Executes on button press in checkbox7.
function checkbox7_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox7


% --- Executes during object creation, after setting all properties.
function line_num_CreateFcn(hObject, eventdata, handles)
% hObject    handle to line_num (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes when selected object is changed in line_num.
function line_num_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in line_num 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function line_num_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to line_num (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


line2=get(handles.ch_line2,'Value')


% --- Executes on button press in ch_line2.
function ch_line2_Callback(hObject, eventdata, handles)
% hObject    handle to ch_line2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ch_line2


% --- Executes on button press in ch_line3.
function ch_line3_Callback(hObject, eventdata, handles)
% hObject    handle to ch_line3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ch_line3


% --- Executes on button press in ch_line4.
function ch_line4_Callback(hObject, eventdata, handles)
% hObject    handle to ch_line4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ch_line4


% --- Executes on button press in ch_line5.
function ch_line5_Callback(hObject, eventdata, handles)
% hObject    handle to ch_line5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ch_line5


% --- Executes on button press in ch_line6.
function ch_line6_Callback(hObject, eventdata, handles)
% hObject    handle to ch_line6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ch_line6


% --- Executes on button press in ch_line7.
function ch_line7_Callback(hObject, eventdata, handles)
% hObject    handle to ch_line7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ch_line7


% --- Executes on button press in ch_line8.
function ch_line8_Callback(hObject, eventdata, handles)
% hObject    handle to ch_line8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ch_line8

function box2_Callback(hObject, eventdata, handles)
% hObject    handle to box2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of box2 as text
%        str2double(get(hObject,'String')) returns contents of box2 as a double

% --- Executes during object creation, after setting all properties.
function box2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to box2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function box3_Callback(hObject, eventdata, handles)
% hObject    handle to box3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of box3 as text
%        str2double(get(hObject,'String')) returns contents of box3 as a double


% --- Executes during object creation, after setting all properties.
function box3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to box3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function box6_Callback(hObject, eventdata, handles)
% hObject    handle to box6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of box6 as text
%        str2double(get(hObject,'String')) returns contents of box6 as a double


% --- Executes during object creation, after setting all properties.
function box6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to box6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function box7_Callback(hObject, eventdata, handles)
% hObject    handle to box7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of box7 as text
%        str2double(get(hObject,'String')) returns contents of box7 as a double


% --- Executes during object creation, after setting all properties.
function box7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to box7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function box4_Callback(hObject, eventdata, handles)
% hObject    handle to box4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of box4 as text
%        str2double(get(hObject,'String')) returns contents of box4 as a double


% --- Executes during object creation, after setting all properties.
function box4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to box4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function box5_Callback(hObject, eventdata, handles)
% hObject    handle to box5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of box5 as text
%        str2double(get(hObject,'String')) returns contents of box5 as a double


% --- Executes during object creation, after setting all properties.
function box5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to box5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function box8_Callback(hObject, eventdata, handles)
% hObject    handle to box8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of box8 as text
%        str2double(get(hObject,'String')) returns contents of box8 as a double


% --- Executes during object creation, after setting all properties.
function box8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to box8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%global line_2 line_3 line_4 line_5 line_6 line_7 line_8;

%KSP: HERE WE GET THE VALUES FROM THE CHECKBOXES THAT WERE CREATED AND
% THE FOLLOWING SET OF LINES ARE ACTIVATED FOR WHEN THE COMPLETE BUTTON ON
% THE GUI IS PRESSED
line_2=get(handles.ch_line2,'Value');
line_3=get(handles.ch_line3,'Value');
line_4=get(handles.ch_line4,'Value');
line_5=get(handles.ch_line5,'Value');
line_6=get(handles.ch_line6,'Value');
line_7=get(handles.ch_line7,'Value');
line_8=get(handles.ch_line8,'Value');

%global num_correction_saccade_line2 num_correction_saccade_line3 num_correction_saccade_line4 ...
 %   num_correction_saccade_line5 num_correction_saccade_line6 num_correction_saccade_line7 ...
  %  num_correction_saccade_line8
num_corr_sacc_line2=eval(get(handles.box2,'String'));
num_corr_sacc_line3=eval(get(handles.box3,'String'));
num_corr_sacc_line4=eval(get(handles.box4,'String'));
num_corr_sacc_line5=eval(get(handles.box5,'String'));
num_corr_sacc_line6=eval(get(handles.box6,'String'));
num_corr_sacc_line7=eval(get(handles.box7,'String'));
num_corr_sacc_line8=eval(get(handles.box8,'String'));

user_response.sweep_indicator= [line_2,line_3,line_4,line_5,line_6,line_7,line_8];
user_response.correction_saccade=[num_corr_sacc_line2,num_corr_sacc_line3,num_corr_sacc_line4,...
                        num_corr_sacc_line5,num_corr_sacc_line6,num_corr_sacc_line7,...
                        num_corr_sacc_line8];
set(0,'default',user_response);

% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over pushbutton1.
function pushbutton1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% KSP: THIS IS AN ADDITION FEATURE WHERE THE CHECKBOXED AND ENTRY FIELDS
% ARE GRAYED OUT FOR WHEN THE USER RIGHT CLICKS ON THE COMPLETE BUTTON IN
% THE GUI
set(handles.ch_line2,'Enable','Off');
set(handles.ch_line3,'Enable','Off');
set(handles.ch_line4,'Enable','Off');
set(handles.ch_line5,'Enable','Off');
set(handles.ch_line6,'Enable','Off');
set(handles.ch_line7,'Enable','Off');
set(handles.ch_line8,'Enable','Off');

set(handles.box2,'Enable','Off');
set(handles.box3,'Enable','Off');
set(handles.box4,'Enable','Off');
set(handles.box5,'Enable','Off');
set(handles.box6,'Enable','Off');
set(handles.box7,'Enable','Off');
set(handles.box8,'Enable','Off');

% --- Executes on button press in load_fig.
function load_fig_Callback(hObject, eventdata, handles)
% hObject    handle to load_fig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global fig_handle cursor_info

% KSP: THE FOLLOWING SET OF LINES ARE USED TO LOAD THE FIGURE AND PLOT THE
% DATA IN FIGURE WINDOW OF THE GUI AND IS ACTIVATED FOR WHEN THE USER
% CLICKS ON THE LOAD FIGURE BUTTON WHICH THEN OPENS UP A GUI AND THE USER
% IS REQUESTED TO SELECT A DATA FILE WHICH IS THEN PLOTTED IN THE FIGURE
% WINDOW. 
% FOLLOWING WHICH THE USER IS EXPECTED TO SELECT THE SWEEP DATA FIRST AND
% SUBSEQUENTLY THE CORRECTION SACCADE DATA
[filename,folderpath]=uigetfile('D:\projects_d\Experiment King Devick Reading\reading\Results\Experiment 3 R vs L\All Subjects\Raw Data\*.txt', 'Please select the data file');
processed_filename=strcat('sweep_dataframe_',filename(31:43));
if isfile([folderpath,processed_filename])==1
    waitfor(msgbox('Already Completed!!'));
end
fig_data.filename=filename;
fig_data.folderpath=folderpath;
datafile_fullpath=[folderpath,filename];
saccade_data=importdata(datafile_fullpath,'\t',1);
data_array=saccade_data.data;
textimg_startfilename='reading_word_data_';
textimg_middlefilename=filename((strfind(filename,'_S')+1:end-11));
textimg_endfilename='page4.png';%Changed for expt 3: Right Vs Left 
textimg_fullfilename=strcat(folderpath,textimg_startfilename,textimg_middlefilename,textimg_endfilename);
RGB_image= imread(textimg_fullfilename);
x_position_start=(data_array(:,4));
x_position_end=(data_array(:,6));
y_position_start=(data_array(:,5));
y_position_end=(data_array(:,7));
saccade_amplitude_h= x_position_end - x_position_start;
saccade_amplitude_v=y_position_end-y_position_start;
backward_saccade_counter=1;
for backward_saccade_idx=1:length(data_array)-1
    if saccade_amplitude_h(backward_saccade_idx)<0 %checks for saccades in the backward direction
        backward_data(backward_saccade_counter,:)=data_array(backward_saccade_idx,:);
        backward_saccade_counter=backward_saccade_counter+1;
    end
end
%Changed for expt 3: Right Vs Left :start
forward_saccade_counter=1;
for foward_saccade_idx=1:length(data_array)-1
    if saccade_amplitude_h(foward_saccade_idx)>0 %checks for saccades in the forward direction
        forward_data(forward_saccade_counter,:)=data_array(foward_saccade_idx,:);
        forward_saccade_counter=forward_saccade_counter+1;
    end
end
%Changed for expt 3: Right Vs Left :end
fig_data.backward_saccades=backward_data;
image(RGB_image);
hold on;
% First we plot the regression data onto the image file 
for backward_idx=1:length(backward_data(:,1))-1
    h_backwards=plot([backward_data(backward_idx,4), backward_data(backward_idx,6)]...
        ,[backward_data(backward_idx,5),backward_data(backward_idx,7)]...
        ,'-o','Color','green');
    hold on
end
%Changed for expt 3: Right Vs Left :start
for forward_idx=1:length(forward_data(:,1))-1
    h_forward=plot([forward_data(forward_idx,4), forward_data(forward_idx,6)]...
        ,[forward_data(forward_idx,5),forward_data(forward_idx,7)]...
        ,'-o','Color','red');
    hold on
end
%Changed for expt 3: Right Vs Left :end
line_num= [2,3,4,5,6,7,8]; %Paragraph numbering sequence
line_num_xposition=50;
line_num_yposition= [240,288,341,389,441,491,544];
for line_numbering_idx=1:length(line_num)
    saccade_text=text(line_num_xposition,line_num_yposition(line_numbering_idx),num2str(line_numbering_idx+1));
end
xlim([0 1920]) %Changed for expt 3: Right Vs Left 
ylim([0 1080])%Changed for expt 3: Right Vs Left 
h=gca;
set(h,'YDir','reverse');
for data_click_idx=1:3
    fig_handle=gcf;
    if data_click_idx==1
        title('Select Sweep Data')
    elseif data_click_idx==2
        title('Select Correction Saccades')
    elseif data_click_idx==3
        title('Select the remaining saccades (that are not regressions)')
    end
    dcm_obj = datacursormode(fig_handle);
    set(dcm_obj,'DisplayStyle','datatip','SnapToDataVertex','on','Enable','on');
    % uiwait(fig_handle)
    % waitforbuttonpress
    currkey=0;
    % do not move on until enter key is pressed
    while currkey~=1
        pause; % wait for a keypress
        cursor_info=getCursorInfo(dcm_obj);
        currkey=get(gcf,'CurrentKey'); 
        if strcmp(currkey, 'return') 
            currkey=1 ;
        elseif ishandle(fig_handle)==0
            currkey=1;
        else
            currkey=0; % Error was here; the "==" should be "="
        end
    end
    fig_data.fig_handle=fig_handle;
    % guidata(hObject,handles);
    if data_click_idx==1
        fig_data.sweep_posn=cursor_info;
        delete(findall(fig_handle,'Type','hggroup')); 
    elseif data_click_idx==2
        fig_data.correction_posn=cursor_info;
        delete(findall(fig_handle,'Type','hggroup')); 
    elseif data_click_idx==3
        fig_data.rejected_saccades=cursor_info;
        delete(findall(fig_handle,'Type','hggroup')); 
    end
    set(0,'userdata',fig_data)
end
title('Push Done Button')

% --- Executes during object creation, after setting all properties.
function load_fig_CreateFcn(hObject, eventdata, handles)
% hObject    handle to load_fig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in click_completion.
function click_completion_Callback(hObject, eventdata, handles)
% hObject    handle to click_completion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% KSP: THIS IS THE FINAL STAGE WHERE ALL THE INFORMATION FROM THE GUI IS
% USED TO CREATE A SWEEP DATAFRAME FOR THE PARTICULAR TRIAL.
% AND FINALLY THE DATA IS WRITTEN A SEPARATE CSV WHICH IS EVENTUALLY SAVED
% INSIDE THE RAW DATA DIRECTORY.
% UPON COMPLETION THE CHECKBOXES AND FIGURE ARE REVERTED TO THE INITIAL
% STATE AND THE USER CAN THEN DECIDE IF THEY WANT TO RUN ANOTHER FILE OR
% CLOSE THE PROGRAM
% NOTE: IN THE CURRENT SETTING THE PROGRAM CAN ONLY HANDLE TWO CORRECTION
% SACCADES IN ADDITION TO THE SWEEP DATA. 
fig_data=get(0,'userdata');
checkbox_response=get(0,'default');
click_info_sweeps=fig_data.sweep_posn;
backward_saccade_data=fig_data.backward_saccades;
fig_handle=fig_data.fig_handle;
filename=fig_data.filename;
folderpath=fig_data.folderpath;
sweep_indicator=checkbox_response.sweep_indicator;
correction_saccade=checkbox_response.correction_saccade;
if isempty(fig_data.correction_posn)
    disp('There are no correction saccades')
else
    click_info_correction=fig_data.correction_posn;
    for corr_sacc_idx=1:length(click_info_correction)
        corr_sacc_info=click_info_correction(corr_sacc_idx).Position(1,:);
        corr_sacc_status=find(ismember(backward_saccade_data,corr_sacc_info));
        [corr_sacc_row,corr_sacc_column]=ind2sub(size(backward_saccade_data),corr_sacc_status);
        corr_sacc_data(corr_sacc_idx,:)=backward_saccade_data(corr_sacc_row(1),:);
    end
    corr_sacc_data=flip(corr_sacc_data);
end
if isempty(fig_data.rejected_saccades)
    disp('There are no rejected saccades')
else 
    click_info_rejected_sacades=fig_data.rejected_saccades;

    for rejected_sacc_idx=1:length(click_info_rejected_sacades)
        rejected_sacc_info=click_info_rejected_sacades(rejected_sacc_idx).Position(1,:);
        rejected_sacc_status=find(ismember(backward_saccade_data,rejected_sacc_info));
        [rejected_sacc_row,rejected_sacc_column]=ind2sub(size(backward_saccade_data),rejected_sacc_status);
        rejected_sacc_data(rejected_sacc_idx,:)=backward_saccade_data(rejected_sacc_row(1),:);
    end
end
<<<<<<< HEAD

for sweep_idx=1:length(click_info_sweeps)
    sweep_info=click_info_sweeps(sweep_idx).Position(1,:);
    sweep_status=find(ismember(backward_saccade_data,sweep_info));
    [sweep_row,sweep_column]=ind2sub(size(backward_saccade_data),sweep_status);
    sweep_data(sweep_idx,:)=backward_saccade_data(sweep_row(1),:);
end
sweep_data=flip(sweep_data);
sweep_dataframe(1:7,1:7)=0;
sweep_counter=1;
for sweep_df_idx=1:length(sweep_indicator)
    if sweep_indicator(sweep_df_idx)==1
        sweep_dataframe(sweep_df_idx,:)=sweep_data(sweep_counter,:);
        sweep_counter=sweep_counter+1;
    else
        sweep_dataframe(sweep_df_idx,:)=0;
=======
if isempty(fig_data.sweep_posn)
    disp('There are no sweeps')
else 
    for sweep_idx=1:length(click_info_sweeps)
        sweep_info=click_info_sweeps(sweep_idx).Position(1,:);
        sweep_status=find(ismember(backward_saccade_data,sweep_info));
        [sweep_row,sweep_column]=ind2sub(size(backward_saccade_data),sweep_status);
        sweep_data(sweep_idx,:)=backward_saccade_data(sweep_row(1),:);
    end
    sweep_data=flip(sweep_data);
    sweep_dataframe(1:7,1:7)=0;
    sweep_counter=1;
    for sweep_df_idx=1:length(sweep_indicator)
        if sweep_indicator(sweep_df_idx)==1
            sweep_dataframe(sweep_df_idx,:)=sweep_data(sweep_counter,:);
            sweep_counter=sweep_counter+1;
        else
            sweep_dataframe(sweep_df_idx,:)=0;
        end
>>>>>>> ffc05614efe535ee69cc0184879dcc1abe79b3ac
    end
end
corr_sacc_counter=1;
% KSP 07/15/19: Changed the dataframe structure, now we are creating an
% individual data frame that contains only the correction saccade
% informationa and the idea is to save the individually in order to better
% the overall performance of the code
if exist('corr_sacc_data','var')
    for corr_sacc_df_idx=1:length(correction_saccade)
        if correction_saccade(corr_sacc_df_idx)==0
            corr_sacc_df(corr_sacc_df_idx,1:14)=0;
%             sweep_dataframe(corr_sacc_df_idx,8:21)=0; %for integerated df
        elseif correction_saccade(corr_sacc_df_idx)==1
            corr_sacc_df(corr_sacc_df_idx,1:7)=corr_sacc_data(corr_sacc_counter,:);
            corr_sacc_df(corr_sacc_df_idx,8:14)=0;
%             sweep_dataframe(corr_sacc_df_idx,8:14)=corr_sacc_data(corr_sacc_counter,:);
%             sweep_dataframe(corr_sacc_df_idx,15:21)=0;
            corr_sacc_counter=corr_sacc_counter+1;
        elseif correction_saccade(corr_sacc_df_idx)==2
            corr_sacc_df(corr_sacc_df_idx,1:7)=corr_sacc_data(corr_sacc_counter,:);
%             sweep_dataframe(corr_sacc_df_idx,8:14)=corr_sacc_data(corr_sacc_counter,:);
            corr_sacc_counter=corr_sacc_counter+1;
            corr_sacc_df(corr_sacc_df_idx,8:14)=corr_sacc_data(corr_sacc_counter,:);
%             sweep_dataframe(corr_sacc_df_idx,15:21)=corr_sacc_data(corr_sacc_counter,:);
            corr_sacc_counter=corr_sacc_counter+1;
        end
    end
else
    corr_sacc_df(:,1:14)=0;
%     sweep_dataframe(:,8:21)=0;
end
output_data=get(0,'userdata');
if exist('sweep_data','var')
    output_data.sweep_data=sweep_data;
end
if exist('corr_sacc_data','var')
    output_data.corr_sacc_data=corr_sacc_data;
    output_data.corr_sacc_df=corr_sacc_df;
end

% output_data.sweep_dataframe=sweep_dataframe;
% f=figure('units','normalized','outerposition',[0 0 1 1]);
% table_handle=uitable(f,'Data',sweep_data,'Position',[20 500 1500 300]);
% table_handle=uitable(f,'Data',sweep_dataframe,'Position',[20 500 1500 300]);
set(0,'userdata',output_data)
outputfilename_sweep=strcat('sweep_dataframe_',filename(31:43));
outputfilename_corr_sacc_df=strcat('correction_saccades_',filename(31:43));
outputfilename_rejected_sacc=strcat('rejected_saccades_',filename(31:43));
outfolder=folderpath;
% [outputfilename,outfolder]=uiputfile('.txt','Please select a location for saving the file');
if exist('sweep_data','var')
    fid_sweep=fopen([outfolder, outputfilename_sweep],'w');
    fprintf(fid_sweep,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n',"Time Start","Time End",...
                    "Saccade Duration","Pos x(start)","Pos y(start)",...
                    "Pos x(end)","Pos y(end)");
    % fprintf(fid_sweep,'%s\t%s\t%s\t%s\t%s\t%s\t%s%s\t%s\t%s\t%s\t%s\t%s\t%s%s\t%s\t%s\t%s\t%s\t%s\t%s\n'...
    %                 ,"Time Start","Time End","Saccade Duration","Pos x(start)","Pos y(start)",...
    %                 "Pos x(end)","Pos y(end)","Time Start","Time End","Saccade Duration","Pos x(start)","Pos y(start)",...
    %                 "Pos x(end)","Pos y(end)","Time Start","Time End","Saccade Duration","Pos x(start)","Pos y(start)",...
    %                 "Pos x(end)","Pos y(end)");
    % shape_sweep_dataframe=size(sweep_dataframe);
    shape_sweep_dataframe=size(sweep_dataframe);
    for sweep_event_idx=1:shape_sweep_dataframe(1)
        fprintf(fid_sweep,'%g\t',sweep_dataframe(sweep_event_idx,:));
    %    fprintf(fid_sweep,'%g\t',sweep_dataframe(sweep_event_idx,:));
       fprintf(fid_sweep,'\n');
    end
    fclose(fid_sweep);
    disp('Sweep Data file created');
end

if exist('corr_sacc_df','var')
    fid_corr_sacc=fopen([outfolder,outputfilename_corr_sacc_df],'w');
    fprintf(fid_corr_sacc,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',...
            "Time Start","Time End","Saccade Duration","Pos x(start)","Pos y(start)",...
            "Pos x(end)","Pos y(end)","Time Start","Time End","Saccade Duration","Pos x(start)","Pos y(start)",...
                    "Pos x(end)","Pos y(end)");
    shape_corr_sacc_df=size(corr_sacc_df);
    for corr_sacc_event_idx=1:shape_corr_sacc_df(1)
        fprintf(fid_corr_sacc,'%g\t',corr_sacc_df(corr_sacc_event_idx,:));
        fprintf(fid_corr_sacc,'\n');
    end
    fclose(fid_corr_sacc);
    disp('correction saccade file created');
end

if exist('rejected_sacc_data','var')
    output_data.rejected_sacc_data=rejected_sacc_data;

    fid_rejected_sacc=fopen([outfolder,outputfilename_rejected_sacc],'w');
    fprintf(fid_rejected_sacc,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n'...
                    ,"Time Start","Time End","Saccade Duration","Pos x(start)","Pos y(start)",...
                    "Pos x(end)","Pos y(end)");
    shape_rejected_sacc_df=size(rejected_sacc_data);
    for rejected_sacc_event_idx=1:shape_rejected_sacc_df(1)
       fprintf(fid_rejected_sacc,'%g\t',rejected_sacc_data(rejected_sacc_event_idx,:));
       fprintf(fid_rejected_sacc,'\n');
    end
    fclose(fid_rejected_sacc);
    disp('Reject saccade file created');
else
    fid_rejected_sacc=fopen([outfolder,outputfilename_rejected_sacc],'w');
    fprintf(fid_rejected_sacc,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n'...
                    ,"Time Start","Time End","Saccade Duration","Pos x(start)","Pos y(start)",...
                    "Pos x(end)","Pos y(end)");
end
% close(fig_handle)
set(handles.ch_line2,'Value',1);
set(handles.ch_line3,'Value',1);
set(handles.ch_line4,'Value',1);
set(handles.ch_line5,'Value',1);
set(handles.ch_line6,'Value',1);
set(handles.ch_line7,'Value',1);
set(handles.ch_line8,'Value',1);

set(handles.box2,'string',num2str(0));
set(handles.box3,'string',num2str(0));
set(handles.box4,'string',num2str(0));
set(handles.box5,'string',num2str(0));
set(handles.box6,'string',num2str(0));
set(handles.box7,'string',num2str(0));
set(handles.box8,'string',num2str(0));

cla reset;


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on button press in restart.
function restart_Callback(hObject, eventdata, handles)
% hObject    handle to restart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% KSP: THE FOLLOWING SET OF LINES RESET THE CHECKBOXES AND ENTRY BOXES AND
% ALSO REMOVE THE FIGURE FROM THE AXIS AND IT IS QUITE SIMILAR TO THE LAST
% SECTION OF THE "DONE" BUTTON. THIS OPTION ALLOWS THE USER TO RESTART THE
% PROGRAM EVEN WHEN RUNNING A PARTICULAR FILE. 

set(handles.ch_line2,'Value',1);
set(handles.ch_line3,'Value',1);
set(handles.ch_line4,'Value',1);
set(handles.ch_line5,'Value',1);
set(handles.ch_line6,'Value',1);
set(handles.ch_line7,'Value',1);
set(handles.ch_line8,'Value',1);

set(handles.box2,'string',num2str(0));
set(handles.box3,'string',num2str(0));
set(handles.box4,'string',num2str(0));
set(handles.box5,'string',num2str(0));
set(handles.box6,'string',num2str(0));
set(handles.box7,'string',num2str(0));
set(handles.box8,'string',num2str(0));

cla reset;



% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
