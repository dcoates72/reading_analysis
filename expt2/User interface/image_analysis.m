%Krish Prahalad
%This script imports an image file superimposed with eye movement data 
% and helps to isolate the different components and replot the image


% The functions of the script are as follows:
% 1. We identify the data file that needs to be imported 
% 2. Import the data file and organize based on data structure 
% 3. Import the appropriate image file using the data file name 
% 4. calculated the saccade parameters and isolate the ones of interest
% 5. plot the saccades and the image in a figure and have the user decide 
%    on which ones are sweeps and which ones are regressions

%% Parameters/testing conditions of the data being analyzed
text_height=34;
text_width=34*0.52;
scotoma_radius=132;

%% Selecting the data and image file from data directory 
% Ask the user to select the Eye data file in this case it would be the
% saccade events detected by the eyelink that has already been parsed and
% written to a seperate file
% The filename would look something like this: 
% "eyetrace_events_saccades_SXX_X_TX_XXX.asc.txt"
% If you wuld like to run for a whole folder then in that case you would be
% selecting the folder that contains the particular files
[filename,folderpath]=uigetfile('*.txt', 'Please select the data file');

datafile_fullpath=[folderpath,filename];
cd (folderpath);
% The file that is selected is then imported into the program. The data
% file that is to be imported already has the appropriate headers and it is
% a tab delimitted file 
saccade_data=importdata(datafile_fullpath,'\t',1);
% Now we assign the different components of the data to respective
% variables
data_array=saccade_data.data;
% column_headers=saccade_data.colheaders;
%using the filename of the data file we can identify the image file that is
% to be used for the plot. For this we create the image filename and
% following that we open the same 
textimg_startfilename='reading_word_data_';
textimg_middlefilename=filename((strfind(filename,'_S')+1:end-8));
textimg_endfilename='.txt.png';

textimg_fullfilename=strcat(textimg_startfilename,textimg_middlefilename,textimg_endfilename);

% The image of the text that was presented is now read and is then cast to 
% variable 
RGB_image= imread(textimg_fullfilename);

% We now check the data directory to identify the PRL position as the
% folder name used would indicate the PRL position that the current trial
% belongs to 
if ~contains(datafile_fullpath,"Inferior")==0
    prl_position='Inferior';
elseif ~contains(datafile_fullpath,"Right")==0
    prl_position='Right';
elseif ~contains(datafile_fullpath,"Left")==0
    prl_position='Left';
else
    prl_position='foveal';
end
%% Setting the offset for the plot 
if strcmpi(prl_position,'foveal')
        offset_x=0;
        offset_y=0;
%In cases where there is peripheral data this part helps to skew the data points such that it is more relation to the PRL 
%that was used by the subject and not the data from the foveal fixation(so removes the overall offset)
elseif strcmpi(prl_position,'left')
        offset_x=(scotoma_radius/2+text_width*2);
        offset_y=0;
elseif strcmpi(prl_position,'right')
        offset_x=-(scotoma_radius/2+text_width*2);
        offset_y=0;
elseif strcmpi(prl_position,'inferior')
        offset_x=0;
        offset_y=((scotoma_radius/2)+(text_height/2))/2;
end
        offset_x=0;
        offset_y=0;
%We use these and identify the starting and ending position for each
%saccade
x_position_start=(data_array(:,4));
x_position_end=(data_array(:,6));
y_position_start=(data_array(:,5));
y_position_end=(data_array(:,7));


%Using the values from above we calculate the saccade amplitude in the
%horizontal meridian & vertical meridian separetely 
saccade_amplitude_h= x_position_end - x_position_start;
saccade_amplitude_v=y_position_end-y_position_start;

%% Automatic Classification of regression and sweeps:
regression_data=[];
sweep_data=[];
backward_data=[];
forward_data=[];
foveating_saccades=[];
sweep_counter=1;
regression_counter=1;
backward_saccade_counter=1;
forward_saccade_counter=1;
foveating_saccade_counter=1;
for saccade_idx=1:length(data_array)-1
    if saccade_amplitude_h(saccade_idx)<0 %checks for saccades in the backward direction
        backward_data(backward_saccade_counter,:)=data_array(saccade_idx,:);
        backward_saccade_counter=backward_saccade_counter+1;
    else
        forward_data(forward_saccade_counter,:)=data_array(saccade_idx,:);
        forward_saccade_counter=forward_saccade_counter+1;
%         if saccade_amplitude_h(saccade_idx)<0 && saccade_amplitude_v(saccade_idx)>0
%             sweep_data(sweep_counter,:)=data_array(saccade_idx,:);
%             sweep_idx=saccade_idx;%assign the index in the saccade array to a variable so that it can be accessed later
%             sweep_counter=sweep_counter+1;  
% %         elseif saccade_amplitude_h(saccade_idx)<0 && saccade_amplitude_v(saccade_idx)<25 && saccade_amplitude_v(saccade_idx)>-25
%         else           
%          regression_data(regression_counter,:)=data_array(saccade_idx,:);
%          regression_counter=regression_counter+1;
    
%         elseif saccade_amplitude_h(saccade_idx+1)>0
%             if saccade_idx-1>0 %To ensure that this is never zero 
%                 if saccade_amplitude_h(saccade_idx-1)<0
%                     sweep_data(sweep_counter,:)=data_array(saccade_idx,:);
%                     sweep_counter=sweep_counter+1;                
%                 else
%                     foveating_saccades(foveating_saccade_counter,:)=data_array(saccade_idx,:);
%                     foveating_saccade_counter=foveating_saccade_counter+1;
%                 end    
%             end
%         end
    end
end
%% Automatic Data: Plotting regression and sweeps (with manual modification feature)
% repeat=true;
% % Now we create a numbering system for each of the data point
% while repeat
%     fig_handle=figure(1);
%     image(RGB_image);
%     hold on;
%     regression_data_size=size(regression_data);
%     regression_data_rows=regression_data_size(1);
%     sweep_data_size=size(sweep_data);
%     sweep_data_rows=sweep_data_size(1);
%     % First we plot the regression data onto the image file 
%     for regression_idx=1:length(regression_data)
%         if regression_idx>regression_data_rows
%             return
%         else
%             h_regression=plot([(regression_data(regression_idx,4)), (regression_data(regression_idx,6))]...
%                 ,[(regression_data(regression_idx,5)),(regression_data(regression_idx,7))]...
%                 ,'-o','Color','red');
%             set(h_regression,'xdata',get(h_regression,'xdata')+offset_x);
%             set(h_regression,'ydata',get(h_regression,'ydata')+offset_y);
%             regression_text=text((regression_data(regression_idx,4)), (regression_data(regression_idx,5)),...
%                 num2str(regression_idx),'VerticalAlignment','bottom','HorizontalAlignment','right');
% %             set(regression_text,'xdata',get(h_regression,'xdata')+offset_x);
% %             set(regression_text,'ydata',get(h_regression,'ydata')+offset_y);
%             hold on
%         end
%     end
%     hold on;
%     for sweep_idx=1:length(sweep_data)
%         if sweep_idx>sweep_data_rows
%             return
%         else
%             h_sweep=plot([(sweep_data(sweep_idx,4)+offset_x),(sweep_data(sweep_idx,6)+offset_x)]...
%                 ,[(sweep_data(sweep_idx,5)+offset_y),(sweep_data(sweep_idx,7)+offset_y)]...
%                 ,'-o','Color','green');
%             set(h_sweep,'xdata',get(h_sweep,'xdata')+offset_x);
%             set(h_sweep,'ydata',get(h_sweep,'ydata')+offset_y);
%             sweep_text=text((sweep_data(sweep_idx,4)+offset_x), (sweep_data(sweep_idx,5)+offset_x),...
%                 num2str(sweep_idx),'VerticalAlignment','bottom','HorizontalAlignment','right');
%             hold on
%         end
%     end
%     hold off;
%     xlim([0 1024])
%     ylim([0 768])
%     h=gca;
%     set(h,'YDir','reverse')
%     dcm_obj = datacursormode(figure(1));set(dcm_obj,'DisplayStyle','datatip',...
%                         'SnapToDataVertex','on','Enable','on')
% 
%         % Set update function
%         set(dcm_obj,'UpdateFcn',@myupdatefcn)
%         % Wait while the user to click
%         disp('Click line to display a data tip, then press "Return"')
%         pause 
%         if isvalid(fig_handle)==0
%             disp('Selection Complete');
%             break;
%         else
%             % Export cursor to workspace
%             info_struct = getCursorInfo(dcm_obj);
%             if isfield(info_struct, 'Position')
%               disp('Clicked position is')
%               disp(info_struct.Position)
%               if isempty(find(sweep_data==info_struct.Position(end,1)))==1
%                   [x_regression,y_regression]=ind2sub(size(regression_data),find(regression_data==info_struct.Position(1)));
%                   sweep_data_size=size(sweep_data);
%                   row_num_sweep=sweep_data_size(1);
%                   sweep_data(row_num_sweep+1,:)=regression_data(x_regression,:);
%                   regression_data(x_regression,:)=[];
%                   continue
%               elseif isempty(find(regression_data==info_struct.Position(end,1)))==1
%                   [x_sweep,y_sweep]=ind2sub(size(sweep_data),find(sweep_data==info_struct.Position(1)));
%                   regression_data_size=size(regression_data);
%                   row_num_regression=regression_data_size(1);
%                   regression_data(row_num_regression+1,:)=sweep_data(x_sweep,:);
%                   sweep_data(x_sweep,:)=[];
%                   continue
%               else
%                   continue
%               end
%            end
%         end
% end
% [~,idx]=sort(sweep_data(:,1));
% sweep_data=sweep_data(idx,:);
%     % We get the output in struct format and hence needs to be saved to matlab
%     % array format which contains the x and y coordinates of each of the clicks
%     % that were made
% %     click_coordinates = cell2mat({cursor_info.Position}');
% % end

%% Sweep Analysis
% sweep_counter=1;
% repeat=true;
% i=1;
% while repeat==true
%     if i>length(sweep_data)
%         repeat=false;
%         continue 
%     else
%         for j=i:(length(sweep_data))
%             if (j+1)<=length(sweep_data)
% %               We identify sweeps by checking for the change in x position
% %               and the time stamps between subsequent saccadic eye
% %               movements. 
%                 if sweep_data(j+1,4)-sweep_data(j,4)<0 && sweep_data(j+1,1)- sweep_data(j,1)<1000
%                         continue
%                 else
%                     if j==i
%                         sweep_data_sorted{sweep_counter}=sweep_data(i:j,:);
%                         disp('Single Sweep');
%                         sweep_counter=sweep_counter+1;
%                         i=j+1;
%                         continue 
%                     else
%                         sweep_data_sorted{sweep_counter}=sweep_data(i:j,:);
%                         disp(i);disp(j);
%                         sweep_counter=sweep_counter+1;
%                         i=j+1;
%                         continue;
%                     end
%                 end
%             elseif j==length(sweep_data)
%                 sweep_data_sorted{sweep_counter}=sweep_data(j,:);
%                 disp('Last Sweep');
%                 sweep_counter=sweep_counter+1;
%                 i=j+1;
%                 continue 
%             else
%                 repeat=false;
%                 continue
%             end
%         end
%     end
% end

%% Plotting of ALL saccades
    fig_handle=figure(1);
    image(RGB_image);
    hold on;
    % First we plot the regression data onto the image file 
    for saccade_idx=1:length(data_array(:,1))-1
        h_saccades=plot([data_array(saccade_idx,4)+offset_x, data_array(saccade_idx,6)+offset_x]...
            ,[data_array(saccade_idx,5)+offset_y,data_array(saccade_idx,7)+offset_y]...
            ,'-o','Color','green');
        set(h_saccades,'xdata',get(h_saccades,'xdata')+offset_x);
        set(h_saccades,'ydata',get(h_saccades,'ydata')+offset_y);
%         h_saccades_text=text((data_array(saccade_idx,4)+offset_x), (data_array(saccade_idx,5)+offset_x),...
%                 num2str(saccade_idx),'VerticalAlignment','bottom','HorizontalAlignment','right');
        hold on
    end
    hold off;
    xlim([0 1024])
    ylim([0 768])
    h=gca;
    set(h,'YDir','reverse')

 %% Plotting of backward saccades
    fig_handle=figure(2);
    image(RGB_image);
    hold on;
    % First we plot the regression data onto the image file 
    for backward_idx=1:length(backward_data(:,1))-1
        h_backwards=plot([backward_data(backward_idx,4), backward_data(backward_idx,6)]...
            ,[backward_data(backward_idx,5),backward_data(backward_idx,7)]...
            ,'-o','Color','green');
        set(h_backwards,'xdata',get(h_backwards,'xdata')+offset_x);
        set(h_backwards,'ydata',get(h_backwards,'ydata')+offset_y);
        hold on
    end
    xlim([0 1024])
    ylim([0 768])
    h=gca;
    set(h,'YDir','reverse');
 
%     The following set of lines creates checkboxes that line up against
%     each line of text of the stimulus and the user here has to indicate
%     as to whether a sweep eye movement was actually found for this
%     particular line or not. The default state of the checkboxes is set to
%     true at the moment(More can be done by playing with the function
%     found at the bottom of the script)
%     The number of the variable here is matched with the line number and
%     as we get sweeps only from the second line onwards we start the
%     checkboxes from the second line and hence we ignore the first line in
%     this case
%     r2 = check_box_func(fig_handle,634,'Line 2');response_line2=get(r2,'Value');
%     r3 = check_box_func(fig_handle,583,'Line 3');response_line3=get(r3,'Value');
%     r4 = check_box_func(fig_handle,532,'Line 3');response_line4=get(r4,'Value');
%     r5 = check_box_func(fig_handle,481,'Line 4');response_line5=get(r5,'Value');
%     r6 = check_box_func(fig_handle,430,'Line 5');response_line6=get(r6,'Value');
%     r7 = check_box_func(fig_handle,379,'Line 6');response_line7=r7.Value;
%     r8 = check_box_func(fig_handle,328,'Line 7');response_line8=r8.Value; 
 
%   We then wait for the user to press button after which the values from
%   the checkboxes are concatenated to form one single array wih binary
%   data
%     user_response=[response_line2,response_line3,response_line4,response_line5,...
%                     response_line6,response_line7,response_line8];
%     waitforbuttonpress
% % After we plot we set the figure to data cursor mode which helps to get
% % the collect the information from each data point as when the user clicks
% % on it 
%     for gui_idx=1:7
%         checkbox_info=eval(sprintf('response_line%d',(gui_idx+1)));
%         if checkbox_info==0
%             sweep_dataframe(gui_idx,:)=0;
%             disp(gui_idx)
%             break;
% %         else
%             dcm_obj = datacursormode(fig_handle);set(dcm_obj,'DisplayStyle','datatip',...
%                                 'SnapToDataVertex','on','Enable','on')
% %             uiwait(fig_handle)
%             % We get the output in struct format and hence needs to be saved to matlab
%             % array format which contains the x and y coordinates of each of the clicks
%             % that were made
%             cursor_info=getCursorInfo(dcm_obj);
%             waitforbuttonpress
%             [row,column] = find(backward_data==cursor_info.Position(1));
%             sweep_dataframe(gui_idx,:)=backward_data(row,4:7);
%             waitforbuttonpress
%             disp(gui_idx)
%         end
%     end
    
%% Plotting Forward Saccades
    fig_handle=figure(3);
    image(RGB_image);
    hold on;
    % First we plot the regression data onto the image file 
    for forward_idx=1:length(forward_data(:,1))-1
        h_forward=plot([forward_data(forward_idx,4), forward_data(forward_idx,6)]...
            ,[forward_data(forward_idx,5),forward_data(forward_idx,7)]...
            ,'-o','Color','green');
        set(h_forward,'xdata',get(h_forward,'xdata')+offset_x);
        set(h_forward,'ydata',get(h_forward,'ydata')+offset_y);
        hold on
    end
    xlim([0 1024])
    ylim([0 768])
    h=gca;
    set(h,'YDir','reverse');

%% Manual Identification of sweeps and regressions
% The image that was read before is now displayed on the screen
fig_handle=figure(1);
image(RGB_image);
hold on;
% Using the data from above we plot the saccadic data onto the image file 
for saccade_idx=1:length(x_position_start)
     if saccade_amplitude_h(saccade_idx)<0
        plot([x_position_start(saccade_idx) x_position_end(saccade_idx)],...
            [y_position_start(saccade_idx) y_position_end(saccade_idx)],'-o')
        hold on; 
     end
end

hold off;
xlim([0 1024])
ylim([0 768])
h=gca;
set(h,'YDir','reverse')
% After we plot we set the figure to data cursor mode which helps to get
% the collect the information from each data point as when the user clicks
% on it 
dcm_obj = datacursormode(figure(1));set(dcm_obj,'DisplayStyle','datatip',...
                    'SnapToDataVertex','on','Enable','on')
uiwait(fig_handle);

% We get the output in struct format and hence needs to be saved to matlab
% array format which contains the x and y coordinates of each of the clicks
% that were made
click_coordinates = cell2mat({cursor_info.Position}');

 %% Manual Selection:Plotting sweeps and regressions
% % For this we loop through the elements of the coordinates and also the
% % data array and identify the ones from our array and declare them as
% % regressions and the remainder as sweeps
% for click_idx=1:length(click_coordinates)
%     tol=eps(0.5);
%     sweep_counter=1 ;%Intitiate a counter for calculating the overall number of sweeps
%         for saccade_idx=1:length(data_array)
%             if saccade_amplitude_h(saccade_idx)<0
%                 if abs(click_coordinates(click_idx,1)==(data_array(saccade_idx,4+offset_x)))
%                     regression_data{click_idx}=data_array(saccade_idx,:);
%                     break;
%                 elseif abs((click_coordinates(click_idx,1))-(data_array(saccade_idx,6+offset_x)))< tol
%                     regression_data{click_idx}=data_array(saccade_idx,:); %#ok<*SAGROW>
%                     break;
%                 else
%                     sweep_data{sweep_counter}=data_array(saccade_idx,:);
%                     sweep_counter=sweep_counter+1;
%                 end
%             end
%         end
% end
%% Data Cursor Function: For automatic data collection
    function output_txt = myupdatefcn(~,event_obj)
      % ~            Currently not used (empty)
      % event_obj    Object containing event data structure
      % output_txt   Data cursor text
      pos = get(event_obj, 'Position');
      output_txt = {['x: ' num2str(pos(1))], ['y: ' num2str(pos(2))]};
    end


%% Checkbox control function:
%Ensure that the line number is always a string
    function button=check_box_func(figure_handle,y_position,line_num)
        button = uicontrol(figure_handle,'Style',...
            'checkbox',...
            'String',line_num,...
            'Position',[300 y_position 50 30],...
            'HandleVisibility','off',...
            'Value',1);
    end
    