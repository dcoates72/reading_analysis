# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 16:52:58 2019

@author: CLAB
"""
import os
import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()
import pandas as pd
import numpy as np

data_directory=filedialog.askdirectory(title='Please select data directory')
WORD=0
POSX=1
POSY=2
nonlast=[]
for dirname,subdirlist,filelist in os.walk(data_directory):
    for filename in filelist:     
        if 'reading_word_data' in filename and filename.endswith(".txt"):
            df_word_info=np.array(pd.read_csv(os.path.join(dirname,filename),names=['word','xpos','ypos'],delimiter='\t'))
            
            print(len(df_word_info))
            for nword,word in enumerate(df_word_info[:-1]):
                if df_word_info[nword+1][POSY]!=df_word_info[nword][POSY]:
                    continue                
                stats=[len(word[0]),df_word_info[nword+1][POSX]-df_word_info[nword][POSX]]
                if nonlast==[]:
                    nonlast=[stats]
                else:
                    nonlast=np.concatenate((nonlast,[stats]))