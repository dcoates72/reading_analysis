# -*- coding: utf-8 -*-
"""
Created on Fri Oct 26 18:06:36 2018

@author: krish
"""
##This program uses the asc file that is obtained from the EyeLink  and filters out the different parameters from the raw data file
#and puts them into separate files. 
#This request the user to provide the location of the folder that  
#1. contains the raw data
#2. is to be used to save the filtered data


#This loops through all the files that are present in the data directory and separates them into saccade and fixation data which is then used 
#to run statistics on

def eye_trace_analysis_postsorting(data_directory):
    import pandas as pd
    import os
    import Eye_Trace_Statistics
    import Name_Change_Function
    saccade_counter=1#Indicates the loop iteration number; one for saccades and one for fixations
    fixation_counter=1
    #Skips the data from subject 3 
#    if "Subject 3" in data_directory.lower():
#        return
    #identifies the position of the PRL that was used which is then written in the summary file
    if "inferior" in data_directory.lower():
        prl_position='inferior'
    elif "left" in data_directory.lower():
        prl_position='left'
    elif "right" in data_directory.lower():
        prl_position='right'
    else:
        prl_position='N/A'
    
    #identifies whether a word highlight was used or not
    if "guides" in data_directory.lower():
        highlight='with highlight'
    elif "foveal" in data_directory.lower():
        prl_position="Foveal"
        highlight="N/A"
    else:
        highlight='no highlight'
        
    #identifies if it is a training session or main experiment
    if "training" in data_directory.lower():
        training='True'
    else:
        training='False'
    
    #The data directory that contains the parsed files is used and we go through one file at a time and the 
    #trial by trial statistics as well as the summary statstics gets built 
    for filename in os.listdir(data_directory):  
        ##Creates a new text file that parses through the data and pulls out the saccade data & creates a pandas dataframe 
        print(filename)
        if "saccade_event_without_sweep" in filename:
            os.chdir(os.path.join(data_directory))
            dfes=pd.read_csv(os.path.join(data_directory,filename),delimiter='\t',skip_blank_lines=True,index_col=False)
#            dfes=pd.read_csv(os.path.join(data_directory,filename),delimiter='\t',header=None,skiprows=[0],skip_blank_lines=True,index_col=False,names=['Time Start',
#                             'Time End','Saccade Duration','Pos x(start)','Pos y(start)','Pos x(end)','Pos y(end)'])         
            saccade_duration_list,saccade_amplitude_h_list_forward,saccade_amplitude_h_list_backward,\
            saccade_amplitude_v_list,saccade_amplitude_h_list_forward_clean,saccade_amplitude_h_list_backward_clean\
                            =Eye_Trace_Statistics.saccade_stats_bytrial(dfes)
                            
            #Now we create a separate variable that gets the number of words & the overall\
            #trial durationthat were presented in each of these trials which is\
            #then added to the summary data
            #When using the Friedman Parser this modification needs to be made inorder to be able to access word data file
            # and the trial duration file
#            modified_filename_saccade=filename.replace('__MNH_','_') 
#            word_data_filename=Name_Change_Function.name_change("reading_word_data",modified_filename_saccade)
#            print(word_data_filename)
#            word_file=open(os.path.join(data_directory,word_data_filename),'rt')
#            word_data=word_file.readlines()
#            num_words_trial=len(word_data)
#            
#            trial_duration_filename=Name_Change_Function.name_change("trial_duration_file",modified_filename_saccade)
#            trial_duration_file=open(os.path.join(data_directory,trial_duration_filename),'rt')
#            trial_duration_data=trial_duration_file.readlines()[0]
#            print(len(saccade_duration_list))
#            print(len(saccade_amplitude_h_list_forward))
#            print(len(saccade_amplitude_h_list_backward))
#            print(len(saccade_amplitude_v_list))
#            print(len(saccade_duration_list))
#            print(len(peak_velocity_list))
            saccade_stats=open('saccade_statistics_without_sweep_%s.txt'%(filename[filename.find('S0'):-4]),"w")
            saccade_summary=open('saccade_summary_without_sweep_%s.txt'%(filename[28:31]),"a") 
            #When we are in the first iteration of the loop we would want to have the headers to be written 
            #not otherwise 
            saccade_stats.write('Saccade Duration(ms)'+'\t'+'Saccade Amplitude H Forward'+'\t'+'Saccade Amplitude H Backward'+'\t'\
                                +'Saccade Amplitude V'+'\n' )#'Peak Velocity'
            for i in range(len(saccade_duration_list)):
                saccade_stats.write(str(saccade_duration_list[i])+'\t'+str(saccade_amplitude_h_list_forward[i])+'\t'\
                                    +str(saccade_amplitude_h_list_backward[i])+'\t'+str(saccade_amplitude_v_list[i])+'\n')
                #str(peak_velocity_list[i])
            saccade_stats.close()
            #When we are in the first iteration of the loop we would want to have the headers to be written 
            #not otherwise
            if saccade_counter==1:
                saccade_summary.write('Filename'+'\t'+'Subject_ID'+'\t'+'Session_No'+'\t'+'Trial_No'+'\t'+'Training'+'\t'+'Training_Trial_Number'+'\t'+\
                                      'PRL_Position'+'\t'+'Presentation_Mode'+'\t'+'Mean_Saccade_Duration'+'\t'+'SD_saccade_duration'+'\t'+'Number_of_saccades'+'\t'+\
                                      'Number_of_forward_Saccades'+'\t'+'Number_of_backward_Saccades'+'\t'+'Mean_Saccade_Amplitude_H_Forward'+'\t'+\
                                      'SD_Saccade_Amplitude_H_Forward'+'\t'+'Mean_Saccade_Amplitude_H_Backward'+'\t'+'SD_Saccade_Amplitude_H_Backward'+'\t'+\
                                      'Mean_Saccade_Amplitude_V'+'\t'+'SD_Saccade_Amplitude_V'+'\n')
                saccade_counter+=1
            mean_saccade,std_saccade,num_saccade,mean_saccade_amplitude_H_forward,std_saccade_amplitude_H_forward,\
            mean_saccade_amplitude_H_backward,std_saccade_amplitude_H_backward,mean_saccade_amplitude_V,\
            std_saccade_amplitude_V,num_forward_saccade,num_backward_saccade=Eye_Trace_Statistics.saccade_stats_summary(saccade_duration_list,\
                                                                           saccade_amplitude_h_list_forward_clean,saccade_amplitude_h_list_backward_clean,\
                                                                           saccade_amplitude_v_list)
            
            trial_num_loc=filename.find('_T')
            saccade_summary.write(str(filename[28:-4])+'\t'+str(filename[28:31])+'\t'+str(filename[32])+'\t'+str(filename[trial_num_loc+2:filename.find("_",trial_num_loc+1)])+'\t'+str(training)+'\t'\
                                  +str(filename[trial_num_loc+2:filename.find("_",trial_num_loc+1)])+'\t'+str(prl_position)+'\t'+str(highlight)+'\t'+str(mean_saccade)+'\t'+str(std_saccade)+'\t'
                                  +str(num_saccade)+'\t'+str(num_forward_saccade)+'\t'+str(num_backward_saccade)+'\t'+str(mean_saccade_amplitude_H_forward)+\
                                  '\t'+str(std_saccade_amplitude_H_forward)+'\t'+str(mean_saccade_amplitude_H_backward)+'\t'+str(std_saccade_amplitude_H_backward)+'\t'+\
                                  str(mean_saccade_amplitude_V)+'\t'+str(std_saccade_amplitude_V)+'\n')
            saccade_summary.close()
        #Creates a new text file that parses through the data and pulls out the fixation data & creates a pandas dataframe 
#        elif "eyetrace_events_MNH_fixation" in filename:
#            os.chdir(os.path.join(data_directory))
#            fixation_stats=open('fixation_statistics_MNH_%s.txt'%(filename[29::]),"w")
#            fixation_summary=open('fixation_summary_MNH_%s.txt'%(filename[29:32]),"a")
#            dfef=pd.read_csv(os.path.join(data_directory,filename),delimiter='\t',skip_blank_lines=True,index_col=False)
##            dfef=pd.read_csv(os.path.join(data_directory,filename),delimiter='\t',header=None,skiprows=[0],skip_blank_lines=True,index_col=False,names=['Time Start',\
##                             'Time End','Fixation Duration','Pos x','Pos y'])
#            fixation_duration_list,fixation_pos_x_list,fixation_pos_y_list=Eye_Trace_Statistics.fixation_stats_bytrial(dfef) #pupil_size_list
#            fixation_stats.write('Fixation Duration'+'\t'+'Fixation Pos X'+'\t'+'Fixation Pos Y'+'\n')#'Pupil Size'
#            
#            #Now we create a separate variable that gets the number of words & the overall\
#            #trial durationthat were presented in each of these trials which is\
#            #then added to the summary data
#            modified_filename_fixation=filename.replace('_MNH_','_')
##            word_data_filename=Name_Change_Function.name_change("reading_word_data",modified_filename_fixation)
##            print(word_data_filename)
##            word_file=open(os.path.join(data_directory,word_data_filename),'rt')
##            word_data=word_file.readlines()
##            num_words_trial=len(word_data)
##                        
#            trial_duration_filename=Name_Change_Function.name_change("trial_duration_file",modified_filename_fixation)
##            print(trial_duration_filename)
#            try:
#                trial_duration_file=open(os.path.join(data_directory,trial_duration_filename),'rt')
#                trial_duration_data=trial_duration_file.readlines()[0]
#            except:
#                print(trial_duration_filename)
#                trial_duration_data=0
##            print(trial_duration_file)
#            
#            
#            for i in range(len(dfef)):
#                fixation_stats.write(str(fixation_duration_list[i])+'\t'+str(fixation_pos_x_list[i])+'\t'+str(fixation_pos_y_list[i])+\
#                                    '\n')#str(pupil_size_list[i])
#            fixation_stats.close()
#            if fixation_counter==1:
#                fixation_summary.write('Filename'+'\t'+'Subject_ID'+'\t'+'Session_No'+'\t'+'Trial_No'+'\t'+'Training'+'\t'+'Training_Trial_Number'+'\t'+\
#                                       'PRL_Position'+'\t'+'Presentation_Mode'+'\t'+'Mean_Fixation_Duration'+'\t'+'SD_Fixation_Duration'+'\t'+'Num_Fixations'+'Trial_Duration'+'\t'\
#                                       +'\n')
#                fixation_counter+=1
#            mean_fixation,std_fixation,num_fixations=Eye_Trace_Statistics.fixation_stats_summary(fixation_duration_list)
#            trial_num_loc=filename.find('_T')
#            fixation_summary.write(str(filename[29:-8])+'\t'+str(filename[29:32])+'\t'+str(filename[33])+'\t'+str(filename[trial_num_loc+2:filename.find("_",trial_num_loc+1)])+'\t'+str(training)+'\t'\
#                                   +str(filename[trial_num_loc+2:filename.find("_",trial_num_loc+1)])+'\t'+str(prl_position)+'\t'+str(highlight)+'\t'+str(mean_fixation)+'\t'+str(std_fixation)+'\t'+\
#                                   str(num_fixations)+'\t'+str(trial_duration_data)+'\n')
#            fixation_summary.close()
           
#    a=[]
#    for i in range(1,len(dfef)-1): # Parses through the fixation data compares the horizontal position and prints the pair of positions that have a difference of 50 pixels or greater
#        if dfef.loc[i,"Pos x"]-dfef.loc[i+1,"Pos x"]>=100:
#            a.append((dfef.loc[i,"Pos x"], dfef.loc[i+1,"Pos x"],dfef.loc[i,"Pos y"], dfef.loc[i+1,"Pos y"]))
#        else:
#            pass
#    sweep=np.array(a,float)
        
        
