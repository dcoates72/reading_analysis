# -*- coding: utf-8 -*-
"""
Created on Fri Oct 26 14:12:37 2018

@author: krish
"""

#This program uses the asc file that is obtained from the EyeLink  and filters out the different parameters from the raz data file
#and puts them into separate files. 
#This request the user to provide the location of the folder that  
#1. contains the raw data
#2. is to be used to save the filtered data
#3. The user is also to provide the folder where the final filtered data is to be saved


import os
import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()
import pandas as pd
from Eye_Trace_Analysis_postsorting import eye_trace_analysis_postsorting
import Eye_Trace_Statistics
import Name_Change_Function

data_directory=filedialog.askdirectory(title='Please select data directory')

#This loops through all the files that are present in the data directory and filters out the required events from the raw data file
#We look through the whole data directory including sub folders and the different 
#files that is created by the program is organized into an appropriate folder 

#Hence the role of the user to provide the directory of the main folder that contains all the data files 
#The loop searches through the directory and the subdirectories and selects
#and converts the relevant files alone 
for dirName,subdirlist,filelist in os.walk(data_directory):

#NOTE!!: Uncomment the following set of lines in cases where the EYELINK Parser is being used for data parsing otherwise we can skip 
    # the filtering phase and start off with the part that carries out the main analysis. 
     
##This loops through all the files that are present in the data directory and filters out the required events from the raw data file
#    for filename in filelist:     
#        if filename.endswith(".edf"):
#            pass
#        if 'reading_et_data' in filename and filename.endswith(".asc"):
#            os.chdir(os.path.join(dirName))
#            eyetrace=open(os.path.join(dirName,filename),'rt')#,encoding="ISO-8859-1")
#            print(eyetrace)
#            file=eyetrace.readlines()
#            filtered_sample=open('sorted_samples_%s.txt'%(filename[16::]),"w")
#            filtered_events_saccades=open('eyetrace_events_saccades_%s.txt'%(filename[16::]),"w")
#            filtered_events_fixation=open('eyetrace_events_fixation_%s.txt'%(filename[16::]),"w")
#            filtered_events_saccades.write("Time Start"+'\t'+"Time End"+'\t'+"Saccade Duration"+'\t'+"Pos x(start)"+'\t'+"Pos y(start)"+'\t'+"Pos x(end)"+'\t'+"Pos y(end)"+'\t'+"hypot"+'\t'+"Peak velocity"+'\n')
#            filtered_events_fixation.write("Time Start"+'\t'+"Time End"+'\t'+"Fixation Duration"+'\t'+"Pos x"+'\t'+"Pos y"+'\t'+"Pupil Size"+'\t'+'\n')
#            trial_duration_file=open('trial_duration_file_%s.txt'%(filename[16:-4]),"w")
#            #def eyetracesorter(): # Sorts the eye trace data and excludes the general,calibration and validation information
#            for i in range(0,len(file)):# It runs through the eye movement file and finds out the starting point of the eye trace 
#                                        #of interest
#                if 'CLAB START' in str(file[i]):
#                    filtered_sample.write(str(file[i][4:]+'\n'))
#                    trial_start_time=file[i][file[i].find('MSG')+4:file[i].find('CLAB')]
#                    i=i+1
#                    break
#            for l in range(i,len(file)):# It runs through the eye movement file and finds out the end point of the eye trace of interest
#                if 'PAGE RIGHT' in str(file[l]):
#                    trial_end_time=file[l][file[l].find('MSG')+4:file[l].find('CLAB')]
#                elif str(file[l][-6:-1])== 'END':
#                    l=l-1
#                    break                    
#            for k in range(i,l): #It excludes the events and writes the sorted samples to a different txt file & the 
#                                 #events are separately written to a different file. 
#                if str(file[k][:5])=='ESACC':
#                    filtered_events_saccades.write(str(file[k][7:]))# It removes the 1 that occurs in the last column of the saccade event (which we dont know what it represents at this point)
#                elif str(file[k][:4])=='EFIX':
#                    filtered_events_fixation.write(str(file[k][6:])) 
#                elif str(file[k][0])=='S':
#                    continue
#                elif 'CLAB' in str(file[k]):
#                    continue
#                elif 'BUTTON' in str(file[k]):
#                    continue
#                elif str(file[k][1:6])=='BLINK':
#                    continue
#                elif '.\t' in str(file[k]):
#                    continue
#                else:
#                    filtered_sample.write(str(file[k][:-4]+'\n'))
#            
#            trial_duration=(int(trial_end_time)-int(trial_start_time))
#            trial_duration_file.write(str(trial_duration)+'\n')
#            
#            trial_duration_file.close()
#            filtered_sample.close()
#            filtered_events_saccades.close()
#            filtered_events_fixation.close()
            
    eye_trace_analysis_postsorting(dirName)
    
    #We build a pandas data frame that contains the summary data from all the subjects
    #for all the trials
    saccade_summary_df=pd.DataFrame([])
#    saccade_summary_df=pd.DataFrame(columns=["Filename","Subject ID"+"Session No","Trial No.","PRL Position","Presentation Mode"\
#                                      ,"Mean Saccade Duration","SD saccade duration","Number of saccades",\
#                                      "Mean Saccade Amplitude H Forward","SD Saccade Amplitude H Forward",\
#                                      "Mean Saccade Amplitude H Backward","SD Saccade Amplitude H Backward",\
#                                      "Mean Saccade Amplitude V","SD Saccade Amplitude V"])
#    fixation_summary_df=pd.DataFrame([])
#    fixation_summary_df=pd.DataFrame(columns=["Filename","Subject ID","Session No","Trial No.","PRL Position","Presentation Mode"\
#                                      ,"Mean Fixation Duration","SD Fixation Duration","Num Fixations"])
for dirName,subdirlist,filelist in os.walk(data_directory):
    for filename in filelist:
        if 'saccade_summary_without_sweep' in filename: 
            saccade_data=pd.read_csv(os.path.join(dirName,filename),delimiter='\t')  
            saccade_summary_df=saccade_summary_df.append(saccade_data)
#        elif 'fixation_summary_MNH' in filename:
#            fixation_data=pd.read_csv(os.path.join(dirName,filename),delimiter='\t')  
#            fixation_summary_df=fixation_summary_df.append(fixation_data)
saccade_summary_df.to_csv(os.path.join(data_directory,'Saccade summary without sweep DF 6 Subjects.txt'))
#fixation_summary_df.to_csv(os.path.join(data_directory,'Fixation summary MNH DF 6 Subjects.txt'))