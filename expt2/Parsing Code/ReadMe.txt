In order to parse and analyse the data from the Eyelink, the follow steps are to be done one after the other:

1. Before you begin the data analysis make sure that you convert the data from the Eyelink from the EDF to ASC 
format. 

Note: This program will not work on ASC files

2. Open the Eye Trace Analysis.py in your Python IDE, and run it on the platform. When asked to specify the 
location of the raw data please select the folder that contains the ASC files from the EyeLink

3. Next Indicate as to where the sorted files(which contains the saccades, fixation and samples data separetely)
to be stored. 

Note: This is not the final set of data and is only an intermediate step and the data would again have to be used
to run the different statistics. 

4. Open the Eye_Trace_Postsorting.py file in your Python IDE, and run it on the platform. It will ask you to 
specify the folders that contains the filtered data. 

At this point you should specify the path that contains the files that were generated in Step 3. 

Next, it will require the user to specify the path that is to be used to store the statistics file for each trial. 

5. The final step would be visualize the data, in order to do this you will have to run the Data_visualization.py 
file in your Pyton IDE, and run the same on your program. 

You will be asked to specify the file that you would like to visualize and also indicate as to whether you would like
the traces to be overlayed on the image that the subject was presented with during the experiment. (The second part
can be skipped if you do not want to use an image as the background)

Note: This program can be tweaked in order to match the requirements of the user. 