# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 17:09:50 2019

@author: krish
"""

import os
import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()
from tkinter import messagebox

#Set the path where you wish to alter the file sequence
data_directory=filedialog.askdirectory(title='Please select data directory')
#The alue for the edf counter is set based on the the options that the user 
#clicks on. Our indexing goes from 0-59 for the 60 trials and depending on
#which condition was done first this would change.
#user_response_1=messagebox.askyesno("Title","Was this done first?")
#user_response_2=messagebox.askyesno("Title","Was a fixation cross used?")
#if user_response_1==True:
#    if user_response_2==True:
#        edf_counter=0
#    else:
#        edf_counter=25
#else:
#    if user_response_2==True:
#        edf_counter=30
#    else:
#        edf_counter=55
#KSP: Renaming Right vs left experiment training was done using only simulated scotoma (highlight condition not
# not used in this experiment) hence counter always starts at zero
edf_counter=0
#edf_counter=int(input('Please enter the first number of the sequence' ))
#The same number is used for both the reading word data and png files as they
#basically follow the same notation as the edf file
word_data_counter=edf_counter
png_counter=edf_counter

os.chdir(data_directory)
#The program initially sorts the files based on date of creation and then
#searches through the directory and renames the file based on the indexing 
#values provided
for filename in sorted(filter(os.path.isfile, os.listdir('.')), key=os.path.getmtime):
    
    if filename.endswith('.edf'):
        left_part_string=filename[:21]
        right_part_string=filename[25:]
        old_string_edf=filename
        part_to_be_changed=filename[21:25]
        new_part=("%s%d%s"%(filename[21:23],edf_counter,filename[24:25]))
        new_string_edf=("%s%s%s"%(left_part_string,new_part,right_part_string))
        print(old_string_edf)
        try:
#            os.rename(old_string,new_string)
            os.system('%s %s %s %s'%('C:/Progra~1/Git/bin/git.exe','mv',old_string_edf,new_string_edf))
        except FileExistsError:
            print('file exists')
            
        print(new_string_edf)
        edf_counter+=1

    elif filename.endswith('.txt'):
        left_part_string_txt=filename[:23]
        right_part_string_txt=filename[27:]
        old_string_txt=filename
        part_to_be_changed=filename[23:27]
        new_part=("%s%d%s"%(filename[23:25],word_data_counter,filename[26:27]))
        new_string_txt=("%s%s%s"%(left_part_string_txt,new_part,right_part_string_txt))
        print(filename)
        try:
#             os.rename(old_string_txt,new_string_txt)
             os.system('%s %s %s %s'%('C:/Progra~1/Git/bin/git.exe','mv',old_string_txt,new_string_txt))
        except FileExistsError:
            print('file exists') 
        old_asc= old_string_txt
        old_asc=old_asc.replace("word","et")
        old_asc=old_asc.replace("txt","asc")
        new_asc= new_string_txt
        new_asc=new_asc.replace("word","et")
        new_asc=new_asc.replace("txt","asc")
        os.system('%s %s %s %s'%('C:/Progra~1/Git/bin/git.exe','mv',old_asc,new_asc))


        old_png= old_string_txt
        old_png=old_png.replace("txt","txt.png")
        new_png= new_string_txt
        new_png=new_png.replace("txt","png")
        os.system('%s %s %s %s'%('C:/Progra~1/Git/bin/git.exe','mv',old_png,new_png))
        print(new_string_txt,old_asc,new_asc)
        word_data_counter+=1 

    
#Would eventually want to write a function that does the renaming of the file