# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:50:37 2019

@author: krish
"""
import os
import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()

data_directory=filedialog.askdirectory(title='Please select data directory')
for dirName,subdirlist,filelist in os.walk(data_directory):
    for filename in os.listdir(dirName):
        os.chdir(os.path.join(dirName))
        print(filename)
        if 'eyetrace_events__MNH_saccades' in filename:
            new_name=filename.replace('_03_','_S03_')
            os.rename(filename,new_name)
            os.system('%s %s %s %s'%('C:/Progra~1/Git/bin/git.exe','mv',filename,new_name))
        elif 'eyetrace_events_MNH_fixation' in filename:
            new_name=filename.replace('_03_','_S03_')
            os.rename(filename,new_name)
            os.system('%s %s %s %s'%('C:/Progra~1/Git/bin/git.exe','mv',filename,new_name))