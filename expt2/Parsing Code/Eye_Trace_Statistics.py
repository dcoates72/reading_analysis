# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 20:58:29 2019

@author: krish
"""
import pandas as pd
import numpy as np

#The following function is used to convert the data that is obtained from the eyelink
#from pixels to degrees (which is what is conventionally used)
    
#Note: Please ensure that the different parameters that go into this function
#match the display that you are using and the also distance between the observer
#and the display

#This is used to perform the conversion from pixels to degrees of visual angle

#We can pass in the parameters for the display that is being usesd which is then
#used to perform the calculations
class monitor_spec:     
    def __init__(self,screen_resolution_vertical,screen_resolution_horizontal,diagonal_screensize_cm,distance_to_screen_cm,scotoma_radius):
        self.screen_resolution_vertical=screen_resolution_vertical
        self.screen_resolution_horizontal=screen_resolution_horizontal
        self.diagonal_screensize_cm=diagonal_screensize_cm
        self.distance_to_screen_cm= distance_to_screen_cm
        self.scotoma_radius=scotoma_radius
    def pixel_to_degrees(self,value_in_pixels):
        diagonal_pixel=np.sqrt(((self.screen_resolution_vertical)**2)+((self.screen_resolution_horizontal)**2))
        pixel_per_cm=diagonal_pixel/self.diagonal_screensize_cm
        scotoma_radius_cm=self.scotoma_radius/pixel_per_cm
        scotoma_dia_cm=scotoma_radius_cm*2
        # This denotes the radius of the scotoma in degrees
        scotoma_radius_degrees= (scotoma_radius_cm/self.distance_to_screen_cm) * 180/np.pi 
        scotoma_dia_degrees=scotoma_radius_degrees*2
        degree_per_cm=(1/self.distance_to_screen_cm)* 180/np.pi
        #Here we calculate the number of degrees within each pixel which can 
        #then be used to perform the pixel to degree conversion
        degrees_per_pixel=degree_per_cm/pixel_per_cm
        #The following are the different parameters that the function returns
        self.value_in_pixels=value_in_pixels
        pixel_coversion=degrees_per_pixel*value_in_pixels
        return pixel_coversion

#The following are the parameters of the two monitors that are used and the current
#values are for when the instrument is set up for tower mount         
monitor='LCD'
if monitor=='LCD':
    monitor=monitor_spec(1080,1920,62.23,57,66)#(768,1024,50,42,66)
elif monitor=='CRT':
    monitor=monitor_spec(768,1024,44.5,42,75)

# This program basically has the different functions which is later called 
#into the parsing program to run the statistics 
    
#The function calculates the different metrics for saccades that are done 
#on a trial by trial basis
def saccade_stats_bytrial(array):
    saccade_duration_list=[]
    saccade_amplitude_h_list_forward=[]
    saccade_amplitude_h_list_forward_clean=[]
    saccade_amplitude_h_list_backward=[]
    saccade_amplitude_h_list_backward_clean=[]
    saccade_amplitude_v_list=[]
    #peak_velocity_list=[] #use this when using Eyelink Parser
    array['Pos x(start)']=pd.to_numeric(array['Pos x(start)'],errors='coerce')
    if np.isnan(array['Pos x(start)'].iloc[0]):
        valids=range(1,len(array))
    else: 
        valids=range(len(array))
    array['Pos y(start)']=pd.to_numeric(array['Pos y(start)'],errors='coerce')
    array['Pos x(end)']=pd.to_numeric(array['Pos x(end)'],errors='coerce')
    #This checks as to whether the values in the two columns are actually valid numbers 
    if np.isnan(array['Pos x(end)'].iloc[-1]):
        valids=valids[:-1]
    array['Pos y(end)']=pd.to_numeric(array['Pos y(end)'],errors='coerce')
    for i in valids:
        saccade_duration_list.append(array.iloc[i,2])
        saccade_amplitude_h=((array.iloc[i,5])-(array.iloc[i,3])) #This indicates the size of the saccades in pixels in the horizontal meridian
        #peak_velocity_list.append(array.iloc[i,8])
        saccade_amplitude_v=np.abs((array.iloc[i,6])-(array.iloc[i,4]))
        saccade_amplitude_v_list.append(saccade_amplitude_v)
        if saccade_amplitude_h>=0:
            #The values are placed in of the list and an empty character is added
            #to the other list so as to ensure that the lengths of the two 
            #lists matches the original list
            
            #Note: Another list which is devoid of the empty character is also 
            #built so that it can later be used to perform statistics on. This 
            #list bears the same name as the original list but has the addition 
            #term clean at the end 
            saccade_amplitude_h_list_forward.append(saccade_amplitude_h)
            saccade_amplitude_h_list_forward_clean.append(saccade_amplitude_h)
            saccade_amplitude_h_list_backward.append(' ')            
        elif saccade_amplitude_h<0:
            saccade_amplitude_h_list_backward.append(saccade_amplitude_h)
            saccade_amplitude_h_list_backward_clean.append(saccade_amplitude_h)
            saccade_amplitude_h_list_forward.append(' ')

    
#The following are the differnt parameters that the function returns for saccade data    
    return saccade_duration_list,saccade_amplitude_h_list_forward,saccade_amplitude_h_list_backward,\
    saccade_amplitude_v_list,saccade_amplitude_h_list_forward_clean,saccade_amplitude_h_list_backward_clean #peak_velocity_list

#The following function provides the saccade summary statistics which uses the information
#from the function saccade_stats_bytrial function
    
#This creates one single file for each folder with a set of files with the summary
#metrics 
def saccade_stats_summary(saccade_duration_list,saccade_amplitude_horizontal_forward,\
                          saccade_amplitude_horizontal_backward,saccade_amplitude_vertical):
    total_saccade_duration=np.array(saccade_duration_list)
    mean_saccade=np.mean(total_saccade_duration,axis=0)
    std_saccade=np.std(total_saccade_duration,axis=0)
    num_saccade=len(saccade_duration_list) 
    num_forward_saccade=len(saccade_amplitude_horizontal_forward)
    num_backward_saccade=len(saccade_amplitude_horizontal_backward)
    saccade_amplitude_H_forward=monitor.pixel_to_degrees(np.array(saccade_amplitude_horizontal_forward))
    mean_saccade_amplitude_H_forward=np.mean(saccade_amplitude_H_forward,axis=0)
    std_saccade_amplitude_H_forward=np.std(saccade_amplitude_H_forward,axis=0)
    saccade_amplitude_H_backward=monitor.pixel_to_degrees(np.array(saccade_amplitude_horizontal_backward))
    mean_saccade_amplitude_H_backward=np.mean(saccade_amplitude_H_backward,axis=0)
    std_saccade_amplitude_H_backward=np.std(saccade_amplitude_H_backward,axis=0)    
    saccade_amplitude_V=monitor.pixel_to_degrees(np.array(saccade_amplitude_vertical))
    mean_saccade_amplitude_V=np.mean(saccade_amplitude_V,axis=0)
    std_saccade_amplitude_V=np.std(saccade_amplitude_V,axis=0)    
#The following are the different parameters that the function returns for saccade data    
    return mean_saccade,std_saccade,num_saccade,mean_saccade_amplitude_H_forward,\
    std_saccade_amplitude_H_forward,mean_saccade_amplitude_H_backward,\
    std_saccade_amplitude_H_backward,mean_saccade_amplitude_V,std_saccade_amplitude_V,num_forward_saccade,\
    num_backward_saccade

#The function calculates the different metrics for fixations that are done 
#on a trial by trial basis
def fixation_stats_bytrial(array):
    fixation_duration_list=[]
    fixation_pos_x_list=[]
    fixation_pos_y_list=[]
    #pupil_size_list=[] #Use this when using Eyelink Parser
    for i in range(len(array)):
        fixation_duration_list.append(array.iloc[i,2])
        fixation_pos_x_list.append(array.iloc[i,3])
        fixation_pos_y_list.append(array.iloc[i,4])
       #pupil_size_list.append(array.iloc[i,5])
#The following are the different parameters that the function returns for fixation data
    return fixation_duration_list,fixation_pos_x_list,fixation_pos_y_list#pupil_size_list

#This creates one single file for each folder with a set of files with the summary
#metrics 
def fixation_stats_summary(fixation_duration_list):
    total_fixation_duration=np.array(fixation_duration_list)
    mean_fixation=np.mean(total_fixation_duration,axis=0)
    std_fixation=np.std(total_fixation_duration,axis=0)
    num_fixations=len(fixation_duration_list)
#The following are the different parameters that are returned from this function
    return mean_fixation,std_fixation,num_fixations    