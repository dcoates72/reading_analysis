# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 10:58:50 2019

@author: krish
"""
import os
import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()

data_directory=filedialog.askdirectory(title='Please select data directory')
for dirName,subdirlist,filelist in os.walk(data_directory):

#NOTE!!: Uncomment the following set of lines in cases where the EYELINK Parser is being used for data parsing otherwise we can skip 
    # the filtering phase and start off with the part that carries out the main analysis. 
     
##This loops through all the files that are present in the data directory and filters out the required events from the raw data file
    for filename in filelist:     
        if filename.endswith(".edf"):
            pass
        if 'reading_et_data' in filename and filename.endswith(".asc"):
            os.chdir(os.path.join(dirName))
            eyetrace=open(os.path.join(dirName,filename),'rt')#,encoding="ISO-8859-1")
            print(eyetrace)
            file=eyetrace.readlines()
            page_num=1 # we define the page number here and is then incremented within the for loop
            #def eyetracesorter(): # Sorts the eye trace data and excludes the general,calibration and validation information
            for i in range(0,len(file)):# It runs through the eye movement file and finds out the starting point of the eye trace 
                                        #of interest
                if 'CLAB START' in str(file[i]):
                    #filtered_sample.write(str(file[i][4:]+'\n'))
                    trial_start_time=file[i][file[i].find('MSG')+4:file[i].find('CLAB')]
                    i=i+1
                    break
            for l in range(i,len(file)):# It runs through the eye movement file and finds out the end point of the eye trace of interest
    #                if 'PAGE RIGHT' in str(file[l]):
    #                    trial_end_time=file[l][file[l].find('MSG')+4:file[l].find('CLAB')]
                if str(file[l][-6:-1])== 'END':
                    l=l-1
                    break
            page_end_list=[]
            next_page_start_list=[]
            for k in range(i,l): #It excludes the events and writes the sorted samples to a different txt file & the 
                                 #events are separately written to a different file. 
                if 'RIGHT' in str(file[k]) and 'PAGE' in str(file[k]) :
                    page_end=k;
                    page_end_list.append(page_end)
                    for j in  range(page_end,l):
                        if 'GO' in str(file[j]) and 'PAGE' in str(file[j]):
                            next_page_start=j
                            break
    #                    elif 'RIGHT' in str(file[j]) and 'PAGE' in str(file[j]):
    #                        next_page_start=j
    #                        break
                    next_page_start_list.append(next_page_start)
    #                page_num+=1
    #                continue
            for page_number in range(0,8):
                filtered_sample=open('sorted_samples_page%s_%s.txt'%(page_num,filename[16::]),"w")
                filtered_events_saccades=open('eyetrace_events_saccades_page%s_%s.txt'%(page_num,filename[16::]),"w")
                filtered_events_fixation=open('eyetrace_events_fixation_page%s_%s.txt'%(page_num,filename[16::]),"w")
                filtered_events_saccades.write("Time Start"+'\t'+"Time End"+'\t'+"Saccade Duration"+'\t'+"Pos x(start)"+'\t'+"Pos y(start)"+'\t'+"Pos x(end)"+'\t'+"Pos y(end)"+'\t'+"hypot"+'\t'+"Peak velocity"+'\n')
                filtered_events_fixation.write("Time Start"+'\t'+"Time End"+'\t'+"Fixation Duration"+'\t'+"Pos x"+'\t'+"Pos y"+'\t'+"Pupil Size"+'\t'+'\n')
                trial_duration_file=open('trial_duration_file_page%s_%s.txt'%(page_num,filename[16:-4]),"w")
                #timing_counter=0
                if page_number==0:
                    lower_limit=i;
                    upper_limit=page_end_list[page_number]
                elif page_number==7:
                    lower_limit=next_page_start_list[-1]
                    upper_limit=l
                else:
                    lower_limit=next_page_start_list[page_number-1]
                    upper_limit=page_end_list[page_number]
                for line_num in range(lower_limit,upper_limit):
                    if str(file[line_num][:5])=='ESACC':
                        filtered_events_saccades.write(str(file[line_num][7:]))# It removes the 1 that occurs in the last column of the saccade event (which we dont know what it represents at this point)
                    elif str(file[line_num][:4])=='EFIX':
                        filtered_events_fixation.write(str(file[line_num][6:])) 
                    elif str(file[line_num][0])=='S':
                        continue
                    elif 'CLAB' in str(file[line_num]):
                        continue
                    elif 'BUTTON' in str(file[line_num]):
                        continue
                    elif str(file[line_num][1:6])=='BLINK':
                        continue
                    elif '.\t' in str(file[line_num]):
                        continue
                    else:    
                         #timing_counter+=1
                        filtered_sample.write(str(file[line_num][:-6]+'\n'))
                try:
                    trial_start_timestamp=int(file[lower_limit+1][0:file[lower_limit+1].find('\t')])
                except:
                    print('BUG')
                    if 'GO' in file[lower_limit]:
                        trial_start_timestamp=int(file[lower_limit][file[lower_limit].find('MSG')+4:file[lower_limit].find('CLAB')])
#                else:    
#                    trial_start_timestamp=int(file[lower_limit][0:file[lower_limit].find('\t')])
                try:
                    trial_end_timestamp=int(file[upper_limit-1][0:file[upper_limit-1].find('\t')])
                except:
                    print('BUG')
                    if 'MSG' in file[upper_limit]:
                        trial_end_timestamp=int(file[upper_limit][file[upper_limit].find('MSG')+4:file[upper_limit].find('CLAB')])
                    else:
                        trial_end_timestamp=int(file[upper_limit][file[upper_limit].find('END')+4:file[upper_limit].find('\tSAMPLES')])
                timing_counter=trial_end_timestamp-trial_start_timestamp
                page_num+=1
                trial_duration=timing_counter#/2000 # 1000 for ms to sec conversion and 2 since we are sampling at 2khz
                                                    #there will be two samples with the same time stamp
                trial_duration_file.write(str(trial_duration)+'\n')
                trial_duration_file.close()
                filtered_sample.close()
                filtered_events_saccades.close()
                filtered_events_fixation.close()
                    