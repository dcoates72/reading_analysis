# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 18:28:37 2018

@author: krish
"""

import os
os.chdir('D:\\KSP\\Study\\Houston\\Projects\Reading Project\\Project Code\\reading\\Results\\All Subjects\\Index Files')
data_directory='D:\\KSP\\Study\\Houston\\Projects\\Reading Project\\Project Code\\reading\\Results\\All Subjects\\Raw Data'
for filename in os.listdir(data_directory):
        eyetrace=open(os.path.join(data_directory,filename),'rt')#,encoding="ISO-8859-1")
        file=eyetrace.readlines()
        index_file=open('index file %s.txt'%(filename[16:19]),"a")
        #index_file.write('Subject ID'+'\t'+'Condition'+'\t'+'Eye'+'\t'+'Paragraph Number'+'\t')
        for i in range (0,len(file)):
            if "subject" in file[i]:
                index_file.write(filename[16:19]+'\t')
            if 'Paragraph_number' in file[i]:
                para_num=str(file[i][35:-12])
            if "eye mode=foveal" in file[i]:
                index_file.write("Foveal"+'\t')
            if "eye mode=simulated only" in file[i]:
                index_file.write("Simulated Only"+'\t')
            if 'eye mode=simulated w/o gaussian' in file[i]:
                index_file.write('Simulated W/O Gaussian'+'\t')
            if 'eye mode=simulated with all guides' in file[i]:
                index_file.write('Simulated with all guides'+'\t')
            if "RIGHT	SAMPLES	EVENTS" in file[i]:
                index_file.write("Right"+'\t'+para_num+'\n')
            if "LEFT	SAMPLES	EVENTS" in file[i]:
                index_file.write("Left"+'\t'+para_num+'\n')

    
            
        index_file.close()