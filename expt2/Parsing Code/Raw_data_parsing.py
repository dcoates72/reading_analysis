# -*- coding: utf-8 -*-
"""
Created on Wed May 22 14:46:09 2019

@author: krish
"""
import os
import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()

data_directory=filedialog.askdirectory(title='Please select data directory')
for dirName,subdirlist,filelist in os.walk(data_directory):

#NOTE!!: Uncomment the following set of lines in cases where the EYELINK Parser is being used for data parsing otherwise we can skip 
    # the filtering phase and start off with the part that carries out the main analysis. 
     
##This loops through all the files that are present in the data directory and filters out the required events from the raw data file
    for filename in filelist:     
        if filename.endswith(".edf"):
            pass
        if 'reading_et_data' in filename and filename.endswith(".asc"):
            os.chdir(os.path.join(dirName))
            eyetrace=open(os.path.join(dirName,filename),'rt')#,encoding="ISO-8859-1")
            print(eyetrace)
            file=eyetrace.readlines()
            filtered_sample=open('sorted_samples_%s.txt'%(filename[16::]),"w")
            filtered_events_saccades=open('eyetrace_events_saccades_%s.txt'%(filename[16::]),"w")
            filtered_events_fixation=open('eyetrace_events_fixation_%s.txt'%(filename[16::]),"w")
            filtered_events_saccades.write("Time Start"+'\t'+"Time End"+'\t'+"Saccade Duration"+'\t'+"Pos x(start)"+'\t'+"Pos y(start)"+'\t'+"Pos x(end)"+'\t'+"Pos y(end)"+'\t'+"hypot"+'\t'+"Peak velocity"+'\n')
            filtered_events_fixation.write("Time Start"+'\t'+"Time End"+'\t'+"Fixation Duration"+'\t'+"Pos x"+'\t'+"Pos y"+'\t'+"Pupil Size"+'\t'+'\n')
            trial_duration_file=open('trial_duration_file_%s.txt'%(filename[16:-4]),"w")
            #def eyetracesorter(): # Sorts the eye trace data and excludes the general,calibration and validation information
            for i in range(0,len(file)):# It runs through the eye movement file and finds out the starting point of the eye trace 
                                        #of interest
                if 'CLAB START' in str(file[i]):
                    filtered_sample.write(str(file[i][4:]+'\n'))
                    trial_start_time=file[i][file[i].find('MSG')+4:file[i].find('CLAB')]
                    i=i+1
                    break
            for l in range(i,len(file)):# It runs through the eye movement file and finds out the end point of the eye trace of interest
                if 'PAGE RIGHT' in str(file[l]):
                    trial_end_time=file[l][file[l].find('MSG')+4:file[l].find('CLAB')]
                elif str(file[l][-6:-1])== 'END':
                    l=l-1
                    break                    
            for k in range(i,l): #It excludes the events and writes the sorted samples to a different txt file & the 
                                 #events are separately written to a different file. 
                if str(file[k][:5])=='ESACC':
                    filtered_events_saccades.write(str(file[k][7:]))# It removes the 1 that occurs in the last column of the saccade event (which we dont know what it represents at this point)
                elif str(file[k][:4])=='EFIX':
                    filtered_events_fixation.write(str(file[k][6:])) 
                elif str(file[k][0])=='S':
                    continue
                elif 'CLAB' in str(file[k]):
                    continue
                elif 'BUTTON' in str(file[k]):
                    continue
                elif str(file[k][1:6])=='BLINK':
                    continue
                elif '.\t' in str(file[k]):
                    continue
                else:
                    filtered_sample.write(str(file[k][:-4]+'\n'))
            
            trial_duration=(int(trial_end_time)-int(trial_start_time))
            trial_duration_file.write(str(trial_duration)+'\n')
            
            trial_duration_file.close()
            filtered_sample.close()
            filtered_events_saccades.close()
            filtered_events_fixation.close()
            