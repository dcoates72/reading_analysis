# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 14:36:09 2019

@author: krish
"""

import os

def name_change(prefix,original_filename):
    subject_information_string=original_filename[original_filename.find('_page'):-8]
    word_data_left_part_string=prefix
    word_data_right_part_string=".txt"
    word_data_filename=('%s%s%s'%(word_data_left_part_string,\
                      subject_information_string,word_data_right_part_string))
    
    return word_data_filename
    
    