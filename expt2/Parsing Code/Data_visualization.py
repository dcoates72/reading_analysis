# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 17:13:54 2019

@author: CLAB
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Jan 19 19:29:20 2019

@author: krish
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import matplotlib.image as mpimg
import tkinter as tk
from tkinter import filedialog
import os

#Part1 of Data Visualization program: This part of the program basically requests for the user to select a data file and an image 
#in order to use them for plotting . But these work mostly when there are individual trials. 
#So the next part will focus on displaying different plots that work on dataframes that contain the data from all of the trials that 
#were conducted


root = tk.Tk()
root.withdraw()
#This is used to open a new window which requires the user to indicate the location of the file that is to be used for plotting
#file_path = filedialog.askopenfilename(title='Please select data file')

#Open up a dialogue box that requests the user to provide the location of the data
#directory
data_directory=filedialog.askdirectory(title='Please select data directory')
text_height=34
text_width=34*0.52
scotoma_radius=132

try:
    #The loop searches through the directory and the subdirectories and selects
    #and converts the relevant files alone 
    for dirName,subdirlist,filelist in os.walk(data_directory):
         
    #This loops through all the files that are present in the data directory and filters out the required events from the raw data file
        for filename in filelist:  
            #occassionally the filename of the events file shows up with an extension 
            #".txt.txt" in order to prevent this from throwing errors we change the filename 
            #such that it matches with the other files 
            if 'events_fixation' or 'events_saccades' in filename: 
                if '.txt.txt' in filename:
                    filename=filename.replace("txt.txt","txt")
                    #now we create the name for the image file that is to be opened
                textimg_filename=('reading_word_data_%s.txt.png'%(filename[filename.find('_S')+1:-8]))
                print(filename,textimg_filename)
                textimg=mpimg.imread(os.path.join(dirName,textimg_filename))
                #Now we identify the experimental condition that was tested using the
                #information from the filepath
                if 'inferior' in dirName.lower():
                    prl_position='inferior'
                elif 'left' in dirName.lower():
                    prl_position='left'
                elif 'right' in dirName.lower():
                    prl_position='right'
                
                #Using the same concept now we set whether a highlight was used
                #or not
                if "guides" in dirName.lower():
                    highlight='with highlight'
                elif "foveal" in dirName.lower():
                    prl_position="foveal"
                    highlight="N/A"
                else:
                    highlight='no highlight'
                    
                    
                if "events_fixation" in filename:         
                    #This reads the fixation file and creates a pandas data frame whcih is then cast into a numpy array
                    fixation_file=pd.read_csv(os.path.join(dirName,filename),delimiter='\t',names=["Time Start","Time End","Fixation Duration", "Pos x", "Pos y","Pupil Size"])
                    data_array=np.array(fixation_file)
                    os.chdir(os.path.join(dirName))
                    plt.figure(figsize=(15,8))
                    if prl_position=='foveal':
                        offset_x=0
                        offset_y=0
                    #In cases where there is peripheral data this part helps to skew the data points such that it is more relation to the PRL 
                    #that was used by the subject and not the data from the foveal fixation(so removes the overall offset)
                    elif prl_position=='left':
                        offset_x=(scotoma_radius//2+text_width*2)
                        offset_y=0
                    elif prl_position=='right':
                        offset_x=-(scotoma_radius//2+text_width*2)
                        offset_y=0
                    elif prl_position=='inferior':
                        offset_x=0
                        offset_y=(scotoma_radius//2)+(text_height//2)#the 150/2 + 34/2 corresponds to apparent position)of the PRL that the subject is using to perform the 
                        #task and this is calculated using foveal position 
                    
                    for i in range(1,np.shape(data_array)[0]):
                       plt.scatter(float(data_array[i,2])+offset_x,(float(data_array[i,3])+offset_y),color='g',s=float(data_array[i,1])//10)
                       plt.text(float(data_array[i,2])+offset_x,(float(data_array[i,3])+offset_y),str(i),color='b')
                    plt.xlabel('Horizontal Eye Position')
                    plt.ylabel('Vertical Eye Position')
                    plt.ylim(max(plt.ylim()), min(plt.ylim()))
                    plt.imshow(textimg)
                    #The following two lines ensure that the X and Y axes limits corresponds to the limits of the display that was used
                    plt.ylim(768,0)
                    plt.xlim(0,1024)
                    plt.show()
                elif "events_saccades" in filename:
                    #This reads the saccade file and creates a pandas data frame whcih is then cast into a numpy array
                    data_file=pd.read_csv(os.path.join(dirName,filename),delimiter='\t',names=["Time Start","Time End","Saccade Duration", "Pos x(start)", "Pos y(start)", "Pos x(end)", "Pos y(end)","hypot","Peak velocity"])
                    data_array=np.array(data_file)
                    plt.figure(figsize=(15,8))
                    if prl_position=='foveal':
                        offset_x=0
                        offset_y=0
                    #In cases where there is peripheral data this part helps to skew the data points such that it is more relation to the PRL 
                    #that was used by the subject and not the data from the foveal fixation(so removes the overall offset)
                    elif prl_position=='left':
                        offset_x=(scotoma_radius//2+text_width*2)
                        offset_y=0
                    elif prl_position=='right':
                        offset_x=-(scotoma_radius//2+text_width*2)
                        offset_y=0
                    elif prl_position=='inferior':
                        offset_x=0
                        offset_y=(scotoma_radius//2)+(text_height//2)#the 150/2 + 34/2 corresponds to apparent position)of the PRL that the subject is using to perform the 
                        #task and this is calculated using foveal position 
                        #In cases where there is peripheral data this part helps to skew the data points such that it is more relation to the PRL 
                        #that was used by the subject and not the data from the foveal fixation(so removes the overall offset)
                    
                    for i in range(1,np.shape(data_array)[0]):
                        
                    #first we identify the saccades that occur in the backward direction
                        saccade_amplitude_h=(float(data_array[i,5])-float(data_array[i,3]))
                    #now we plot only the backward saccades
                        if saccade_amplitude_h<0:
                            plt.plot((float(data_array[i,3])+offset_x,float(data_array[i,5])+offset_x),(float(data_array[i,4])+offset_y,float(data_array[i,6])\
                                      +offset_y),marker='o',color='g')
                            plt.text((float(data_array[i,3])+offset_x),float(data_array[i,4])+offset_y,str(i),color='b')
                    plt.xlabel('Horizontal Eye Position')
                    plt.ylabel('Vertical Eye Position')
                    plt.ylim(max(plt.ylim()), min(plt.ylim()))
                    plt.imshow(textimg)
                     #The following two lines ensure that the X and Y axes limits corresponds to the limits of the display that was used
                    plt.ylim(768,0)
                    plt.xlim(0,1024)
                    plt.show()

except:
    print('file does not exist')
    

