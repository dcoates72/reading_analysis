# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 12:14:25 2019

@author: krish
"""
import tkinter as tk
from tkinter import filedialog
import pandas as pd
root = tk.Tk()
root.withdraw()


open_file=filedialog.askopenfilename(title='Please select word file')
data_file=filedialog.askopenfilename(title='Please select Saccade file')
word_file=pd.read_csv(open_file,delimiter='\t',names=["Word", "Pos x", "Pos y"])
sweep_start=[]
sweep_end=[]
for i in range(len(word_file)-1):
    if word_file.iloc[i,2]-word_file.iloc[i+1,2]>50:
        sweep_start.append(word_file.iloc[i,2])
        sweep_end.append(word_file.iloc[i+1,2])
        


saccade_data=pd.read_csv(data_file,delimiter='\t',names=["Time Start","Time End","Saccade Duration", "Pos x(start)", "Pos y(start)", "Pos x(end)", "Pos y(end)","hypot","Peak velocity"])