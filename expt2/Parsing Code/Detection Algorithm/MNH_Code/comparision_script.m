figure(1);
for i=1:length(saccade_event)
    plot([saccade_event(i,4),saccade_event(i,6)]...
            ,[saccade_event(i,5),saccade_event(i,7)],'-o','Color','red');  
    hold on
end        
xlim([0 1024]); ylim([0 768]);
h=gca;
set(h,'YDir','reverse');

[filename,folderpath]=uigetfile('*.txt', 'Please select the data file');
saccade_data=importdata([folderpath,filename],'\t');
data_array=saccade_data.data;
column_headers=saccade_data.colheaders;
figure(2);
for i=1:length(saccade_event)
    plot([data_array(i,4),data_array(i,6)]...
            ,[data_array(i,5),data_array(i,7)],'-o','Color','green');  
    hold on
end     
hold off
xlim([0 1024]); ylim([0 768]);
h=gca;
set(h,'YDir','reverse');