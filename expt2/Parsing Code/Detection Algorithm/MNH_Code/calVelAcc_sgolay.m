function calVelAcc_sgolay(signals) %i=Subject,j=Session
global ET
global Verbose_TF

if Verbose_TF,fprintf('\n%s\n','***************** in calVelAcc_sgolay'),end

%
% This function smoothes the data, and computes the smoothed
% velocity and acceleration signals
% It also computes radial velocity and radial acceleration
%
% Load unfiltered data into ET structure
%--------------------------------------------------------------------------

ET.Vectors.Msec=signals(:,1);
ET.Vectors.Xorg=signals(:,2);
ET.Vectors.Yorg=signals(:,3);
ET.Vectors.Pupil=signals(:,4);
clear signals

% Set up Savitzky-Golar Filter Setting
%--------------------------------------------------------------------------
N = 2;                 % Order of polynomial fit

% Find Window Length (min = 5)

F=ceil(ET.Scalars.Params.SmoothingWindowLengthInSeconds*ET.Scalars.Params.samplingFreq);
if mod(F,2)~=1;
    for i = F:100
        if mod(i,2)==1
            F=i;
            break;
        end
    end;
end
if (F < 5);F=5;end;
if Verbose_TF, fprintf('\nFilter window in samples = F = %5.2f\n',F),end
[b,g] = sgolay(N,F);   % Calculate S-G coefficients

% Extract relevant smoothed gaze coordinates for the current trial.
% Note: Filter delay is not removed.

ET.Vectors.Xsmo = filter(g(:,1),1,ET.Vectors.Xorg);
ET.Vectors.Ysmo = filter(g(:,1),1,ET.Vectors.Yorg);

% Calculate the velocity and acceleration

ET.Vectors.velX = filter(g(:,2),1,ET.Vectors.Xorg)*[ET.Scalars.Params.samplingFreq];
ET.Vectors.velX = -1.*ET.Vectors.velX; %Flip velocity to be correct
ET.Vectors.velY = filter(g(:,2),1,ET.Vectors.Yorg)*[ET.Scalars.Params.samplingFreq];
ET.Vectors.velY = -1.*ET.Vectors.velY; %Flip velocity to be correct
ET.Vectors.vel  = sqrt(ET.Vectors.velX.^2 + ET.Vectors.velY.^2); % Radial Velocity
ET.Vectors.accX = filter(g(:,3),1,ET.Vectors.Xorg)*[ET.Scalars.Params.samplingFreq]^2;
ET.Vectors.accY = filter(g(:,3),1,ET.Vectors.Yorg)*[ET.Scalars.Params.samplingFreq]^2;
ET.Vectors.acc = sqrt(ET.Vectors.accX.^2 + ET.Vectors.accY.^2); % Radial Acceleration

FindAllTheNaNsInTheDataNow=find(isnan(ET.Vectors.Xsmo));
ET.Vectors.NaNIndex(FindAllTheNaNsInTheDataNow)=true;
if Verbose_TF, fprintf('Number NaNs from EyeLink and SG filter = %d\n',sum(ET.Vectors.NaNIndex)),end

return
