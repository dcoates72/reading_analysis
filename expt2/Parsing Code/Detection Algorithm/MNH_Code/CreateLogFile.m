function [logname]=CreateLogFile(message,PathToLogFile)

global Verbose_TF

if Verbose_TF,fprintf('\n%s\n','***************** in CreateLogFile'),end

dt = datestr(now,'mmmm dd, yyyy HH:MM:SS.FFF AM');
dt=strrep(dt,' ','-');
dt=strrep(dt,':','-');
dt=strrep(dt,',','-');
dt=strrep(dt,'--','-');
logname = sprintf('%s-%s',message,dt);
logname=[PathToLogFile logname '.txt'];
diary(logname)
fprintf('Diary File Name = %s\n',logname)
return
