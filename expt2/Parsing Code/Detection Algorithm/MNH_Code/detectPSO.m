function [IsPSO,Reject,PSOStartIndex,PSOEndIndex]=detectPSOs(FinalSaccadeStartIndex,SaccadeLocalMinEndIndex,N_GoodSaccades)

%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************

global ET
global Verbose_TF

if Verbose_TF,fprintf('\n%s\n','***************** in detectPSO'),end

%----------------------------------------------------------------------  
% DETECT PSO
%----------------------------------------------------------------------   

V = [ET.Vectors.vel];
A = [ET.Vectors.acc];

%
% Initialize 
%
IsPSO=false;
Reject=false;
IsLargePSO=false;
IsModeratePSO=false;
IsSmallPSO=false;
%
% If PSO exists, PSO must start immediately after the Saccade Local Minimum after the Peak Velocity
%
PSOStartIndex=SaccadeLocalMinEndIndex+1;
%
% Check for the end of this potential PSO
%
PSOEndIndex=[];
for m = PSOStartIndex+1:ET.Scalars.Data.Length-6
    %
    % Potential PSO ends when 5 consecutive points have a velocity less than the 90th Percentile of the 
    % distribution of noise during fixation.
    %
    if nanmax(V(m:m+5)) < ET.Scalars.Data.NoisePercentile90;
        PSOEndIndex=m;
        break
    end
end
if isempty(PSOEndIndex)
    IsPSO=false;Reject=true;PSOStartIndex=0;PSOEndIndex=0;
    PSO_Decision{N_GoodSaccades}='Reject_PSO_Ends_Too_Close_to_End_of_Data';
    if Verbose_TF;
        fprintf('\tFor PSO after Potential Good Saccade Number %d, Final Descision = %s\n',N_GoodSaccades,PSO_Decision{N_GoodSaccades});
    end
    return
end

if PSOEndIndex <= PSOStartIndex;
    IsPSO=false;Reject=true;PSOStartIndex=0;PSOEndIndex=0;
    PSO_Decision{N_GoodSaccades}='Reject_Malformed PSO start>end?';
    if Verbose_TF;
        fprintf('\tFor PSO after Potential Good Saccade Number %d, Final Descision = %s\n',N_GoodSaccades,PSO_Decision{N_GoodSaccades});
        fprintf('\tPSO Start Index = %d, PSO End Index = %d\n',PSOStartIndex,PSOEndIndex);
    end
    return
end
%
% Check if previous event is a saccade.
%
if ET.Vectors.SaccadeIndex(PSOStartIndex-1)==false;
    %
    % Previous event is not a saccade.
    %
    IsPSO=false;Reject=true;PSOStartIndex=0;PSOEndIndex=0;
    PSO_Decision{N_GoodSaccades}='Reject_Previous_Event_Is_Not_A_Saccade';
    if Verbose_TF;fprintf('\tFor PSO after Potential Good Saccade Number %d, Final Descision = %s\n',N_GoodSaccades,PSO_Decision{N_GoodSaccades});end
    return
end
%
% Make sure that there are no NaNs in the PSO.
%
if sum(ET.Vectors.NaNIndex(PSOStartIndex:PSOEndIndex)) > 0
    IsPSO=false;Reject=true;PSOStartIndex=0;PSOEndIndex=0;
    PSO_Decision{N_GoodSaccades}='Reject_Potential_PSO_Has_NaNs';
    if Verbose_TF;fprintf('\tFor PSO after Potential Good Saccade Number %d, Final Descision = %s\n',N_GoodSaccades,PSO_Decision{N_GoodSaccades});end
    return
end
%
% Fine the peak velocity and peak acceleration in this PSO
%
PSOVelocity = V(PSOStartIndex:min(PSOEndIndex,ET.Scalars.Data.Length));
PeakPSOVelocity = max(PSOVelocity);
if length(PeakPSOVelocity) > 1,PeakPSOVelocity(2:end)=[];end;
PSOAcceleration = V(PSOStartIndex:min(PSOEndIndex,ET.Scalars.Data.Length));
PeakPSOAcceleration = max(PSOAcceleration);
if length(PeakPSOAcceleration) > 1,PeakPSOAcceleration(2:end)=[];end;
if isempty(PeakPSOVelocity)
    IsPSO=false;Reject=true;PSOStartIndex=0;PSOEndIndex=0;
    PSO_Decision{N_GoodSaccades}='Reject_Could_Not_Find_Peak_Velocity';
    if Verbose_TF;fprintf('\tFor PSO after Potential Good Saccade Number %d, Final Descision = %s\n',N_GoodSaccades,PSO_Decision{N_GoodSaccades});end
    return
end
%
% Check if peak velocity in this potential PSO is greater than the peak velocity of the prior saccade
%
if PeakPSOVelocity > max(V(FinalSaccadeStartIndex:SaccadeLocalMinEndIndex));
    IsPSO=false;Reject=true;PSOStartIndex=0;PSOEndIndex=0;
    PSO_Decision{N_GoodSaccades}='Reject_Peak_Velocity_during_this_potential_PSO_exceeds_the_Velocity_of_the_Prior_Saccade.';
    if Verbose_TF;fprintf('\tFor PSO after Potential Good Saccade Number %d, Final Descision = %s\n',N_GoodSaccades,PSO_Decision{N_GoodSaccades});end
    return
end;
%
%  Is this a Large PSO
%
if PeakPSOVelocity > ET.Scalars.Params.PeakVelocityThreshold;
    IsPSO=true;Reject=false;IsLargePSO=true;IsModeratePSO=false;IsSmallPSO=false;
    PSO_Decision{N_GoodSaccades}='Accept_A_Large_PSO_is_Found';
    if Verbose_TF;fprintf('\tFor PSO after Potential Good Saccade Number %d, Final Descision = %s\n',N_GoodSaccades,PSO_Decision{N_GoodSaccades});end
%
% Is this a Moderately size PSO
%
elseif PeakPSOVelocity <= ET.Scalars.Params.PeakVelocityThreshold... 
    && PeakPSOVelocity > ET.Scalars.Params.SaccadeSubthreshold ;
    IsPSO=true;Reject=false;IsLargePSO=false;IsModeratePSO=true;IsSmallPSO=false;
    PSO_Decision{N_GoodSaccades}='Accept_A_Moderate_PSO_is_Found';
    if Verbose_TF;fprintf('\tFor PSO after Potential Good Saccade Number %d, Final Descision = %s\n',N_GoodSaccades,PSO_Decision{N_GoodSaccades});end
%
% Is this a small PSO?
%
elseif PeakPSOVelocity <= ET.Scalars.Params.SaccadeSubthreshold...
     && PeakPSOVelocity > ET.Scalars.Params.SmallPSOVelocityThreshold ;
    IsPSO=true;Reject=false;IsLargePSO=false;IsModeratePSO=false;IsSmallPSO=true;
    PSO_Decision{N_GoodSaccades}='Accept_A_Small_PSO_is_Found';
    if Verbose_TF;fprintf('\tFor PSO after Potential Good Saccade Number %d, Final Descision = %s\n',N_GoodSaccades,PSO_Decision{N_GoodSaccades});end
end;
%
% Deos PSO meet minimum velocity reqruirements?
%
if PeakPSOVelocity <= ET.Scalars.Params.SmallPSOVelocityThreshold
    IsPSO=false;Reject=true;IsLargePSO=false;IsModeratePSO=false;IsSmallPSO=false;
    PSOStartIndex=0;PSOEndIndex=0;
    PSO_Decision{N_GoodSaccades}='Reject_Peak_PSO_Velocity_Does_Not_Meet_Minimum_Velocity_Threshold';
    if Verbose_TF;fprintf('\tFor PSO after Potential Good Saccade Number %d, Final Descision = %s\n',N_GoodSaccades,PSO_Decision{N_GoodSaccades});end
    return;
end
%
% Is PSO at least 3 msec??
%
if PSOEndIndex - PSOStartIndex + 1 < 3
    IsPSO=false;Reject=true;IsLargePSO=false;IsModeratePSO=false;IsSmallPSO=false;
    PSOStartIndex=0;PSOEndIndex=0;
    PSO_Decision{N_GoodSaccades}='Reject_PSO < 3 msec';
    if Verbose_TF;fprintf('\tFor PSO after Potential Good Saccade Number %d, Final Descision = %s\n',N_GoodSaccades,PSO_Decision{N_GoodSaccades});end
    return;
end
%
% Update PSO Vector
%
ET.Vectors.PSOIndex(PSOStartIndex:PSOEndIndex)=true;
%
% Load PSOInfo 
%
if IsLargePSO   ,ET.Scalars.Data.PSOInfo(N_GoodSaccades).PSO_Type = 'L';end;
if IsModeratePSO,ET.Scalars.Data.PSOInfo(N_GoodSaccades).PSO_Type = 'M';end;
if IsSmallPSO   ,ET.Scalars.Data.PSOInfo(N_GoodSaccades).PSO_Type = 'S';end;
ET.Scalars.Data.PSOInfo(N_GoodSaccades).start = PSOStartIndex/[ET.Scalars.Params.samplingFreq]; % in seconds  
ET.Scalars.Data.PSOInfo(N_GoodSaccades).end   = PSOEndIndex  /[ET.Scalars.Params.samplingFreq]; % in seconds
ET.Scalars.Data.PSOInfo(N_GoodSaccades).peakVelocity = PeakPSOVelocity; 
ET.Scalars.Data.PSOInfo(N_GoodSaccades).peakAcceleration = PeakPSOAcceleration; 
ET.Scalars.Data.PSOInfo(N_GoodSaccades).duration = (PSOEndIndex-PSOStartIndex+1)/[ET.Scalars.Params.samplingFreq];
if IsLargePSO   ,ET.Scalars.Data.PSOInfo(N_GoodSaccades).PSO_Type = 'L';end;
if IsModeratePSO,ET.Scalars.Data.PSOInfo(N_GoodSaccades).PSO_Type = 'M';end;
if IsSmallPSO   ,ET.Scalars.Data.PSOInfo(N_GoodSaccades).PSO_Type = 'S';end;

return
