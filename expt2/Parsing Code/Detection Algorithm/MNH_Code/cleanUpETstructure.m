function cleanUpETstructure()

%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************


global ET
global Verbose_TF

if Verbose_TF,fprintf('\n%s\n','***************** in cleanUpETstructure'),end

ET.Scalars.Data.Subject=[];
ET.Scalars.Data.Session = [];
ET.Scalars.Data.OriginalLength = []; 
ET.Scalars.Data.Length = []; 
ET.Scalars.Data.NoisePercentile90 = []; 
ET.Scalars.Data.NumNaNBlocks = [];
ET.Scalars.Data.NoiseTrial = [];
ET.Scalars.Data.PSOInfo = [];
ET.Scalars.Data.SaccadeInfo = [];
ET.Scalars.Data.N_Saccades = [];
ET.Scalars.Data.N_PSOs = [];
ET.Scalars.Data.FixationInfo = [];
ET.Scalars.Data.N_Fixations = [];
ET.Scalars.Data.Percent_NaNs_with_All_Filters = [];
ET.Scalars.Data.N_Sample_Fixations = [];
ET.Scalars.Data.N_Sample_Saccades = [];
ET.Scalars.Data.N_Sample_PSO = [];
ET.Scalars.Data.N_Sample_Noise = [];
ET.Scalars.Data.N_Sample_UnClassified =[];

ET.Vectors.Msec = []; 
ET.Vectors.Xorg = []; 
ET.Vectors.Yorg = []; 
ET.Vectors.Xsmo = [];
ET.Vectors.Ysmo = [];
ET.Vectors.velX = [];
ET.Vectors.velY = []; 
ET.Vectors.vel = [];
ET.Vectors.accX = []; 
ET.Vectors.accY = [];
ET.Vectors.acc = [];
ET.Vectors.Classification = [];
ET.Vectors.NaNIndex = [];
ET.Vectors.FixationIndex = []; 
ET.Vectors.SaccadeIndex = [];
ET.Vectors.PSOIndex = [];
ET.Vectors.Pupil = []; 
ET.Vectors.Is_Vel_GT_SaccadePkVelThresh = [];

return


