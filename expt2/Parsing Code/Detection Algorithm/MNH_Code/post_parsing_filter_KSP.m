%% Post Analysis Sorting
function [saccade_event,fixation_event]=post_parsing_filter_KSP(ET,data_file_original)
%% Extracting Saccade details
saccade_counter_start=1;
saccade_counter_end=1;
%We merge the data from the vector information of the saccades and the PSO 
% saccade_idx_with_pso=or(ET.Vectors.SaccadeIndex,ET.Vectors.PSOIndex);
saccade_idx_no_pso=or(ET.Vectors.SaccadeIndex,ET.Vectors.PSOIndex);
for i=1:(length(saccade_idx_no_pso)-1)
    if saccade_idx_no_pso(i)==1 && saccade_idx_no_pso(i-1)==0 ...
        && saccade_idx_no_pso(i+1)==1
        saccade_start_idx(saccade_counter_start)=i;
        saccade_counter_start=saccade_counter_start+1;
    elseif saccade_idx_no_pso(i)==1 && saccade_idx_no_pso(i-1)==1 ...
        && saccade_idx_no_pso(i+1)==0
            saccade_end_idx(saccade_counter_end)=i;
            saccade_counter_end=saccade_counter_end+1;
    elseif i==(length(saccade_idx_no_pso)-1) && saccade_idx_no_pso(i)==1 %To catch events that occur at the end of the trial
        saccade_end_idx(saccade_counter_end)=i;
        saccade_counter_end=saccade_counter_end+1;
    end
end

%now we try to align the data to match the eyelink saccade event data
%structure
for j=1:length(saccade_start_idx)
    start_point=saccade_start_idx(j);
    end_point=saccade_end_idx(j);
    saccade_event(j,1)=data_file_original.data(start_point,1);%saccade start time stamp
    saccade_event(j,2)=data_file_original.data(end_point,1);% saccade end time stamp
    saccade_event(j,3)=saccade_event(j,2)-saccade_event(j,1); %saccade duration
    saccade_event(j,4:5)=data_file_original.data(start_point,2:3);%start position
    saccade_event(j,6:7)=data_file_original.data(end_point,2:3);%end position
end

%% Extracting Fixation Details
fixation_counter_start=1;
fixation_counter_end=1;
for i=1:(length(ET.Vectors.FixationIndex)-1)
    if i==1 %KSP (08/20/2019): added this to prevent crashes during first iteration of loop
        continue
    elseif ET.Vectors.FixationIndex(i)==1 && ET.Vectors.FixationIndex(i-1)==0 ...
        && ET.Vectors.FixationIndex(i+1)==1
        fixation_start_idx(fixation_counter_start)=i;
        fixation_counter_start=fixation_counter_start+1;
    elseif ET.Vectors.FixationIndex(i)==1 && ET.Vectors.FixationIndex(i-1)==1 ...
        && ET.Vectors.FixationIndex(i+1)==0
        fixation_end_idx(fixation_counter_end)=i;
        fixation_counter_end=fixation_counter_end+1;
    elseif i==(length(ET.Vectors.FixationIndex)-1) && ET.Vectors.FixationIndex(i)==1 %To catch events that occur at the end of the trial
        fixation_end_idx(fixation_counter_end)=i;
        fixation_counter_end=fixation_counter_end+1;
    end
end

%now we try to align the data to match the eyelink saccade event data
%structure
for j=1:length(fixation_start_idx)
    start_point=fixation_start_idx(j);
    end_point=fixation_end_idx(j);
    fixation_event(j,1)=data_file_original.data(start_point,1);%saccade start time stamp
    fixation_event(j,2)=data_file_original.data(end_point,1);% saccade end time stamp
    fixation_event(j,3)=fixation_event(j,2)-fixation_event(j,1); %saccade duration
    fixation_event(j,4:5)=mean(data_file_original.data(start_point,2:3));%average gaze position
end
end