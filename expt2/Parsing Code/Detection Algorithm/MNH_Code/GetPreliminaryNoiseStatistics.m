function GetPreliminaryNoiseStatistics()

%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************

global ET
global PathToOutputDirectory
global RawDataFileName
global Verbose_TF

if Verbose_TF,fprintf('\n%s\n','***************** in GetPreliminaryNoiseStatistics '),end

%
% We temporarily define fixation points for the purpose of evaluating velocity noise during
% fixation.  True fixations are defined by the detectFixation function
% 
% At preent the maximum velcoity for these possible fixations is 100.  Should try dropping
% this to 55 (the peak saccade velocity threshold.
possibleFixationIdx = ET.Vectors.vel < ET.Scalars.Params.ExcludeAboveThisVelForNoiseAnalysis;
FixBlocks = bwlabel(possibleFixationIdx);

% Process one possible fixation period at a time. 

fixNoise = []; % This will be a vector of velocities during possible fixations
N_Potential_Fixations=max(FixBlocks);
for k = 1:N_Potential_Fixations

    % The samples related to the current possible fixation
    fixIdx = find(FixBlocks == k);
    
    % Check that the possible fixation duration exceeds the minimum duration criteria. 
    if length(fixIdx)/[ET.Scalars.Params.samplingFreq] < ET.Scalars.Params.minFixDur;
        continue    
    end
    
    % Extract the samples from the center of the fixation
    centralFixSamples = [ET.Scalars.Params.minFixDur]*[ET.Scalars.Params.samplingFreq]/6;
    % velcoity noise in this possible fixation
    fNoise = ET.Vectors.vel(floor(fixIdx(1)+centralFixSamples):ceil(fixIdx(end)-centralFixSamples));
    % accumulate possible fixation noise 
    fixNoise = [fixNoise;fNoise];
end
%
% Determine the 90th percentile of the velocities of possible fixations.
%
ET.Scalars.Data.NoisePercentile90=prctile(fixNoise,90);
%
% Create a useful histogram of velocity noise during possible fixation
% and show some useful thresholds.  Save graph to file, as in:
% S_1051_S2_TEX_Noise.jpg
%
if Verbose_TF;
    figure(1)
    Height=350;Width=500;
    myposition=[10 50 Width Height];
    set(gcf,'units','pixels','position',myposition);
    %   Create Histogram
    h=histogram(fixNoise,40);
    MaxFreq=max(h.Values);
    ylabel('Frequency (counts)')
    xlim([0 ET.Scalars.Params.PeakVelocityThreshold*1.1])
    xlabel('Velocity Noise In Fixation');
    set(h,'FaceColor',[0.8 0.8 0.8]);
    hold on;
    % Indicate the 90 percentile of the noise distribution
    x=[ET.Scalars.Data.NoisePercentile90 ET.Scalars.Data.NoisePercentile90];
    y=[0 ,MaxFreq*1.2];
    plot(x,y,'-m','linewidth',4)
    text(ET.Scalars.Data.NoisePercentile90*0.87,MaxFreq*0.2,['90th Percentile = ' num2str(ET.Scalars.Data.NoisePercentile90,'%5.2f')],'rotation',90);
    % Indicate the Small Glissade Velocity Threshold
    x=[ET.Scalars.Params.SmallPSOVelocityThreshold ET.Scalars.Params.SmallPSOVelocityThreshold];
    plot(x,y,'-c','linewidth',4)
    text(ET.Scalars.Params.SmallPSOVelocityThreshold*0.93,MaxFreq*0.2,['Small Glissade Vel Thresh = '  num2str(ET.Scalars.Params.SmallPSOVelocityThreshold,'%5.2f') ],'rotation',90)
    % Indicate the SacOnOffThreshold
    x=[ET.Scalars.Params.SaccadeSubthreshold ET.Scalars.Params.SaccadeSubthreshold];
    plot(x,y,'-b','linewidth',4)
    text(ET.Scalars.Params.SaccadeSubthreshold*0.96,MaxFreq*0.2,['Sac On/Off Thresh = ' num2str(ET.Scalars.Params.SaccadeSubthreshold,'%5.2f')],'rotation',90)
    % Indicate the Peak Velocity Threshold
    x=[ET.Scalars.Params.PeakVelocityThreshold ET.Scalars.Params.PeakVelocityThreshold];
    plot(x,y,'-r','linewidth',4)
    text(ET.Scalars.Params.PeakVelocityThreshold*0.97,MaxFreq*0.2,['Sac Peak Thresh = ' num2str(ET.Scalars.Params.PeakVelocityThreshold,'%5.2f')],'rotation',90)
    % Indicate the Noise Velocity Threshold
    x = [ET.Scalars.Params.PeakVelocityThreshold*1.07 ET.Scalars.Params.PeakVelocityThreshold*1.07];
    plot(x,y,':b','linewidth',4)
    text(x(1)*0.97,MaxFreq*0.2,['Noise Vel. Threshold = ' num2str(ET.Scalars.Params.ExcludeAboveThisVelForNoiseAnalysis,'%5.2f')],'rotation',90)
    title([' Velocity Noise in Fixation' ' | Subject = ',num2str(i),' | Session = ',num2str(j)])
    ImgFileName=[PathToOutputDirectory RawDataFileName(1:13) '_Noise.jpg'];
    saveas(gcf,ImgFileName);
    close all;
end;


