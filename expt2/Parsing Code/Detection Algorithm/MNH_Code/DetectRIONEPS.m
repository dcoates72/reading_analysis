function [RIONEPS,IM,DATCF,TDT]=DetectRIONEPS(PV,SR,OT)

%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************

global ET
global Verbose_TF

if Verbose_TF,fprintf('\n%s\n','***************** in DetectRIONEPS'),end

%Inputs:
%   PV = Position Vector (Horizontal or Vertical)
%   SR = Sample Rate
%   OT = Inefficiency Threshold
%
%Outputs:
%   RIONEPS = Logivsl vector,same length as eye_position_vector 
%       0 = No Noise
%       1 = RIONEPS

RIONEPS=false(length(PV),1);

WS   =floor(SR/20);           % WS = Window Size
TDT  =zeros(length(PV)-WS,1); % Total Distance Traveled
DATCF=zeros(length(PV)-WS,1); % Distance as the Crow Files
IM   =zeros(length(PV)-WS,1); % Inefficiecy Metric

FirstWindowPositionStart = 1;
LastWindowPositionStart = length(PV)-WS;
for i = FirstWindowPositionStart:LastWindowPositionStart
    %
    % Find first and last good data points
    %
    NaNsInWindow=isnan(PV(i:i+WS-1));
    NstartSkip=0;
    NaNIndex=0;
    for j = i:i+(WS/2)
        NaNIndex=NaNIndex+1;
        if NaNsInWindow(NaNIndex) == 1
            NstartSkip=NstartSkip+1;
        else
            break
        end
    end
    NendSkip=0;
    NaNIndex=WS+1;
    for j = i+WS-1:-1:i+WS/2
        NaNIndex=NaNIndex-1;
        if NaNsInWindow(NaNIndex) == 1
            NendSkip=NendSkip+1;
        else
            break
        end
    end
    %
    % Compute total distance travelled (TDT)
    %
    NaNIndex=0;
    CountNaNsBetweenWindowStartAndWindowEnd=0;
    for j=i+1+NstartSkip:i+WS-1-NendSkip
        NaNIndex=NaNIndex+1;
        if isnan(PV(j)) || isnan(PV(j-1))
            CountNaNsBetweenWindowStartAndWindowEnd=CountNaNsBetweenWindowStartAndWindowEnd+1;
            continue
        else
            TDT(i)=TDT(i)+abs(PV(j)-PV(j-1));
        end
    end
    %
    % Compute distance as the crow flies (DATCF)
    %
    DATCF(i)=abs(PV(i+WS-NendSkip-1)-PV(i+NstartSkip));
    %
    % Compute Inefficiency Metric (IM)
    %
    WindowSizeNoNans=WS-sum(NaNsInWindow+NstartSkip+NendSkip);
    IM(i)=(TDT(i)-DATCF(i))*1000/WindowSizeNoNans; %Inefficiency Metric (IM)
    if IM(i) > OT
        RIONEPS(i:i+WS-1)=1;
    end
end
return
