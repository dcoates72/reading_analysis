function beginEventDetection()
%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************

% This is the first program to run for classification according to the 
% MNH algorithm.  The order of functions called is:
%
%**********************************************************************
% Orchestrate the entire classification algorithm:
%**********************************************************************
%       eventdetection
%**********************************************************************
% Initialize ET data structure
%**********************************************************************
%       cleanUpETstructure\n
%**********************************************************************
% Read in the data file, set up vectors:
%**********************************************************************
%       loaddata
%**********************************************************************
% Calculate smoothed velocity and acceleration:
%**********************************************************************
%       calVelAcc_sgolay(signals)
%**********************************************************************
% Characterize Fixation Noise prior to Classification:
%**********************************************************************
%       GetPreliminaryNoiseStatistics();
%**********************************************************************
% Detect blinks and noise/artifact:
%**********************************************************************
%       detectAndRemoveNoise();DetectRIONEPS\n
%**********************************************************************
% Detect saccades and PSOs:
%**********************************************************************
%       detectSaccades();detectPSO
%**********************************************************************
%**********************************************************************
% Detect Fixations:
%**********************************************************************
%       detectFixations();
%**********************************************************************
% Do some final checking and prepare and write output files
%**********************************************************************
%       finalCleanUpAndCreateOutput()
%**********************************************************************
% 
% After running this program, run Plot_MNH_Results to prepare .fig files
% and .pdf files for each secon of data.   Run ViewData to view the .fig
% files
%

global ET;
global PathToLogFile
global Verbose_TF;Verbose_TF=true;

if Verbose_TF,fprintf('\n%s\n','***************** in beginEventDetection\n'),end


close all;
clc;
commandwindow()

ET=[];

%--------------------------------------------------------------------------
% Create Log File
%--------------------------------------------------------------------------
PathToLogFile='C:\Dropbox\NYSTROM_MATLAB_CODE\Nystom_Modified_Method\CleanCodeForPublication\LogFiles\';
if ~exist(PathToLogFile,'dir'), mkdir(PathToLogFile),end;
[LogFileName] = CreateLogFile('LogFile',PathToLogFile);
%--------------------------------------------------------------------------
% Init parameters
%--------------------------------------------------------------------------
ET.Scalars.Params.samplingFreq = 1000;                       % sampling rate
ET.Scalars.Params.NaNVelocityThreshold = 1000;               % Changed from paper to 1000 from 1500. if vel > 1000 degrees/s, it is noise/artifact.
ET.Scalars.Params.NaNAccThreshold = 100000;                  % if acc > 100000 degrees/s^2, it is noise/artifact.
ET.Scalars.Params.ExcludeAboveThisVelForNoiseAnalysis = 100; 

ET.Scalars.Params.SmoothingWindowLengthInSeconds =0.020; % Original Parameter@KSP :0.007;
ET.Scalars.Params.PeakVelocityThreshold=55;
ET.Scalars.Params.SaccadeSubthreshold=45;
ET.Scalars.Params.SmallPSOVelocityThreshold=20;

ET.Scalars.Params.minFixDur =0.080; %original parameter @KSP: 0.030; % in seconds
ET.Scalars.Params.minSaccadeDur = 0.010; % in seconds

ET.Scalars.Params.MaxPosDiffWithinAFixation=2.0;

ET.Scalars.Params.RIONEPS_Threshold = 100.;

%--------------------------------------------------------------------------
% Begin detection
%--------------------------------------------------------------------------

% Process data
eventDetection
fprintf('Log File Name = \n%s\n',strcat(LogFileName));
close all
fclose('all');
end