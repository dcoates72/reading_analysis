%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************
This is the Matlab (Natick, MA) software for the 'MNH' eye 
movement classification method described in the soon to be
published (as of 10/9/2017) paper in Behavior Research 
Methods, entitled “A Novel Evaluation of Two Related
Algorithms for Eye Movement Classification during Reading”, 
with authors Lee Friedman, Ioannis Rigas, Evgeny Abdulin and
Oleg V. Komogortsev, The Department of Computer Science,
Texas State University

The software has been designed and tested using Eyelink-1000 data
with a sampling rate of 1000 Hz.  Use of this software on any 
other data source has not been tested and we have no expectations
of the success of this software in such a case.

The software was designed and tested for a text reading task,
and although it should work on other data without smooth pursuit,
we do not have extensive experience using this software with 
other tasks.

Our naming convention for data files is S_XXXX_SY_TXT.csv, where
XXXX is the subject number and Y is the session number.  So,
S_1054_S2_TXT.csv is data from subject 1054, session 2, text
reading task.

During a reading task, some subjects finished before the recording
period ends.  Therefore, a file with a single number, the sample
number when reading ends, is required for the software to run.
The name of the end or reading file is something like this:

S_1054_S2_TEX_EndOfReading.txt

and it typically contains a single number such as 36229.  The 
total task runs for 60000 msec, so this number tells the 
software to only score up to the 36229, when the subject finished
reading.  There are a number of methods to determine when the
subject finishes reading.  When viewing vertical eye movements
during the entire 60 second recording, it is typically obvious
approximately when the subject finished reading.

The contact person for this software is Lee Friedman, PH.D.,
at lfriedman10@gmail.com

Most of the code is for the classfication system, but two helper
programs are also provided, After running the classification code,
run 'Plot_MNH_Results' to prepare .fig files  and .pdf files for 
each second of data.  Run ViewData to view the .fig files.

Note that when viewing the data with ViewData, clicking the left
mouse button near an event will display a zoomed-in section of
the recording where one can see individual samples.

For eye movement classification, the first program to run is 
beginEventDetection.  After that, the following functions are 
called:

**********************************************************************
 Orchestrate the entire classification algorithm:
**********************************************************************
       eventdetection
**********************************************************************
 Initialize ET data structure
**********************************************************************
       cleanUpETstructure
**********************************************************************
 Read in the data file, set up vectors:
**********************************************************************
       loaddata
**********************************************************************
 Calculate smoothed velocity and acceleration:
**********************************************************************
       calVelAcc_sgolay(signals)
**********************************************************************
 Characterize Fixation Noise prior to Classification:
**********************************************************************
       GetPreliminaryNoiseStatistics();
**********************************************************************
 Detect blinks and noise/artifact:
**********************************************************************
       detectAndRemoveNoise();DetectRIONEPS
**********************************************************************
 Detect saccades and PSOs:
**********************************************************************
       detectSaccades();detectPSO
**********************************************************************
 Detect Fixations:
**********************************************************************
       detectFixations();
**********************************************************************
 Do some final checking and prepare and write output files
**********************************************************************
       finalCleanUpAndCreateOutput()
**********************************************************************
 

All of the data collected during each run are collected in a structure
named 'ET'.  The entire ET structure is saved for each run, for example:
ET_S_1054_S2_TEX.mat.  You can load it anytime to see the other data that
are available.

ET.Vectors stores data in vectors.
ET.Scalars.Params stores the parameters set for the run.
ET.Scalars.Data contains several different types of information for
each event (fixation, saccade, PSO).
 
 The software produces a file in the output directory with a name like:
S_1054_S2_TEX_Results.txt.

Here are the contents of a sample file:

-------------------Noise Accounting------------------
Number of samples of Noise due to NaNs and SG filter   = 00286, Percent of recording =  0.79
Number of samples of Noise due to Velocity too high    = 00012, Percent of recording =  0.03
Number of samples of Noise due to Accleration too high = 00000, Percent of recording =  0.00
Number of samples of RIONEPS Noise                     = 00024, Percent of recording =  0.07
Number of samples of Noise surrounding other Noise     = 00093, Percent of recording =  0.26
Number of samples of All noise                         = 00415, Percent of recording =  1.15


-------------------Sample Counts By Class------------------
The number of samples classified as fixation     = 29333, percent =  80.97
The number of samples classified as saccade      = 04137, percent =  11.42
The number of samples classified as PSO          = 02344, percent =   6.47
The number of samples classified as noise        = 00415, percent =   1.15
The number of samples classified as unclassified = 00000, percent =   0.00
The total number of samples classified           = 36229, percent = 100.00


-------------------Event Statistics By Class------------------
The number of Fixations           = 170, Average Duration (msec) = 171.54706
The number of Saccades            = 167, Average Duration (msec) = 23.77246
The number of PSO                 = 142, Average Duration (msec) = 16.50704

This is by no means the complete amount of data that can be extracted form 
the ET data structure.  I leave this to you to write code that you need.


