% %  KSP 05/06/2019
% This scipt sets up the variables and inities the Modified saccade
% detection algorithm on the Eyelink data file. 

%% Define a starting folder.
global start_path
start_path = fullfile(matlabroot, '\toolbox');
if ~exist(start_path, 'dir')
	start_path = matlabroot;
end
% Ask user to confirm the folder, or change it.
%uiwait(msgbox('Pick a starting folder on the next window that will come up.'));
topLevelFolder = uigetdir(start_path);
if topLevelFolder == 0
	return;
end
fprintf('The top level folder is "%s".\n', topLevelFolder);

%Now we use the dirwalk function to get the files within the folder
% [listOfFileNames]=dirWalkFunction(topLevelFolder);
[listOfFileNames,fullFileName]=dirWalkFunction(topLevelFolder);
%% Looping through the Main Folder 
for file_idx=1:length(listOfFileNames)
    [filepath,filename,ext] = fileparts(fullFileName{1,file_idx});
    required_filename='sorted_sample';
    if contains(filename,required_filename) %We check to see if this is a data file
            %We change the working dir to the folder which contains the file of interest
%             cd(filepath); 
        %% Set up variables --------------------------------------------------------
            global data_file
            fullpath_data=[filepath,'\',filename,ext];
        %   Uncomment the next two lines if in case you would like to select individual files   
        %   [infile_data,infolder_data]=uigetfile('*.txt','Please select the binary data file');
        %   fullpath_data=[infolder_data,infile_data];
            % The file is then opened and is read into a variable
            raw_data_file=importdata(fullpath_data,'\t');
            %In cases where the original data collection had been done with
            %a sampling rate of 2K we would have to downsample in order to
            %be able to use the current saccade detection algorithm
            %data_file.data=downsample(raw_data_file.data,2); Noticed bugs
            %with Right to left expt 07/05/2019 [KSP]
            data_file.data=downsample(raw_data_file,2);
            % Now we create a copy of the original data set before making changes
            data_file_original=data_file;
            % recentering the data to match the center of the monitor
            data_file.data(:,2)=data_file.data(:,2)-512;
            data_file.data(:,3)=data_file.data(:,3)-384;
        %%  Cartesian Converter 
            % Pixel to degree conversion
            screen_resolution_vertical=1080;%768;
            screen_resolution_horizontal=1920;%1024;
            diagonal_screensize_cm=62.23;%50;
            distance_to_screen_cm=57;%42
            diagonal_pixel=sqrt(((screen_resolution_vertical)^2)+((screen_resolution_horizontal)^2));
            pixel_per_cm=diagonal_pixel/diagonal_screensize_cm;
            degree_per_cm=(1/distance_to_screen_cm)* 180/pi;
            degrees_per_pixel=degree_per_cm/pixel_per_cm;
            % converting the pixel values in the samples to degrees
            data_file.data(:,2)=data_file.data(:,2)*degrees_per_pixel;
            data_file.data(:,3)=data_file.data(:,3)*degrees_per_pixel;

        %% Detection & sorting of data:
            %start the event detection algorithm
            global ET
            beginEventDetection()
            %Now we create the saccade and fixation event data
            [saccade_event,fixation_event]=post_parsing_filter_KSP(ET,data_file_original);
            
        %% Saccade and Fixation Event files
            outfolder=filepath; %folder location to write output file
            outfile_saccade=sprintf('eyetrace_events__MNH_saccades_%s.txt',(filename(16:end)));
            outfile_fixation=sprintf('eyetrace_events_MNH_fixation_%s.txt',(filename(16:end)));
            outfile_fullpath_saccade=[outfolder,'\',outfile_saccade];
            outfile_fullpath_fixation=[outfolder,'\',outfile_fixation];
           [fid_saccade,~]=fopen(outfile_fullpath_saccade,'w');
           [fid_fixation,~]=fopen(outfile_fullpath_fixation,'w');
           saccade_data_title=(["Time Start","Time End","Saccade Duration",...
               "Pos x(start)","Pos y(start)","Pos x(end)","Pos y(end)",newline]);
           fixation_data_title=(["Time Start","Time End","Fixation Duration","Pos x",...
                                "Pos y",newline]);      
           %now we write the corresponding title into the output file
           fprintf(fid_saccade,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n',"Time Start",...
                            "Time End","Saccade Duration","Pos x(start)","Pos y(start)",...
                            "Pos x(end)","Pos y(end)");
           fprintf(fid_fixation,'%s\t%s\t%s\t%s\t%s\t\n',"Time Start","Time End"...
                                ,"Fixation Duration","Pos x","Pos y");
           %Now we loop through and write the saccade event data
           size_saccade_event=size(saccade_event);
           for saccade_event_idx=1:size_saccade_event(1)
               fprintf(fid_saccade,'%g\t',saccade_event(saccade_event_idx,:));
               fprintf(fid_saccade,'\n');
           end
          %now we do the same for the fixation data 
          size_fixation_event=size(fixation_event);
          for fixation_event_idx=1:size_fixation_event(1)
               fprintf(fid_fixation,'%g\t',fixation_event(fixation_event_idx,:));
               fprintf(fid_fixation,'\n');
          end
          fclose(fid_saccade);
          fclose(fid_fixation);
    end
end