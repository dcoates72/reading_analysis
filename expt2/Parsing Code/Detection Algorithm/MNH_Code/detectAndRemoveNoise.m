function detectAndRemoveNoise()
%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************

% Detects and removes un-physiological movement 
% (which derives from noise and NaNBlocks)

global ET
global Verbose_TF;
global PathToOutputDirectory
global RawDataFileName
global FID_Results % File Identifier for results file '..._Results.csv'.

if Verbose_TF,fprintf('\n%s\n','***************** in detectAndRemoveNoise'),end

V = ET.Vectors.vel;
A = ET.Vectors.acc;
NoiseType=zeros(5,1);

% Check for Extreme Velocity

NoiseType(1)=sum(ET.Vectors.NaNIndex);
ET.Vectors.NaNIndex(V > ET.Scalars.Params.NaNVelocityThreshold & ET.Vectors.NaNIndex==false)=true;
NoiseType(2) = sum(ET.Vectors.NaNIndex) - NoiseType(1);

% Check for Extreme Acceleration

ET.Vectors.NaNIndex(A > ET.Scalars.Params.NaNAccThreshold & ET.Vectors.NaNIndex==false)=true;
NoiseType(3) = sum(ET.Vectors.NaNIndex) - NoiseType(2) - NoiseType(1);

% Check for RIONEPS Noise
[RIONEPS_X]=DetectRIONEPS(ET.Vectors.Xorg,1000,ET.Scalars.Params.RIONEPS_Threshold);
[RIONEPS_Y]=DetectRIONEPS(ET.Vectors.Yorg,1000,ET.Scalars.Params.RIONEPS_Threshold);
RIONEPS=RIONEPS_X | RIONEPS_Y;
ET.Vectors.NaNIndex(RIONEPS==true & ET.Vectors.NaNIndex==false)=true;
NoiseType(4) = sum(ET.Vectors.NaNIndex) - NoiseType(3) - NoiseType(2) - NoiseType(1) ;

% Loop Through NaNBlocks to see if data needs to be removed before and
% after each NaNBlock

NaNBlock = bwlabel(ET.Vectors.NaNIndex);
ET.Scalars.Data.NumNaNBlocks=max(NaNBlock);
N_NaNsInRange=0;
for N_NaN_Block = 1:ET.Scalars.Data.NumNaNBlocks;
    
    NaNBlockIndex= find(NaNBlock == N_NaN_Block);

    % Find New Start Of NaN Block
    NewStart=[];
    if Verbose_TF;
        fprintf('\nTotal Number of NaN Blocks = %d, Processing NaN Block %d\n',ET.Scalars.Data.NumNaNBlocks,N_NaN_Block);
    end;
    if NaNBlockIndex(1) <=10;
        NewStart = 1;
    else
        for m = NaNBlockIndex(1)-1:-10:11        
            VelocityBlockToCheck=V(m-9:m);
            if max(VelocityBlockToCheck) > ET.Scalars.Data.NoisePercentile90;
                continue
            else
                NewStart=m+1;
                break;
            end
        end;
    end
    
    % Find New End Of NaN Block
    NewEnd=[];
    if NaNBlockIndex(end) >= ET.Scalars.Data.Length-100;
        NewEnd = ET.Scalars.Data.Length;
    else
        for m = NaNBlockIndex(end)+1:10:ET.Scalars.Data.Length-10;
            VelocityBlockToCheck=V(m:m+9);
            if max(VelocityBlockToCheck) > ET.Scalars.Data.NoisePercentile90;
                continue
            else
                NewEnd=m;
                break;
            end
        end;
    end
    for Index = NewStart:NewEnd
        if ET.Vectors.NaNIndex(Index)==false;
            ET.Vectors.NaNIndex(Index)=true;
            N_NaNsInRange=N_NaNsInRange+1;
        end;
    end
    if(Verbose_TF);
        fprintf('This NaNBlock originally starts at %d, now it start at %d\n',NaNBlockIndex(1)  ,NewStart)
        fprintf('This NaNBlock originally ends   at %d, now it ends  at %d\n',NaNBlockIndex(end),NewEnd);
    end;
end;

FullPathToResultsFile=[PathToOutputDirectory RawDataFileName(1:13) '_Results.txt']
FID_Results =fopen(FullPathToResultsFile,'w');
NoiseType(5) = sum(ET.Vectors.NaNIndex) - NoiseType(4) - NoiseType(3) - NoiseType(2) - NoiseType(1);
if Verbose_TF
    E=ET.Scalars.Data.Length;
    fprintf(FID_Results,'\n\n-------------------Noise Accounting------------------\n');
    fprintf(FID_Results,'Number of samples of Noise due to NaNs and SG filter   = %5.5d, Percent of recording = %5.2f\n',NoiseType(1),100*NoiseType(1)/E);
    fprintf(FID_Results,'Number of samples of Noise due to Velocity too high    = %5.5d, Percent of recording = %5.2f\n',NoiseType(2),100*NoiseType(2)/E);
    fprintf(FID_Results,'Number of samples of Noise due to Accleration too high = %5.5d, Percent of recording = %5.2f\n',NoiseType(3),100*NoiseType(3)/E);
    fprintf(FID_Results,'Number of samples of RIONEPS Noise                     = %5.5d, Percent of recording = %5.2f\n',NoiseType(4),100*NoiseType(4)/E);
    fprintf(FID_Results,'Number of samples of Noise surrounding other Noise     = %5.5d, Percent of recording = %5.2f\n',NoiseType(5),100*NoiseType(5)/E);
    AllTypesOfNoise = sum(NoiseType);
    fprintf(FID_Results,'Number of samples of All noise                         = %5.5d, Percent of recording = %5.2f\n',AllTypesOfNoise,100*AllTypesOfNoise/E);
    if sum(ET.Vectors.NaNIndex) ~= AllTypesOfNoise
        fprintf('Problem with noise accounting %d, %d\n',sum(ET.Vectors.NaNIndex),AllTypesOfNoise)
        fprintf('There are more NaNs than Noise events counted.  This should never happen.\n')
        PlaySound(2)
        pause
    end
end

All_NaN_Indexes = find(ET.Vectors.NaNIndex);
ET.Scalars.Data.Percent_NaNs_with_All_Filters = 100*length(All_NaN_Indexes)/ET.Scalars.Data.Length;
%
% if trial contains more than 18% noise/artifact, mark it as a bad trial
%
if length(All_NaN_Indexes)/ET.Scalars.Data.Length > 0.18
    disp('Warning: This trial contains > 18 Percent NaN samples')
    ET.Scalars.Data.NoiseTrial = 1;
else
    ET.Scalars.Data.NoiseTrial = 0;
end
return