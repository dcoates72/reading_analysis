function detectFixations()

%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************

%--------------------------------------------------------------------------
% Fixation detection
% Fixation are detected implicitly 
%--------------------------------------------------------------------------

global ET
global Verbose_TF
global PathToOutputDirectory
global FileName;

if Verbose_TF,fprintf('\n%s\n','***************** in detectFixations'),end

V = [ET.Vectors.vel];
%
% Potential fixations are all periods that are not saccades, PSOs or noise/artifact
%

PotentialFixationBlocks = ...
    ~(ET.Vectors.SaccadeIndex | ET.Vectors.PSOIndex | ET.Vectors.NaNIndex);
PotentialFixationBlocks(PotentialFixationBlocks>ET.Scalars.Data.Length)=[];

PotentialFixationBlocks = bwlabel(PotentialFixationBlocks);
% Process one potential fixation (or more precisely a period of gaze samples that might
% be a fixation) at the time.
N_GoodFixations = 1; % Count of fixations that meet all criteria

if Verbose_TF, fprintf('\n-------- Detecting Fixations, Total Number of Potential Fixations = %d ---------\n\n',max(PotentialFixationBlocks)),end;

for ThisPotentialFixation = 1:max(PotentialFixationBlocks)
    
    if Verbose_TF;
        fprintf('\nChecking potential fixation # %d, Good Fixations Found = %d, Subject %d, Session = %d.\n',...
        ThisPotentialFixation,N_GoodFixations-1,ET.Scalars.Data.Subject,ET.Scalars.Data.Session);
    end;

    % The samples related to the current potential fixation
    IndicesOfThisFixationBlock = find(PotentialFixationBlocks == ThisPotentialFixation);
    FixationBlockStartsAt=IndicesOfThisFixationBlock(1);
    FixationBlockEndsAt=IndicesOfThisFixationBlock(end);
    if Verbose_TF, fprintf('This Potential Fixation Starts at %d and Ends at %d\n',FixationBlockStartsAt,FixationBlockEndsAt), end;

    % Check that the fixation duration exceeds the minimum duration criteria.
    
    fixdur = length(IndicesOfThisFixationBlock)/ET.Scalars.Params.samplingFreq;
    if fixdur < ET.Scalars.Params.minFixDur;
        if Verbose_TF,fprintf('REJECT POTENTIAL FIXATION: Too Short - Strt=%d, End=%d, Duration (sec) = %5.3f\n',FixationBlockStartsAt,FixationBlockEndsAt,fixdur), end;
        continue    
    end
    
    % Find the maximum position difference between any 2 points in this
    % potential fixation
    MaxX=max(ET.Vectors.Xsmo(IndicesOfThisFixationBlock));
    MinX=min(ET.Vectors.Xsmo(IndicesOfThisFixationBlock));
    MinMaxDiffX=abs(MaxX-MinX);
    MaxY=max(ET.Vectors.Ysmo(IndicesOfThisFixationBlock));
    MinY=min(ET.Vectors.Ysmo(IndicesOfThisFixationBlock));
    MinMaxDiffY=abs(MaxY-MinY);
    MinMaxDiff=max(MinMaxDiffX,MinMaxDiffY);
    if MinMaxDiff>ET.Scalars.Params.MaxPosDiffWithinAFixation;
        if Verbose_TF
            fprintf('MinMaxDiff=%5.2f, Threshold = %5.2f *************\n',MinMaxDiff,ET.Scalars.Params.MaxPosDiffWithinAFixation);
            fprintf('REJECT POTENTIAL FIXATION: The Fixation has huge position MinMaxDiff - Strt=%d, End=%d\n',FixationBlockStartsAt,FixationBlockEndsAt);
        end
        continue
    end;
      
    % If all the above criteria are fulfilled, label it as a fixation.
    if Verbose_TF,fprintf('Potential Fixation Number %d is a true Fixation, Number of good fixations = %d !!!\n',ThisPotentialFixation,N_GoodFixations),end;
    ET.Vectors.FixationIndex(IndicesOfThisFixationBlock) = true;
    
    % Calculate the position of the fixation
    ET.Scalars.Data.FixationInfo(N_GoodFixations).meanXSmoPos = nanmean(ET.Vectors.Xsmo(IndicesOfThisFixationBlock));
    ET.Scalars.Data.FixationInfo(N_GoodFixations).meanYSmoPos = nanmean(ET.Vectors.Ysmo(IndicesOfThisFixationBlock));

    % Collect information about the fixation
    FixationBlockStartsAt = IndicesOfThisFixationBlock(1);
    FixationBlockEndsAt   = IndicesOfThisFixationBlock(end);
    if FixationBlockStartsAt == 0 || FixationBlockEndsAt == 0;
        fprintf('Fixation Block either starts at 0 msec or ends at 0 msec.  This should never happen\n')
        PlaySound(2)
        pause
    end;
    ET.Scalars.Data.FixationInfo(N_GoodFixations).start      = FixationBlockStartsAt/ET.Scalars.Params.samplingFreq; % in seconds
    ET.Scalars.Data.FixationInfo(N_GoodFixations).end        = FixationBlockEndsAt  /ET.Scalars.Params.samplingFreq; % in seconds
    ET.Scalars.Data.FixationInfo(N_GoodFixations).duration   = ET.Scalars.Data.FixationInfo(N_GoodFixations).end - ET.Scalars.Data.FixationInfo(N_GoodFixations).start;
    ET.Scalars.Data.FixationInfo(N_GoodFixations).MaxPosDiff = MinMaxDiff;

    %
    % Using the 90th percentile of the noise distribution, measure certain statistics of this fixation in relation to this threshold.
    %
    %       Percent of samples above the theshold 
    %
    FixationVelocityAboveThreshold=V(FixationBlockStartsAt:FixationBlockEndsAt)>ET.Scalars.Data.NoisePercentile90;
    ET.Scalars.Data.FixationInfo(N_GoodFixations).PrcntAboveThresh=...
        100*sum(FixationVelocityAboveThreshold)/(FixationBlockEndsAt-FixationBlockStartsAt+1);
    %
    %       Number of times the velocity channel crosses above or below this threshold 
    %
    ind = crossing(V(round(FixationBlockStartsAt):round(FixationBlockEndsAt)),[],ET.Scalars.Data.NoisePercentile90);
    ET.Scalars.Data.FixationInfo(N_GoodFixations).PrctThresholdCrossing=100*length(ind)/(FixationBlockEndsAt-FixationBlockStartsAt+1);
       
    N_GoodFixations = N_GoodFixations+1;
end
ET.Scalars.Data.N_Fixations=N_GoodFixations-1;

return