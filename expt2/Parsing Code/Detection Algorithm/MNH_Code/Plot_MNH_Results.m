function Plot_MNH_Results_Test()

%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************

%
% This program reads in a '..._Class.csv' file, which contains the 
% position and velocity vectors as well as the class.
%
% Classes
%
% Fixation Class         = 1
% Saccade Class          = 2
% Glissade Class         = 3
% Noise\Artifact         = 4
% Unclaswsified          = 5
%
% It creates '....fig' files and '....pdf' files for each 1 second
% of data.
%
% To view the .fig files use ViewData.
%

clc
close all
commandwindow()

%
% Path to both Input (_Class.csv) and Output (.fig, .pdf) files
%
PathStr=strcat('D:\Dropbox\NYSTROM_MATLAB_CODE\Nystom_Modified_Method\CleanCodeForPublication\Results\');

fprintf('Starting Plot_MNH_for_Scoring\n')

%
% Which subjects and sessions to analyze.
%
% GoodDataVector = [1:11 13:17 19:27 29:35 37:39 41:49 51:55 57:63 65:74 76:81 83:103 106:111 113:123 125:159 161:173 175:204 206:323 325:335];
% GoodDataVector = GoodDataVector+1000;
% numsubs=length(GoodDataVector);
% RandomSubs=randi(numsubs,5,1);
% Indices = [50 308 304 154 254];
% GoodDataVector = GoodDataVector(Indices)
% % Randomly selected subjects   1057        1326        1321        1169        1271
GoodSession    =[1 2 1 2 1 2 1 2 1 2 ];
GoodDataVector=[1057  1057 1326 1326 1321 1321 1169 1169 1271 1271];
%
% Loop through subjecs and sessions
%

for GoodDataIndex = 1:length(GoodDataVector)
    Subject = GoodDataVector(GoodDataIndex);
    Session = GoodSession(GoodDataIndex);
    PrepareToPlotEyeMovement(PathStr,Subject,Session)
end
fprintf('Ending  Plot_MNH_Results\n')
return

function PrepareToPlotEyeMovement(PathStr,Subject,Session)

global DataArray
global NSamples

fprintf('Starting PlotEyeMovement\n')
%
% Create name of subdirectory for this input (_Class.csv) and output (.fig,.pdf) data.

SubDirName=strcat('S_',num2str(Subject,'%04d'),'_S',num2str(Session),'_TEX\');
%
% Set up colors for each class
%
ColorSet(1,:)=[1.0,0.0,0.0];% Fixation - Red
ColorSet(2,:)=[0.0,1.0,0.0];% Saccade - Green
ColorSet(3,:)=[0.0,0.0,1.0];% PSO - Blue
ColorSet(4,:)=[1.0,0.0,1.0];% Noise/Artifact - Magenta
ColorSet(5,:)=[0.0,0.0,0.0];% Unclassified - Black

InputFileName=strcat(SubDirName(1:13),'_Class.csv');

FullPathToInputFile=[PathStr SubDirName InputFileName];
fprintf('Full Path to Input File: \n%s\n',FullPathToInputFile)
%
% Read input file
%
DataArray=csvread(FullPathToInputFile);
%
% Need to set up vectors before plotting the data
%
PreparePlottingValues()

NSamples=length(DataArray);
fprintf('The total number of samples is %d\n',NSamples)
%
% plot Each Second of DATA
%
for i = 1:1000:NSamples
    %
    % Create file name for .fig output file
    %
    ImageFileName=[num2str(Subject) '-' num2str(Session) '-' num2str(i,'%06d') '-' num2str(i+999,'%06d') '.fig'];
    MyTitle=['Subject = ' num2str(Subject) ' | Session = ' num2str(Session) ' | Start Msec = ' num2str(i,'%06d') ' | End Msec = ' num2str(i+999,'%06d') ' |'];
    fprintf('Title =\n%s\n',MyTitle)
    %
    % Call function that actually plots the data
    %
    plotMyData(i,MyTitle,PathStr,SubDirName,ImageFileName,ColorSet);
    %
    % Run Java Garbage Collector
    %
    java.lang.System.gc
    java.lang.System.gc()
    close all
end

fprintf('Ending PlotEyeMovement\n')
return

function plotMyData(StartMsec,MyTitle,PathStr,SubDirName,ImageFileName,ColorSet)

global C
global NSamples

fprintf('Starting plotMyData\n');
%
% Set up figure for page of data
%
FigHandle=figure(1);
set(FigHandle,'units','pixels','position',[50 50 1100 800],'Color',[0.8 0.8 0.8],...
    'GraphicsSmoothing','off','visible','on','NumberTitle','off');
%
% Determine the first and last sample to be plotted
%
s=StartMsec;
e=min(s+999,NSamples);
% Some debugging code
% if s==20001
%     for i = s:e
%         fprintf('At Sample = %d, Class = %d\n',i,C.YsmoClass(i, 2))
%     end
%     pause
% end
fprintf('Start Sample= %d, End Sample = %d\n',s,e)
%
% Loop thru plot types
%
for Nsubplot = 1:3
    if Nsubplot == 1; % Plot Vertical Eye Position
        ax1 = subplot(3,1,Nsubplot);
        for m = 1:5
%             plot(ax1,C.Msec(s:e),C.YsmoClass(s:e, m),'LineStyle','-','Color',ColorSet(m,:),'linewidth',2); % Plot data as lines
              scatter(ax1,C.Msec(s:e),C.YsmoClass(s:e, m),1,ColorSet(m,:));% Plot data as points
              hold on
        end
        %
        % Computing Y axis limits
        %
        [MeanYsmo,MinYsmo,MaxYsmo]=GetMeanOfData('Y',s,e);
        YLIM_MIN=MeanYsmo-10;YLIM_MAX=MeanYsmo+10;
        if MinYsmo < YLIM_MIN+2;YLIM_MIN=MinYsmo-4;end;
        if MaxYsmo > YLIM_MAX-2;YLIM_MAX=MaxYsmo+4;end;
        if isnan(YLIM_MIN);return,end
        set(ax1,'Position',[0.030   0.70   0.935  0.22],...
        'XLim',[s-1 s+999],...
        'YLim',[YLIM_MIN YLIM_MAX],...
        'Color',[0.5 0.5 0.5],...
        'XTick',s-1:200:s+999,...
        'XTickLabelMode','manual',...
        'XTickLabel',{});
        text(s+25,YLIM_MAX-1.8,'Vert Pos','FontSize',15)
        MyTitle=strrep(MyTitle,'_','-');
        title(MyTitle)
    elseif Nsubplot == 2;% Plot Horizontal Eye Position
        ax2 = subplot(3,1,Nsubplot);
        for m = 1:5
%            plot(ax2,C.Msec(s:e),C.XsmoClass(s:e, m),'LineStyle','-','Color',ColorSet(m,:),'linewidth',2);
           scatter(ax2,C.Msec(s:e),C.XsmoClass(s:e, m),1,ColorSet(m,:));
           hold on
        end
        ax2.XTickLabelRotation=90;
        [MeanXsmo,MinXsmo,MaxXsmo]=GetMeanOfData('X',s,e);
        YLIM_MIN=MeanXsmo-10;YLIM_MAX=MeanXsmo+10;
        if MinXsmo < YLIM_MIN+2;YLIM_MIN=MinXsmo-4;end;
        if MaxXsmo > YLIM_MAX-2;YLIM_MAX=MaxXsmo+4;end;
        set(ax2,'Position',[0.030   0.40   0.935  0.29],...
                'XLim',[s-1 s-1+1000],...
                'YLim',[YLIM_MIN YLIM_MAX],...
                'Color',[0.5 0.5 0.5],...
                'XTick',s-1:200:s-1+1000,...
                'XTickLabelMode','manual',...
                'XTickLabel',{});
         if (YLIM_MAX - MaxXsmo) < 4
             text(s+25,YLIM_MIN+1.5,'Hor Pos','FontSize',15)
         else
             text(s+25,YLIM_MAX-1.5,'Hor Pos','FontSize',15)
         end
    elseif Nsubplot == 3;% Plot Radial Velocity
        ax3=subplot(3,1,Nsubplot);
        for m = 1:5
%             plot(ax3,C.Msec(s:e),C.Vel_Class(s:e, m),'LineStyle','-','Color',ColorSet(m,:),'linewidth',2);
            scatter(ax3,C.Msec(s:e),C.Vel_Class(s:e, m),1,ColorSet(m,:));hold on
            if m == 1,plot(ax3,C.Msec(s:e),C.Vel_Class(s:e, m),'-r'),hold on,end
            if m == 2,plot(ax3,C.Msec(s:e),C.Vel_Class(s:e, m),'-g'),hold on,end
            if m == 3,plot(ax3,C.Msec(s:e),C.Vel_Class(s:e, m),'-b'),hold on,end
            if m == 4,plot(ax3,C.Msec(s:e),C.Vel_Class(s:e, m),'-m'),hold on,end
            if m == 5,plot(ax3,C.Msec(s:e),C.Vel_Class(s:e, m),'-k'),hold on,end
        end
        YLIM_MIN=0;YLIM_MAX=600;
        MinVel=nanmin(C.Vel_Class(s+20:e,1:5));MinVel=min(MinVel);
        MaxVel=nanmax(C.Vel_Class(s+20:e,1:5));MaxVel=max(MaxVel);
        if MinVel < YLIM_MIN;YLIM_MIN=MinVel;end;
        if MaxVel > YLIM_MAX;YLIM_MAX=MaxVel;end;
        set(ax3,'Position',[0.030   0.09  0.935  0.30], ...
                'XLim',[s-1 s-1+1000],...
                'YLim',[0 600],... 
                'Color',[0.5 0.5 0.5],...
                'XTick',s-1:200:s-1+1000,...
                'XTickLabel',{num2str(s-1,'%05d'),num2str(s-1+200,'%05d'),num2str(s-1+400,'%05d'),num2str(s-1+600,'%05d'),num2str(s-1+800,'%05d'),num2str(s-1+1000,'%05d')},...
                'XTickLabelMode','manual',...
                'XTickLabelRotation',90)
         text(s+25,550,'Velocity','FontSize',15)
    end
end
linkaxes([ax1,ax2,ax3],'x');
OutputImageFileFullPath=[PathStr SubDirName ImageFileName];

if exist(OutputImageFileFullPath,'file') == 2;delete(OutputImageFileFullPath);end;
fprintf('Full Path to Output File: \n%s\n',OutputImageFileFullPath)
FigHandle.InvertHardcopy = 'off';
savefig(gcf,OutputImageFileFullPath);

ImageFileName=strrep(ImageFileName,'fig','pdf');
OutputImageFileFullPath=[PathStr SubDirName ImageFileName];
if exist(OutputImageFileFullPath,'file') == 2;delete(OutputImageFileFullPath);end;
FigHandle.PaperSize = [11 8.5];
FigHandle.PaperPosition = [0.2500 0.25 10.5 7];
print(gcf,OutputImageFileFullPath,'-dpdf','-r500')
fprintf('Ending plotMyData\n')
return

function PreparePlottingValues()

fprintf('Starting PreparePlottingValues\n')

global C
global DataArray
global NSamples

C.Msec =DataArray(:,1);

Xsmo =DataArray(:,2);
Xsmo(isnan(Xsmo))=0;
Ysmo =DataArray(:,3);
Ysmo(isnan(Ysmo))=0;
Vel  =DataArray(:,4);
Vel(isnan(Vel))=300;
Class=DataArray(:,5);

C.XsmoClass=NaN(NSamples,5);C.YsmoClass=NaN(NSamples,5);C.Vel_Class=NaN(NSamples,5);C.Vel_Class_ONH=NaN(NSamples,5);
%
% Make sure that every sample has a class
%
if ~isempty(find(C.XsmoClass==0, 1));
    FirstSampleWithNoClass=find(C.XsmoClass==0, 1);
    fprintf('Found at least one sample without a class at %d\n',FirstSampleWithNoClass)
    pause
end;
%
% Loop thru each class
%
for i = 1:5

    C.ClassType=find(Class==i);
    C.XsmoClass(C.ClassType,i)=Xsmo(C.ClassType);
    C.YsmoClass(C.ClassType,i)=Ysmo(C.ClassType);
    C.Vel_Class(C.ClassType,i)=Vel(C.ClassType);
    C.ClassLength=length(C.ClassType);
    fprintf('Class = %d, Total = %d\n',i,C.ClassLength)

    C.NonClassType=find(Class~=i);
    C.XsmoClass(C.NonClassType,i)=NaN;
    C.YsmoClass(C.NonClassType,i)=NaN;
    C.Vel_Class(C.NonClassType,i)=NaN;
    C.ClassLength=length(C.ClassType);
end;
fprintf('Ending PreparePlottingValues\n')
return;

function [Mean,Min,Max]=GetMeanOfData(XorY,s,e)

    global C
    
    if strcmp(XorY,'Y')
        ThisArray=C.YsmoClass;
    elseif strcmp(XorY,'X')
        ThisArray=C.XsmoClass;
    end
    MySumGood = sum(nansum(ThisArray(s:e, 1:3))); %Sum of Good Data
    NaNs=isnan(C.XsmoClass(s:e, 1:3));
    SumNaNs=sum(NaNs,2);
    NumNaNs=length(SumNaNs(SumNaNs == 3));
    NumSamples=e-s+1;
    N_Good_Samples=NumSamples-NumNaNs;

    MySumBad = sum(nansum(ThisArray(s:e, 4:5))); %Sum of Good Data
    NaNs=isnan(C.XsmoClass(s:e, 4:5));
    SumNaNs=sum(NaNs,2);
    NumNaNs=length(SumNaNs(SumNaNs == 2));
    NumSamples=e-s+1;
    N_Bad_Samples=NumSamples-NumNaNs;
    %
    % We want to base the mean on good data only (fixations, saccades, PSO) as long as there are more than 10%
    % good data in this single second of data .If there are fewer than 100 good samples (fixation, saccade, PSO) on this page, 
    % compute the mean based on the noise and unclassified data only. 
    %
    if N_Good_Samples < 100              
        Mean = MySumBad/double(N_Bad_Samples); 
        Min=nanmin(ThisArray(s:e,4:5));Min=min(Min);
        Max=nanmax(ThisArray(s:e,4:5));Max=max(Max);
        fprintf('Using Noise, Unclassified, etc...,CountAlt=%d,MeanY = %0.1f\n',N_Bad_Samples,Mean)
    else
        Mean=MySumGood/double(N_Good_Samples);
        Min=nanmin(ThisArray(s:e,1:3));Min=min(Min);
        Max=nanmax(ThisArray(s:e,1:3));Max=max(Max);
    end
    return