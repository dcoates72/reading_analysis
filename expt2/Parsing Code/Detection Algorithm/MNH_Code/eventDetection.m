function eventDetection

%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************

global ET
global FullPathToInputFile
global PathToOutputDirectory
global RawDataFileName
global Verbose_TF

if Verbose_TF,fprintf('\n%s\n','***************** in eventDetection'),end

PathToRawData=strcat('D:\Dropbox\EyeMovementRawData\TEX_Only\Round1\');

% GoodDataVector = [1:11 13:17 19:27 29:35 37:39 41:49 51:55 57:63 65:74 76:81 83:103 106:111 113:123 125:159 161:173 175:204 206:323 325:335];
% GoodDataVector=GoodDataVector+1000;
% GoodDataVector = [1057        1326        1321        1169        1271];
GoodDataVector = 1001;
cleanUpETstructure();

for GoodDataIndex = 1:length(GoodDataVector)
    Subject = GoodDataVector(GoodDataIndex);
    for Session = 1:2;
        ET.Scalars.Data.Subject = Subject;
        ET.Scalars.Data.Session = Session;
        %
        % Set Up Paths
        %
        %-------------------------------------
        % Set up input path
        % Our data input files are named something like S_1051_S2_TEX.csv
        % This is subject number 1051, recording session #2, the text reading task.
        %-------------------------------------
        RawDataFileName = ['S_' num2str(Subject,'%04d') '_S' num2str(Session) '_TEX.csv'];
        FullPathToInputFile=[PathToRawData RawDataFileName];
        fprintf('\nFull Path To Raw Data Input File: %s\n',FullPathToInputFile)
        %
        % Describe Raw Data File Format
        %
        %    Data Format
        %         msec,       xpos,     ypos, pupil, valid
        %            0, -17.429579, 8.319143,  2893,     0
        %            1, -17.413525, 8.324913,  2893,     0
        %            2, -17.410849, 8.336452,  2890,     0
        %            3, -17.429579, 8.330682,  2887,     0
        %            4, -17.450981, 8.319143,  2886,     0
        %            5, -17.442956, 8.301832,  2887,     0
        %            6, -17.43493 , 8.281635,  2889,     0
        %
        % msec - an integer representing the millisecond of this sample
        % xpos - horizontal position in degrees of visual angle
        % ypos - vertical position in degrees of visual angle
        % pupil - size of pupil in pixels
        % valid - 0 unless data are returned as NaN (blink, eye-closure)
        %         valid is ignored in this software, and pupil is not used at all for
        %         classification, and can be removed.
        %-------------------------------------
        % Set up output path and delete old files
        %-------------------------------------
        PathToOutputDirectory=[pwd '\Results\' RawDataFileName(1:13) '\'];
        fprintf('\nPathToOutputDirectory = \n%s\n',PathToOutputDirectory);
        if ~exist(PathToOutputDirectory,'dir'), mkdir(PathToOutputDirectory),end;
        %
        % Remove all .csv, .jpg, and .fig files from the output directory
        % Do not remove .txt files, need this for 'end of reading' files, see loadData.m
        %
        warning('off','all');
        if exist(PathToOutputDirectory,'dir');
            delete(strcat(PathToOutputDirectory,'*.csv'));
            delete(strcat(PathToOutputDirectory,'*.jpg'));
            delete(strcat(PathToOutputDirectory,'*.fig'));
        end;
        warning('on','all');
        %**********************************************************************
        [signals]=loadData();
        %**********************************************************************
        % Calculate velocity and acceleration
        %**********************************************************************
        calVelAcc_sgolay(signals)
        %**********************************************************************
        % Characterize Fixation Noise prior to Classification
        %**********************************************************************
        GetPreliminaryNoiseStatistics();
        %**********************************************************************
        % Detect blinks and noise/artifact
        %**********************************************************************
        detectAndRemoveNoise()
        %**********************************************************************
        % Detect saccades and PSOs
        %**********************************************************************
        detectSaccades();
        %**********************************************************************
        detectFixations();
        %**********************************************************************
        finalCleanUpAndCreateOutput()
        %**********************************************************************
        matfilename=strcat(PathToOutputDirectory,'ET_S_',num2str(Subject,'%03d'),'_S',num2str(Session),'_TEX.mat');
        warning('off','MATLAB:DELETE:FileNotFound');
        delete(matfilename);
        warning('on','MATLAB:DELETE:FileNotFound');
        save(matfilename,'ET')
        %**********************************************************************
    end
end
return
