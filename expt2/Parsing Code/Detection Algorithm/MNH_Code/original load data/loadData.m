function [signals]=loadData()

%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/

%************************************************************************
%************************************************************************
%************************************************************************

global ET;
global FullPathToInputFile
global PathToOutputDirectory
global RawDataFileName
global Verbose_TF

if Verbose_TF,fprintf('\n%s\n','***************** in loadData'),end


DataArray=csvread(FullPathToInputFile,2);
Msec     =DataArray(:,1);
X        =DataArray(:,2);
Y        =DataArray(:,3);
Pupil    =DataArray(:,4);
EOD=length(X);
ET.Scalars.Data.OriginalLength = EOD;
%
% For text reading, we only want to analyze from the start to the time
% that the subject stops reading (End Of Reading, or EOR).
% There must be a file names something like S_1051_S2_TEX_EndOfReading.txt
% which contains an integer which is the msec sample when the EOR occurs.
% We determine this with our own software which shows us the entire record of 
% the vertical position trace.
%
EOR_FileName=[PathToOutputDirectory,RawDataFileName(1:13) '_EndOfReading.txt'];
fprintf('\nEnd Of Reading File Name = \n%s\n',EOR_FileName)
exist_status = exist(EOR_FileName,'file');
if exist_status ~= 2;
    fprintf('End Of Reading File does not exist:\n%s\ncode =  %d\n',EOR_FileName,exist_status)
    fprintf('Either an End Of Reading file needs to be created or this code needs to be modified to work without it.\n')
    PlaySound(2)
    pause
end;
EOR=csvread(EOR_FileName);

Msec=Msec(1:EOR);
X=X(1:EOR);
Y=Y(1:EOR);
Pupil=Pupil(1:EOR);
signals=[Msec X Y Pupil];

fprintf('\nEnd of Data = %6.6d, End of Reading = %6.6d\n',EOD,EOR)

ET.Scalars.Data.Length = EOR;

% Create ET.Vectors

ET.Vectors.Msec              =zeros(ET.Scalars.Data.Length,1);
ET.Vectors.Xorg              =zeros(ET.Scalars.Data.Length,1);
ET.Vectors.Yorg              =zeros(ET.Scalars.Data.Length,1);
ET.Vectors.Xsmo              =zeros(ET.Scalars.Data.Length,1);
ET.Vectors.Ysmo              =zeros(ET.Scalars.Data.Length,1);
ET.Vectors.velX              =zeros(ET.Scalars.Data.Length,1);
ET.Vectors.velY              =zeros(ET.Scalars.Data.Length,1);
ET.Vectors.vel               =zeros(ET.Scalars.Data.Length,1);
ET.Vectors.accX              =zeros(ET.Scalars.Data.Length,1);
ET.Vectors.accY              =zeros(ET.Scalars.Data.Length,1);
ET.Vectors.acc               =zeros(ET.Scalars.Data.Length,1);
ET.Vectors.NaNIndex          =false(ET.Scalars.Data.Length,1);
ET.Vectors.FixationIndex     =false(ET.Scalars.Data.Length,1);
ET.Vectors.SaccadeIndex      =false(ET.Scalars.Data.Length,1);
ET.Vectors.PSOIndex          =false(ET.Scalars.Data.Length,1);
ET.Vectors.NaNIndex          =false(ET.Scalars.Data.Length,1);
ET.Vectors.UnclassifiedIndex =false(ET.Scalars.Data.Length,1);
ET.Vectors.Classification    =zeros(ET.Scalars.Data.Length,1);

% Fill Relevant Vectors

ET.Vectors.NaNIndex=isnan(X(1:ET.Scalars.Data.Length));
ET.Scalars.Data.Percent_EyeLink_NaNs=100*sum(ET.Vectors.NaNIndex)/ET.Scalars.Data.Length;
fprintf('\nNumber NaNs from EyeLink Only= %d\n',sum(ET.Vectors.NaNIndex))

clear DataArray Msec X Y Pupil

end

