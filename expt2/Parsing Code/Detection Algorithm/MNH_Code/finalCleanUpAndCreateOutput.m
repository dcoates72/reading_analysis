function finalCleanUpAndCreateOutput()

%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************

global ET
global PathToOutputDirectory
global RawDataFileName;
global Verbose_TF;
global FID_Results % File Identifier for results file '..._Results.csv'.

if Verbose_TF,fprintf('\n%s\n','***************** finalCleanUpAndCreateOutput'),end

%
% Classes
%
% Fixation Class         = 1
% Saccade Class          = 2
% PSO Class              = 3
% Noise/Artifact Class   = 4
% UnClassfied            = 5

if Verbose_TF, fprintf('\n---------------------- finalCleanUpAndCreateOutput ------------------\n\n'),end;
V = [ET.Vectors.vel];

%
% Check to see if any samples are labelled as more than 1 event
% Also assign classes to Classification vector
%
fprintf('Checking for samples with more than one classification\n');
for i = 1:ET.Scalars.Data.Length
    SumClass=ET.Vectors.NaNIndex(i)+ET.Vectors.FixationIndex(i)+ET.Vectors.SaccadeIndex(i)+ET.Vectors.PSOIndex(i);
    if SumClass > 1
        fprintf('Found a Sample with more than 1 classification = %d, NaN = %d, Fixation = %d, Saccade = %d, PSO = %d\n', ...
                i,ET.Vectors.NaNIndex(i),ET.Vectors.FixationIndex(i),ET.Vectors.SaccadeIndex(i),ET.Vectors.PSOIndex(i));
        pause
    elseif SumClass ==0
        ET.Vectors.UnclassifiedIndex(i)=true;
        ET.Vectors.Classification(i)   =5;
    end
    if ET.Vectors.FixationIndex(i),ET.Vectors.Classification(i)=1;end
    if ET.Vectors.SaccadeIndex(i) ,ET.Vectors.Classification(i)=2;end
    if ET.Vectors.PSOIndex(i)     ,ET.Vectors.Classification(i)=3;end
    if ET.Vectors.NaNIndex(i)     ,ET.Vectors.Classification(i)=4;end
end
fprintf('Finished checking for samples with more than one classification\n');
%
% Look for unclassified data.  If it is surrounded by NaNs, declare it all as noise/artifact
%
LabelledUnclassified=bwlabel(ET.Vectors.UnclassifiedIndex);
if Verbose_TF;
    fprintf('The number of UnClassified Blocks is  %d\n',nanmax(LabelledUnclassified))
end;
% 
% Check if unclassified samples should be NaNs (surrounded by NaNs)
% 
if Verbose_TF;
    fprintf('\nFinding Unclassified Blocks that need to be NaNs\n\n')
end;
% 
for EachUnclassifiedBlock = 1:max(LabelledUnclassified);
    ThisUnclassfiedBlock=find(LabelledUnclassified==EachUnclassifiedBlock);
    Start=ThisUnclassfiedBlock(1);
    End=ThisUnclassfiedBlock(end);
    if Verbose_TF;
        fprintf('Working On UnClassified Blocks # %d, Start = %d, End = %d\n',...
            EachUnclassifiedBlock,Start,End)
    end;    
    if Start > 1 && End < ET.Scalars.Data.Length-1
        if ET.Vectors.NaNIndex(Start-1)==true || ET.Vectors.NaNIndex(End+1)==true
            %
            % This unclassified block is surrounded by NaNs.
            %
            ET.Vectors.NaNIndex(Start:End)=true;
            ET.Vectors.UnclassifiedIndex(Start:End)=false;
            ET.Vectors.Classification(Start:End)=4;
            if Verbose_TF;
                fprintf('Reclassifying Unclassified as NaN from Start %d to End %d, Length = %d\n',...
                    Start,End,End-Start+1)
            end;
        end;
    end
end
if Verbose_TF;
    fprintf('\nThe number of UnClassified Samples at Final Stage = %d\n',sum(ET.Vectors.UnclassifiedIndex))
end;
%
% Find the number of samples classified as fixation, saccade,PSO, NaN and Unclassified
%
ET.Scalars.Data.N_Sample_Fixations    = sum(ET.Vectors.FixationIndex);
ET.Scalars.Data.N_Sample_Saccades     = sum(ET.Vectors.SaccadeIndex);
ET.Scalars.Data.N_Sample_PSO          = sum(ET.Vectors.PSOIndex);
ET.Scalars.Data.N_Sample_Noise        = sum(ET.Vectors.NaNIndex);
ET.Scalars.Data.N_Sample_UnClassified = sum(ET.Vectors.UnclassifiedIndex);
a=ET.Scalars.Data.N_Sample_Fixations;
b=ET.Scalars.Data.N_Sample_Saccades;
c=ET.Scalars.Data.N_Sample_PSO;
d=ET.Scalars.Data.N_Sample_Noise;
e=ET.Scalars.Data.N_Sample_UnClassified;

[f,g,h]=GetMeanDurations();

fprintf(FID_Results,'\n\n-------------------Sample Counts By Class------------------\n');
fprintf(FID_Results,'The number of samples classified as fixation     = %5.5d, percent = %6.2f\n',a,100*a/ET.Scalars.Data.Length);
fprintf(FID_Results,'The number of samples classified as saccade      = %5.5d, percent = %6.2f\n',b,100*b/ET.Scalars.Data.Length);
fprintf(FID_Results,'The number of samples classified as PSO          = %5.5d, percent = %6.2f\n',c,100*c/ET.Scalars.Data.Length);
fprintf(FID_Results,'The number of samples classified as noise        = %5.5d, percent = %6.2f\n',d,100*d/ET.Scalars.Data.Length);
fprintf(FID_Results,'The number of samples classified as unclassified = %5.5d, percent = %6.2f\n',e,100*e/ET.Scalars.Data.Length);
TotalSamplesClassfied=ET.Scalars.Data.N_Sample_Fixations+ET.Scalars.Data.N_Sample_Saccades+ET.Scalars.Data.N_Sample_PSO+ET.Scalars.Data.N_Sample_Noise+ET.Scalars.Data.N_Sample_UnClassified;
fprintf(FID_Results,'The total number of samples classified           = %5.5d, percent = %6.2f\n',TotalSamplesClassfied,100*TotalSamplesClassfied/ET.Scalars.Data.Length);

fprintf(FID_Results,'\n\n-------------------Event Statistics By Class------------------\n');
fprintf(FID_Results,'The number of Fixations           = %d, Average Duration (msec) = %5.5f\n',ET.Scalars.Data.N_Fixations,f);
fprintf(FID_Results,'The number of Saccades            = %d, Average Duration (msec) = %5.5f\n',ET.Scalars.Data.N_Saccades,g);
fprintf(FID_Results,'The number of PSO                 = %d, Average Duration (msec) = %5.5f\n',ET.Scalars.Data.N_PSOs,h);

fclose(FID_Results);
%
% Create Output File with Classified Events
%
out=[ET.Vectors.Msec ET.Vectors.Xsmo ET.Vectors.Ysmo ET.Vectors.vel ET.Vectors.Classification];
OutputFileName=[RawDataFileName(1:13) '_Class.csv'];
if Verbose_TF
    fprintf('Full Path to Output File = \n%s\n',[PathToOutputDirectory OutputFileName])
end
csvwrite([PathToOutputDirectory OutputFileName],out);

return

function[a,b,c]=GetMeanDurations()
    global ET

    %******************************************
    a=0;count=0;
    for i = 1:ET.Scalars.Data.N_Fixations;
        if ~isnan(ET.Scalars.Data.FixationInfo(i).duration)
            a=a+ET.Scalars.Data.FixationInfo(i).duration;
            count=count+1;
        end;
    end
    ET.Scalars.Data.N_Fixations=count;
    a=(a*ET.Scalars.Params.samplingFreq)/count;
    %******************************************
    b=0;count=0;
    for i = 1:ET.Scalars.Data.N_Saccades
        if ~isnan(ET.Scalars.Data.SaccadeInfo(i).duration)
            b=b+ET.Scalars.Data.SaccadeInfo(i).duration;
            count=count+1;
        end
    end;
    ET.Scalars.Data.N_Saccades=count;
    b=(b*ET.Scalars.Params.samplingFreq)/count;
    %******************************************
    c=0;count=0;
    for i = 1:ET.Scalars.Data.N_PSOs
        if ~isnan(ET.Scalars.Data.PSOInfo(i).duration)
            c=c+ET.Scalars.Data.PSOInfo(i).duration;
            count=count+1;
        end
    end;
    ET.Scalars.Data.N_PSOs=count;
    c=(c*ET.Scalars.Params.samplingFreq)/count;
    %******************************************
return