function ViewData()

%************************************************************************
%************************************************************************
%************************************************************************

% This work is licensed under a Creative Commons Attribution-NonCommercial
% -ShareAlike 3.0 United States License" 
% https://creativecommons.org/licenses/by-nc-sa/3.0/us/
%
%************************************************************************
%************************************************************************
%************************************************************************

% 
% This program presents each second of data for viewing.
% It reads in the .fig files created by Plot_MNH_Results
% and displays them
%
% NOTE: There is a built in magnify function.  To use it,
% just point the mouse where you want to magnify and click the
% left moues button.  The first time you press it on the current
% page, you will have to click twice.  Thereafter, only one click
% will be necessary.
%
%

    clear all
    global PathToInputDirectory;
    global m;
    global SubjectCode;
    global ytext
    global MainFigure
    global ChangePage
    global m_fileName
    m_fileName='m.csv'

    clc
    close all;
    fclose('all');
    commandwindow();
    
    MainFigurePos=[21,175,1234,754];
    PathToInputDirectory='D:\Dropbox\NYSTROM_MATLAB_CODE\Nystom_Modified_Method\CleanCodeForPublication\Results\';
    PathToInputDirectory = uigetdir(PathToInputDirectory,'Choose Directory')
    PathToInputDirectory=[PathToInputDirectory '\'];
    WhereInString=findstr(PathToInputDirectory,'S_');
    subject=str2double(PathToInputDirectory(WhereInString+2:WhereInString++2+3));
    WhereInString=findstr(PathToInputDirectory,'_S');
    session=str2double(PathToInputDirectory(WhereInString+2));
    WhereInString=findstr(PathToInputDirectory,'S_');
    ExitSubjectSession=false;
    ExitProgram=false;
    csvwrite(strcat(PathToInputDirectory,'ExitProgram.csv'),ExitProgram);
    
    filelist=dir([PathToInputDirectory '*.fig']);
    NumFiles=length(filelist);
    NSamples=NumFiles*1000;
    
    while ExitProgram==false

        java.lang.System.gc;java.lang.System.gc();% run java garbage collectors
  
        ExitSubjectSession = false;csvwrite(strcat(PathToInputDirectory,'ExitSubjectSession.csv'),ExitSubjectSession);
        
        m = 1;csvwrite(strcat(PathToInputDirectory,m_fileName),m);
        ChangePage=true;
        while m <= NSamples
            m=csvread(strcat(PathToInputDirectory,m_fileName));
            if m >= NSamples;
                return;
            end;
            if m < 1;
                m=1;
                uiwait(msgbox('First Page For This Subject','Success','modal'));
            end;
            if (ChangePage);
                FigImageFileName=[num2str(subject),'-',num2str(session),'-',num2str(m,'%06d'),'-',num2str(m+999,'%06d'),'.fig']
                FullPath=char(strcat(PathToInputDirectory,FigImageFileName));
                if exist(FullPath,'file');
                    close all;
                    MainFigure=openfig(FullPath,'reuse');
                    set(MainFigure,'OuterPosition',MainFigurePos,'Visible','on','Units','normalized','NumberTitle','off');
                    ytext=0.0;
                else
                    uiwait(msgbox('Could Not Find Input .fig file','Success','modal'));
                    close(MainFigure);close(ScoreWindow);return;
                end
                figure(MainFigure);
                magnify()
            end;
            fprintf('working on Subject = %d, Second # %5.5d\n',SubjectCode,m)
            B    =uicontrol('Style', 'pushbutton','String','BACK',    'Units','normalized','Position', [0.96 0.235 0.04 0.06],'Callback',@Back_Callback);
            N    =uicontrol('Style', 'pushbutton','String','NEXT',    'Units','normalized','Position', [0.96 0.165 0.04 0.06],'Callback',@NextTrial_Callback);
            XX   =uicontrol('Style', 'pushbutton','String','EXIT',    'Units','normalized','Position', [0.96 0.025 0.04 0.06],'Callback',@EXIT_Callback);
            ExitSubjectSession=csvread(strcat(PathToInputDirectory,'ExitSubjectSession.csv'));
            if ExitSubjectSession;close 'all';break;end;
            ExitProgram=csvread(strcat(PathToInputDirectory,'ExitProgram.csv'));
            if ExitProgram;close all;fclose('all');return;end;
            uiwait
            if ChangePage;
                if (any(findall(0,'Type','Figure')==1));
                   close(1);
                end;
            end;
            ExitProgram=csvread(strcat(PathToInputDirectory,'ExitProgram.csv'))
            if ExitProgram
                fprintf('Exiting Program\n')
                close('all');fclose('all');return;
            end
%             ExitSubjectSession=csvread(strcat(PathToInputDirectory,'ExitSubjectSession.csv'))
%             if ExitSubjectSession
%                 fprint('Exiting Subject Session\n')
%                 close('all');fclose('all');return;
%             end
        end
    end
return

function Back_Callback(hObject, eventdata, handles)
global LoggingWindow
global ChangePage
global m_fileName
commandwindow()
global PathToInputDirectory
m = csvread(strcat(PathToInputDirectory,m_fileName));
m=m-1000;
ChangePage=true;
csvwrite(strcat(PathToInputDirectory,m_fileName),m);
close(LoggingWindow);
uiresume();
return

function NextTrial_Callback(hObject, eventdata, handles)
global PathToInputDirectory
global MainFigure
global LoggingWindow
global count
global ChangePage
global m_fileName
m = csvread(strcat(PathToInputDirectory,m_fileName));
m=m+1000;
ChangePage=true;
csvwrite(strcat(PathToInputDirectory,m_fileName),m);
close(MainFigure);close(LoggingWindow);
count=1;
uiresume();
return


function EXIT_Callback(hObject, eventdata, handles)
global PathToInputDirectory
global SRater
close all;
fclose('all')
ExitSubjectSession=true;csvwrite(strcat(PathToInputDirectory,'ExitSubjectSession.csv'),ExitSubjectSession);
ExitProgram=true;csvwrite(strcat(PathToInputDirectory,SRater,'ExitProgram.csv'),ExitProgram);
return

