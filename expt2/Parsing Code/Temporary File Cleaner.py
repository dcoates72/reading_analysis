# -*- coding: utf-8 -*-
"""
Created on Mon Mar  4 12:42:06 2019

@author: krish
"""

import os
import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()

data_directory=filedialog.askdirectory(title='Please select data directory')


for dirName,subdirlist,filelist in os.walk(data_directory):
#    try:
    for filename in filelist:
        if 'MNH' in filename:
            pass
        elif 'saccade_statistics_without_sweep' in filename:
            os.remove(os.path.join(dirName,filename))
#        elif 'fixation_statistics' in filename:
#            os.remove(os.path.join(dirName,filename))
#        elif 'statistics' in filename:
#            os.remove(os.path.join(dirName,filename))
        elif 'saccade_summary_without_sweep' in filename:
            os.remove(os.path.join(dirName,filename))
#            elif 'eyetrace_events_saccades' in filename:
#                os.remove(os.path.join(dirName,filename))
#            elif 'eyetrace_events_fixation' in filename:
#                os.remove(os.path.join(dirName,filename))
#            elif 'samples' in filename:
#                os.remove(os.path.join(dirName,filename))
#            elif 'trial_duration' in filename:
#                os.remove(os.path.join(dirName,filename))
#            elif 'eyetrace_events' in filename:
#                os.remove(os.path.join(dirName,filename))
#    except FileNotFoundError:
#        print('File not found')